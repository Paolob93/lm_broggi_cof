package client;

import java.rmi.RemoteException;

import client.rmi.RMIClientHandler;
import client.view.Console;



/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *This Class Client, it contains the main method.
 */
public class Client {

	private String connection;
	private RMIClientHandler clientRMI;
	/**
	 * Empty constructor
	 */
	Client(){
		}
	
	/**
	 * Create new RMICLientHandler object.
	 * @throws RemoteException
	 */
	public void addToServerRMI() throws RemoteException {
	
		clientRMI = new RMIClientHandler();
		clientRMI.setClient();
		
	}
	
	

	public RMIClientHandler getRMIClient(){
		return clientRMI;
	}
	
	public String getConnection(){
		return connection;
	}
	
	/**
	 * Interrupt this Thread when they player has been disconnected
	 */
	public static void killMe(){
		Thread.currentThread().interrupt();
	}
	

	/**
	 * The main method
	 * @param args
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws RemoteException, InterruptedException {
		
		Console.printEmptyLins();
		Console.printEmptyLins();
		Console.printLine("-- WELCOME TO:");
		Console.printLOGO();
		Console.printEmptyLins();
		Console.printEmptyLins();
		Client client = new Client();
		client.addToServerRMI();
		client.getRMIClient().startMatch();	
	}
	
}
