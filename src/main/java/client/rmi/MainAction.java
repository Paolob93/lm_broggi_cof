package client.rmi;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.view.Console;
import common.exception.CustomException;
import common.exception.WrongCardsException;
import common.interfaces.PlayerControllerInterface;

/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *This is the mainAction class which contains all the methods of the main action
 *
 */
public class MainAction {
	
	private static int lobbyID;
	private static PlayerControllerInterface controller;
	private static Logger loggerPlayerController = Logger.getLogger(MainAction.class.getName());
	private static final  List<String> colorString = Arrays.asList("BLACK", "ORANGE","WHITE","BLUE","PINK","VIOLET","JOLLY") ;
	private static final List<String> colorStringNoJolly = Arrays.asList("BLACK", "ORANGE","WHITE","BLUE","PINK","VIOLET") ;
	private static final List<String> regionString = Arrays.asList("SEA", "HILL","MOUNTAIN","KING") ;
	private static final List<String> regionStringNoKing = Arrays.asList("SEA", "HILL","MOUNTAIN") ;
	private static Map<Character, Method> mainActionMap = new HashMap<>();
	private FileHandler fh;
	
	/**
	 * The constructor of the class that puts the four main methods in the Map, and creates the file logger
	 * @param lobby
	 * @param controllerInt
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public MainAction(int lobby, PlayerControllerInterface controllerInt) throws NoSuchMethodException{
		lobbyID = lobby;
		controller =controllerInt;
		mainActionMap.put('1', MainAction.class.getMethod("mainActionElectCounsellor")); 
		mainActionMap.put('2', MainAction.class.getMethod("mainActionBuyPermitTile"));
		mainActionMap.put('3', MainAction.class.getMethod("mainActionBuildEmporium"));
		mainActionMap.put('4', MainAction.class.getMethod("mainActionBuildEmporiumKing"));
		try {
			fh = new FileHandler(MainAction.class.getName() + ".txt", true);
			fh.setFormatter(new SimpleFormatter());
			loggerPlayerController.addHandler(fh);
			loggerPlayerController.setUseParentHandlers(false);
		} catch (IOException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);;
		}
	}
	
	/**
	 * This method allows the player to elect a councillor.
	 * @throws RemoteException
	 */
	public static void mainActionElectCounsellor() throws RemoteException{
		Scanner scannerInput = new Scanner(System.in);
		Console.showCouncils(lobbyID,true);
		Console.showCouncillors(lobbyID);
		boolean ok;
		String color;
		do{
			 ok = true;
			Console.printLine("Enter a valid color:");
			color = scannerInput.nextLine();
			color = color.toUpperCase();
			if(!colorStringNoJolly.contains(color)){
				Console.printLine("Input ERROR! , repeat");
				ok=false;
			}
		}while(!ok);
		String region;
		
		do{
			ok=true;
			Console.printLine("Enter a valid region");
			region = scannerInput.nextLine();
			region = region.toUpperCase();
			if(!regionString.contains(region)){
				Console.printLine("Input ERROR! , repeat");
				ok=false;
			}
		}while(!ok);
		
		
		if("KING".equals(region)){
			try {
				controller.backup();
				controller.electKingCouncillor(color);
			} catch (CustomException  e) {
				controller.restore();
				loggerPlayerController.log(Level.INFO, e.getDetails(), e);
			}
		

		}else{
			try {
				controller.backup();
				controller.electCouncillor(color,region);
				Console.printLine("Councillor successfully elected!");
			} catch (CustomException  e) {
				controller.restore();
				Console.printLine(e.getDetails());
				loggerPlayerController.log(Level.INFO, e.getDetails(), e);
			}

		}
	}

	
	/**
	 * This method allows the player to buy a permit tile from the region board.
	 * @throws RemoteException
	 */
	public static void mainActionBuyPermitTile() throws RemoteException{
		Scanner scannerInput = new Scanner(System.in);
		Console.showPermitTiles(lobbyID);
		Console.showCardPlayer(controller);
		Console.showCouncils(lobbyID,false);
		Console.printLine("Enter the color of the card that you want to use - max 4 included the jolly card - (ORANGE,WHITE,BLACK,BLUE,PINK,VIOLET,JOLLY)");
		Console.printLine("Enter 0 if you want to go back to the menu");
		Console.printLine("First enter the number of cards you want to use");
		try {
			controller.backup();
			int i = scannerInput.nextInt();
			if(!(i==1 || i==2 || i==3 || i==4))
				throw new WrongCardsException();
			controller.clearHand();
			addCardBlock(i);
			String region;
			boolean ok;
			do{
				ok = true;
				Console.printLine("Enter the region from which to purchase a PermitTile - (sea,hill,mountain)");
				region = Console.read();
				region = region.toUpperCase();
				if(!regionStringNoKing.contains(region)){
					Console.printLine("Input ERROR! , repeat");
					ok=false;
				}
			}while(!ok);
			int position;
			
			do{
				ok = true;
				Console.printLine("Enter the index position of the Permit Tile which you want to  purchase (0 - left, 1 - right)");
				position = scannerInput.nextInt();
				if(position!=1 && position!=0){
					Console.printLine("Input ERROR! , repeat");
					ok=false;
				}
			}while(!ok);

			controller.buyPermitTile(region, position);

		} catch (CustomException  e) {
			controller.restore();
			Console.printLine(e.getDetails());
			loggerPlayerController.log(Level.INFO, e.getDetails(), e);
		}

	}
	
	/**
	 * This methods allows the player to build an Emporium in a city.
	 * @throws RemoteException
	 */
	public static void mainActionBuildEmporium() throws RemoteException{
		Scanner scannerInput = new Scanner(System.in);
		Console.showMap(lobbyID);
		Console.printLine("You can biuld in this cities:");
		int numTile = Console.getPermitTilePlayer(controller)-1;
		int positionTile;
		boolean ok;
		do{
			ok= true;
			Console.printLine("Enter the number corresponding to the card position to use");
			Console.printLine("To go back enter 999");
			positionTile = scannerInput.nextInt();
			if(positionTile==999){
				ok=true;
			}
			else if(positionTile<0 || positionTile>numTile){
				Console.printLine("Input ERROR! , repeat");
				ok=false;
			}

		}while(!ok);
		try{
			if(positionTile==999)
				throw new CustomException();
			Console.printLine("Enter the name of the city in the Pertmit Tile choice");
			String city = Console.read();
			String cityUp = city.substring(0, 1).toUpperCase() + city.substring(1);
			controller.backup();
			controller.buildEmporium(cityUp, positionTile);
		} catch (CustomException   e) { 
			controller.restore();
			Console.printLine(e.getDetails());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
	}

	
	/**
	 * This methods allows the player to build an Emporium in a city whit the help of the King.
	 * @throws WrongCardsException
	 * @throws RemoteException
	 */
	public static void mainActionBuildEmporiumKing() throws WrongCardsException, RemoteException{
		Scanner scannerInput = new Scanner(System.in);
		Console.showMap(lobbyID);
		Console.showCardPlayer(controller);
		Console.showKingCouncil(lobbyID);
		Console.printLine("");
		Console.printLine("Enter the color of the card that you want to use - max 4 included the jolly card - (ORANGE,WHITE,BLACK,BLUE,PINK,VIOLET,JOLLY)");
		Console.printLine("Enter 0 if you want to go back to the menu");
		Console.printLine("First enter the number of cards you want to use");
		int i = scannerInput.nextInt();
		if(!(i==1 || i==2 || i==3 || i==4))
			throw new WrongCardsException();
		else if(i==0)
			return;
		controller.clearHand();
		addCardBlock(i);
		
		Console.printLine("Enter the name of the destination city");

		String cityKing = Console.read();
		String cityUpKing = cityKing.substring(0, 1).toUpperCase() + cityKing.substring(1);
		
		try {
			controller.backup();
			controller.buildEmporiumKing(cityUpKing);
		} catch (CustomException  e){
			controller.restore();
			Console.printLine(e.getDetails());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}

	}
	
	/**
	 * This methods get the main action method from the map and executes it.
	 * @param index
	 */
	
	public void doMainAction(char index){
		try {
			mainActionMap.get(index).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	/**
	 * This method run n-times the inserts of a color card 
	 * @param i
	 * @throws WrongCardsException 
	 */
	public static void addCardBlock(int i) throws WrongCardsException{
		int j=0;
		String[] cardsArray = new String[i];
		do{
			Console.printLine("Enter the "+(j+1)+"° color");
			String color = Console.read();
			color = color.toUpperCase();
			if(colorString.contains(color)){
				cardsArray[j] = color;
				j++;
			}else{
				Console.printLine("Input color wrong, please reenter the correct one");
			}
		}while(j<i);
		addCard(cardsArray);
	}
	
	/**
	 * This methods call a remote methods from the controller-interface, and it pass the color of the card that we want use.
	 * @param cardsArray
	 * @throws WrongCardsException 
	 */
	public static void addCard(String[] cardsArray) throws WrongCardsException{
		try {
			controller.addCard(cardsArray);
		} catch (RemoteException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
