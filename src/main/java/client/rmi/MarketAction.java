package client.rmi;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.view.Console;
import common.exception.NotAvailableAssistantsException;
import common.exception.YouArePoorException;
import common.interfaces.PlayerControllerInterface;

/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *This class Market Action contains the methods of the market session.
 */
public class MarketAction {

	private static PlayerControllerInterface controller;
	private static Scanner scannerInput = new Scanner(System.in);
	private static Map<Character, Method> marketActionMap = new HashMap<>();
	private static Logger loggerPlayerController = Logger.getLogger(MainAction.class.getName());
	private FileHandler fh;
	
	/**
	 * Constructor of the class, it puts three Method type object in the Map and create the file logger.
	 * @param controllerInt
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public MarketAction(PlayerControllerInterface controllerInt) throws NoSuchMethodException{
		controller = controllerInt;
		marketActionMap.put('1', MarketAction.class.getMethod("sellPoliticCard"));
		marketActionMap.put('2', MarketAction.class.getMethod("sellPermitTile"));
		marketActionMap.put('3', MarketAction.class.getMethod("sellAssistant"));
		try {
			fh = new FileHandler(MainAction.class.getName() + ".txt", true);
			fh.setFormatter(new SimpleFormatter());
			loggerPlayerController.addHandler(fh);
			loggerPlayerController.setUseParentHandlers(false);
		} catch (IOException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);;
		}
	}
	
	/**
	 * This method ask the player to insert the color and the price of the Politic Card that he want sell. And call a remote object.
	 * @throws RemoteException
	 */
	public static void sellPoliticCard() throws RemoteException{
		Console.printLine("Insert the color of the cards that you want to sell: ");
		String color =  Console.read();
		Console.printLine("Insert the price: ");
		int price =  scannerInput.nextInt();
		color = color.toUpperCase();
		controller.sellPoliticCard(color, price);
	}
	
	
	/**
	 * This method ask the player to insert the index of the tile and the price of the Politic Card that he want sell. And call a remote object.
	 * @throws RemoteException
	 */
	public static void sellPermitTile() throws RemoteException{
		Console.printLine("Insert the index of the tile that you want to sell: ");
		int index =  scannerInput.nextInt();
		Console.printLine("Insert the price: ");
		int price =  scannerInput.nextInt();
		controller.sellPermitTile(index, price);
	}
	
	/**
	 * This method ask the player to insert the price of the assistant that he want sell.
	 * @throws RemoteException
	 */
	public static void sellAssistant() throws RemoteException{
		Console.printLine("Insert the price: ");
		int price =  scannerInput.nextInt();
		try {
			controller.backup();
			controller.sellAssistant(price);
		} catch ( NotAvailableAssistantsException e) {
			controller.restore();
			Console.print(e.getMessage());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
	}
	
	/**
	 * This method try to buy an item in the market store.
	 * @param index
	 * @throws RemoteException
	 */
	public void buyItem(int index) throws RemoteException{
		try {
			controller.backup();
			controller.buyItem(index);
		} catch (YouArePoorException e) {
			controller.restore();
			Console.printLine(e.getMessage());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
	}
	
	/**
	 * Sell phase of the market, shows the item of the player that we could sell and the options which object sell.
	 * @throws RemoteException
	 */
	public static void sellPhaseMarket() throws RemoteException{
		boolean market = false;
		while(!market){
			Console.printLine("--- MARKET ---");
			Console.printLine("--- Oggetti che potresti vendere ---");
			Console.showItemPlayerMarket(controller);
			Console.showActionMarketSell();

			char marketAction = scannerInput.next().charAt(0);
			if(marketAction == '0')
				market = true;
			else
				doMarketAction(marketAction);
		}
		
	}
	
	/**
	 * Buy phase of the market, shows the item selled in the market and ask the player if he wants buy something and what.
	 * @param lobbyID
	 * @throws RemoteException
	 */
	public void buyPhaseMarket(int lobbyID) throws RemoteException{
		
		Console.showItemInMarket(lobbyID);
		Console.printLine("Insert the index of the object that you want to buy - if you don't want to buy anything insert '999': ");
		int marketAction = scannerInput.nextInt();
		if(marketAction == 999)
			Console.printLine("--- MARKET ENDED ---");
		else
			buyItem(marketAction);
	}
	
	
	/**
	 * Receive the index of the action that is store in the Map and that execute it.
	 * @param index
	 */
	public static void doMarketAction(char index){
		try {
			marketActionMap.get(index).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			loggerPlayerController.log(Level.WARNING, e.getMessage(), e);
		}
	}
}
