package client.rmi;


import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.view.Console;
import common.exception.CustomException;
import common.exception.NotAvailableAssistantsException;
import common.interfaces.PlayerControllerInterface;
import common.interfaces.ServerControllerInterface;



/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *
 *Player Controller class, this is the controller of the match in the client side.
 */
public class PlayerController{
	
	private static PlayerControllerInterface controller;
	private ServerControllerInterface serverController;
	private char command;
	private static Logger loggerPlayerController = Logger.getLogger(PlayerController.class.getName());
	private int myID;
	private int lobbyID;
	private MainAction mainActionPlayer;
	private MarketAction marketActionPlayer;
	private QuickAction quickActionPlayer;
	private FileHandler fh;
	
	/**
	 * Public constructor of the class, creates three class and the file logger.
	 * @param controllerInt
	 * @param serverControllerInt
	 * @param myID
	 * @param lobbyID
	 * @throws RemoteException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws NotBoundException
	 */
	public PlayerController(PlayerControllerInterface controllerInt, ServerControllerInterface serverControllerInt, int myID, int lobbyID) throws RemoteException, NoSuchMethodException, NotBoundException{
		
		this.myID = myID;
		this.lobbyID = lobbyID;
		controller = controllerInt;
		serverController = serverControllerInt;
		mainActionPlayer = new MainAction(lobbyID, controller);
		quickActionPlayer =  new QuickAction(lobbyID, controller);
		marketActionPlayer = new MarketAction(controller);
		
		try {
			   fh = new FileHandler(PlayerController.class.getName() + ".txt", true);
			   fh.setFormatter(new SimpleFormatter());
			   loggerPlayerController.addHandler(fh);
			   loggerPlayerController.setUseParentHandlers(false);
			  } catch (IOException e) {
			   loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);;
			  }
	}
	
	
	
	/**
	 * This methods is the controller method that wait the turn of the player in the three phase , main and quick action , sell market , buy market.
	 * It calls remote method.
	 * 
	 * @throws RemoteException
	 */
	public void start() throws RemoteException{
		
		
		
		controller.drawCard(6);
		controller.addMoney(9+myID);
		try {
			controller.addAssistant(myID);
		} catch (NotAvailableAssistantsException e) {
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
		
		try {
			Thread.sleep(400);
		} catch (InterruptedException e1) {
			loggerPlayerController.log(Level.INFO, e1.getMessage(), e1);
			Thread.currentThread().interrupt();
		}
		while(!serverController.isEndGame() && !serverController.isEnd()){
			Console.printLine("\nIT'S NOT YOUR TURN, PLEASE WAIT");
			
			while(serverController.playerTurn() != myID){
			
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					loggerPlayerController.log(Level.WARNING, e.getMessage(), e);
					Thread.currentThread().interrupt();
				}
				
			}
			
			
			Console.printLine("---IT'S YOUR TURN, YOU CAN BEGIN---");
			Console.printEmptyLins();
			Console.showRanking(lobbyID);
			Console.printEmptyLins();
			Console.showCardPlayer(controller);
			Console.printEmptyLins();
			Console.getPermitTilePlayer(controller);
			Console.printEmptyLins();
			Console.printLine("You drawed a card of color: "+controller.drawCard());
			Console.printEmptyLins();
			
			mainAndQuickActionController();
			controller.setMainActionDone(false);
			controller.setQuickActionDone(false);
			
			serverController.setEndTurnPlayer(true);
			marketActionController();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				loggerPlayerController.log(Level.WARNING, e.getMessage(), e);
				Thread.currentThread().interrupt();
			}
		}
		Console.showFinalRanking(serverController);
	
	}
	
	
	/**
	 * This method is the controller of the market action it wait his turn and calls method.
	 * @throws RemoteException
	 */
	public void marketActionController() throws RemoteException{
		Console.printLine("	\n--- PLEASE WAIT THE SELL MARKET PHASE ---	\n");
		while((serverController.getTurnIDMarket() != myID || !serverController.getStartMarketSell())&& !serverController.isEndGame()){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				loggerPlayerController.log(Level.INFO, e.getMessage(), e);
				Thread.currentThread().interrupt();
			}
		}
		if(!serverController.isEnd() && !serverController.isEndGame()){
			
			MarketAction.sellPhaseMarket();
			serverController.setStartMarketSell(false);
			
			Console.printLine("\n--- PLEASE WAIT THE BUY MARLET PHASE ---\n");
			while(serverController.getTurnIDMarket() != myID || !serverController.getStartMarketBuy()){
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					loggerPlayerController.log(Level.INFO, e.getMessage(), e);
					Thread.currentThread().interrupt();
				}
			}
			marketActionPlayer.buyPhaseMarket(lobbyID);
			serverController.setStartMarketBuy(false);
		}
		else
			Console.printLine("\n--- MARKET NOT AVAILABLE ---\n");
	}
	
	
	
	
	/**
	 * This  method is the controller of the main and quick action. It cycle until the player has finished the actions.
	 * @throws RemoteException
	 */
	public void mainAndQuickActionController() throws RemoteException{
		boolean quickAction = controller.getQuickActionDone();
		boolean mainAction = controller.getMainActionDone();
		
		
		Scanner input = new Scanner (System.in);
		boolean ok;
		while(!mainAction || !quickAction){

			Console.printEmptyLins();
			Console.printLine("Enter the corresponding value of the action to execute\n"); 
			Console.printEmptyLins();
			if (!mainAction)
				Console.showMainOptionAction();
			Console.printEmptyLins();
			if(!quickAction)
				Console.showQuickAction();
			Console.printEmptyLins();
			Console.showInfoActions(); 
			Console.printEmptyLins();
			
			do{
				ok =true;
				try{
					command = input.next().charAt(0);
					input.nextLine();
				}catch(NoSuchElementException e){
					loggerPlayerController.log(Level.INFO, e.getMessage(), e);
					return;
				}
				if((command<'0' || command>'8') && command!='n' && command!='m' && command!='c' && command!='t'){
					ok=false;
					Console.printLine("\nInput ERROR! please reenter\n");
				}
					

			}while(!ok);

			if(command>='1' && command<='4' && !controller.getMainActionDone()){
				mainActionPlayer.doMainAction(command);
			}
			else if(command == '8' && !controller.getQuickActionDone()){
				Console.printLine("\n--- YOU GOT A MAIN ACTION ADDITIONAL ---\n");
				controller.setMainActionDone(false);
				controller.setQuickActionDone(true);
			}
			else if( (command=='0' ||(command>='5' && command<='7')) && !controller.getQuickActionDone())
				quickActionPlayer.doQuickAction(command);
			
			else if(command=='n' || command=='m' || command=='c' || command=='t'){
				Console.showInfo(command, lobbyID);
			}
			
			else
				Console.printLine("You chose already performed action...please enter the correct one");
		
			quickAction = controller.getQuickActionDone();
			mainAction = controller.getMainActionDone();
			checkFaceUpBonus();
			checkFaceDownBonus();
			checkTokenReward();
		}
	}
	
	
	
	/**
	 * This method checks whether the player has the boolean attribute of the FaceUpBonus to true and than show the tile from which the player has to choose one
	 * @throws RemoteException
	 */
	public void checkFaceUpBonus() throws RemoteException{
		if(controller.checkFaceUpBonus()){
			Console.printLine("\nYou have earned the FaceUpPermitTile bonus, choose which tile to get for free:\n ");
			Console.printLine(controller.showFUTiles());
			Scanner scannerInput = new Scanner(System.in);
			int index = scannerInput.nextInt();
			controller.chooseFaceUpPT(index);
		}
	}
	
	/**
	 * This method checks whether the player has the boolean attribute of the FaceDownBonus to true and than show the tile from which the player has to choose one
	 * @throws RemoteException
	 */
	
	public void checkFaceDownBonus() throws RemoteException {
		if(controller.checkFaceDownBonus()){
			int max = controller.getNumberFdTile();
			int index;
			try {
				Console.printLine("\nYou have earned the FaceDownPermitTile bonus , choose the tile from which earn their bonus:\n ");
				boolean ok;
				Scanner scannerInput;
				do{
					ok = true;
					Console.printLine(controller.showFDTiles());
					scannerInput = new Scanner(System.in);
					index = scannerInput.nextInt();
					if(index<0 && index>max-1){
						ok = false;
						Console.printLine("\nERROR! Enter the correct index\n");
					}
				}while(!ok);

				controller.chooseDownTile(index);
			} catch (CustomException e) {
				loggerPlayerController.log(Level.INFO, e.getDetails(), e);
			}
		}
	}
	
	
	/**
	 * This method check if the player has the value RtBonus > 0 and than show the token from which the player has to choose one
	 * @throws RemoteException
	 */
	
	public void checkTokenReward() throws RemoteException{
		for(int i=0; i<controller.getRtBonus();i++){
			int[] indexArray = controller.geNumberRT();
			if(indexArray.length==0)
				break;
			Console.printLine("\nYou have earned the RewardToken bonus, choose the reward token from which earn their bonus: \n");
			Console.printLine(controller.showRewardTokens());
			boolean ok;
			Scanner scannerInput;
			int index;
			do{
				ok = false;
				scannerInput = new Scanner(System.in);
				index = scannerInput.nextInt();
				for (int inArray: indexArray) {
					if(inArray == index){
						ok=true;
					}
				}
				if (!ok) 
					Console.printLine("\nERROR! Enter the correct index\n");
			}while(!ok);

			controller.chooseRewardTokenBonus(index);

		}
	}











}