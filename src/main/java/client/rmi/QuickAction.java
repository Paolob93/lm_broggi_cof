package client.rmi;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.view.Console;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import common.exception.YouArePoorException;
import common.interfaces.PlayerControllerInterface;

/**
 * This is the quickAction class which contains all the methods of the quick action.
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 */
public class QuickAction {
	
	private static int lobbyID;
	private static PlayerControllerInterface controller;
	private static Logger loggerPlayerController = Logger.getLogger(QuickAction.class.getName());
	private static Scanner scannerInput = new Scanner(System.in);
	private static Map<Character, Method> quickActionMap = new HashMap<>();
	private FileHandler fh;
	
	
	
	/**
	 * The constructor of the class that puts the four quick methods in the Map, and creates the file logger.
	 * @param lobby
	 * @param controllerInt
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public QuickAction(int lobby, PlayerControllerInterface controllerInt) throws NoSuchMethodException{
		lobbyID = lobby;
		controller =controllerInt;
		
		quickActionMap.put('5',QuickAction.class.getMethod("quickActionIngageAssistant"));
		quickActionMap.put('6',QuickAction.class.getMethod("quickActionChangeTiles"));
		quickActionMap.put('7', QuickAction.class.getMethod("quickActionElectCouncillorAssistant"));
		quickActionMap.put('0', QuickAction.class.getMethod("setQuickActionDone"));
		try {
			fh = new FileHandler(QuickAction.class.getName() + ".txt", true);
			fh.setFormatter(new SimpleFormatter());
			loggerPlayerController.addHandler(fh);
			loggerPlayerController.setUseParentHandlers(false);
		} catch (IOException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);;
		}
	}
	
	
	/**
	 * This method calls a remote method of the player controller that set the quick action done boolean.
	 */
	public static void setQuickActionDone(){
		try {
			controller.setQuickActionDone(true);
		} catch (RemoteException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	/**
	 * This method calls a remote method of the player controller that ingage an assistant.
	 * @throws RemoteException
	 */
	public static void quickActionIngageAssistant() throws RemoteException{
		try {
			controller.backup();
			controller.ingageAssistant();
			Console.printLine("--- ASSISTENTE INGAGGIATO ---");
		} catch (NotAvailableAssistantsException | YouArePoorException e) {
			controller.restore();
			Console.printLine(e.getMessage());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * This method calls a remote method of the player controller that ask the player in which region he wants change the tile.
	 * @throws RemoteException
	 */
	public static void quickActionChangeTiles() throws RemoteException{

		Console.showPermitTiles(lobbyID);
		Console.printLine("Inserire la regione in cui cambiare le permitTile - (sea,hill,mountain)");
		String region = scannerInput.nextLine();
		region = region.toUpperCase();

		try {
			controller.backup();
			controller.changeTile(region); 
			Console.printLine("Nuove Permit Tile:");
			Console.showPermitTiles(lobbyID);
		} catch (NotAvailableAssistantsException e) {
			controller.restore();
			Console.printLine(e.getMessage());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}
	}

	
	/**
	 * This method calls a remote method of the player controller that elect a councillor whit using an assistant
	 * @throws RemoteException
	 */
	public static void quickActionElectCouncillorAssistant() throws RemoteException{

		Console.showCouncils(lobbyID, true);
		Console.showCouncillors(lobbyID);
		Console.printLine("Inserire un colore valido");

		String color = scannerInput.nextLine();
		Console.printLine("Inserire una regione valida");
		String region = scannerInput.nextLine();
		color = color.toUpperCase();
		region = region.toUpperCase();

		try {
			controller.backup();
			controller.electCounselorAssistant(color, region);
		} catch (NotAvailableAssistantsException | NotAvailableCouncillorsException | NotAvailableColorCouncillorsException e) {
			controller.restore();
			Console.printLine(e.getMessage());
			loggerPlayerController.log(Level.INFO, e.getMessage(), e);
		}

		
	}

	
	/**
	 * This method get the method from the hashmap and execute it.
	 * @param index
	 */
	public void doQuickAction(char index){
		
		try {
			quickActionMap.get(index).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			loggerPlayerController.log(Level.SEVERE, e.getMessage(), e);
		}
	}


}
