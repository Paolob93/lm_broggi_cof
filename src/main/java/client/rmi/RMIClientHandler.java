package client.rmi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.view.Console;
import client.view.ConsoleUpdate;
import common.exception.CannotLinkCityException;
import common.interfaces.PlayerControllerInterface;
import common.interfaces.ServerControllerInterface;
import common.interfaces.SettingsCreatorInterface;
import common.interfaces.WaitingRoomInterface;
import server.model.Constants;


/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *This is the RMI Cliet Handler class that menaged the connection and the creating of the player controller , and calls remote methods for add the player in the match.
 *
 */
public class RMIClientHandler{
	
	private int myID;
	private String username;
	private int lobbyID;
	private int matchID;
	private PlayerControllerInterface controller;
	private ServerControllerInterface serverController;
	private Logger logger = Logger.getLogger(RMIClientHandler.class.getName());
	private FileHandler fh;
	
	public RMIClientHandler(){
		try {
			   fh = new FileHandler(PlayerController.class.getName() + ".txt", true);
			   fh.setFormatter(new SimpleFormatter());
			   logger.addHandler(fh);
			   logger.setUseParentHandlers(false);
			  } catch (IOException e) {
				 logger.log(Level.SEVERE, e.getMessage(), e);;
			  }
	}
	
	/**
	 * This method asks the player to enter its username.
	 * @throws IOException
	 */
	public void setUsername() throws IOException{
		
		Console.print("INSERT URSERNAME -> ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String inputLine = input.readLine();
		
		username = inputLine;
	}
	

	/**
	 * This method , whit remote methods, create and manages the start of the the match.
	 * Wait that the creation of the lobby and match class and wait that the player 1 finish the input of the parameters.
	 * @throws RemoteException
	 */
	public void setClient() throws RemoteException {
		
		Registry registry;
		registry = LocateRegistry.getRegistry(1099);
	
		WaitingRoomInterface lobby;
		try {
			lobby = (WaitingRoomInterface) registry.lookup("waitingRoom");
			int activePlayer  = lobby.getActiveNumPlayer();
			if(activePlayer == 8){
				lobby.resetRoom();
			}
			lobby.incrementPlayer();
			myID = lobby.getActiveNumPlayer();
			Console.printLine("You are the player number "+myID);
			Console.printLine("\nWaiting for the match to start.\n");
			lobby.setLobby(myID);
			lobbyID = lobby.getLobbyID();
			ConsoleUpdate consoleUpdate = new ConsoleUpdate();
			registry.bind("consoleUpdate"+lobbyID+myID, consoleUpdate);
			if(myID==1){
				addParameters();
				lobby.setCanContinue(true);
				Console.printLine("The match will start soon.\n");
			}
			
			SettingsCreatorInterface settingsCreator=(SettingsCreatorInterface)registry.lookup("settings"+lobbyID);
			while(!settingsCreator.getStart() || !lobby.getCanStart()){
				Thread.sleep(100);
			}
			insertUsernamePlayerCreation(lobby);
			matchID = lobby.getMatchID();
			Console.printLine("\nThe match number "+matchID+" will start soon.\n");
		} catch (NotBoundException | CannotLinkCityException | AlreadyBoundException | InterruptedException e1) {
			logger.log(Level.INFO, e1.getMessage(), e1);
		}
		
	}
	
	
	/**
	 * This method try to create a player controller until the username is different from that of the other player.
	 * @param lobby
	 * @throws NotBoundException
	 */
	public void insertUsernamePlayerCreation(WaitingRoomInterface lobby) throws NotBoundException{

		boolean flag = true;
		while(flag){
			try{
				setUsername();
				lobby.createPlayerControllerServer(username, myID,lobbyID);
				flag = false;
			}catch (AlreadyBoundException | IOException e){
				logger.log(Level.INFO,e.getMessage() , e);
				Console.printLine("This UserName has already been choosen, please try again.");

			}
		}
	}
	
	
	
	/**
	 * This is the method that allows the player 1 to insert all the parameters calling remote methods.
	 */
	public void addParameters(){
		
		Registry registry;
		SettingsCreatorInterface settingsCreator;
		try {
			
			Console.printLine("Enter a value between 1 and "+Constants.MAX_VALUE_INPUT_BONUS);
			registry = LocateRegistry.getRegistry(1099);
			settingsCreator=(SettingsCreatorInterface)registry.lookup("settings"+lobbyID);
			
			int value = checkNumber("Insert the maximum number of bonuses that you want on a Permit Tile.");
			settingsCreator.setMaxBonusNumberPT(value);

			value = checkNumber("Insert the maximum value that a bonus on a Permit Tile could have.");
			settingsCreator.setMaxValueBonusPT(value);

			value = checkNumber("Insert the maximum number of bonuses that you want on every Tile of the Nobility Track");
			settingsCreator.setMaxBonusNumberNT(value);

			value = checkNumber("Insert the maximum value that a bonus on a Nobility Track could have.");
			settingsCreator.setMaxValueBonusNT(value);

			value = checkNumber("Insert the maximum number of bonuses that you want on a Reward Token");
			settingsCreator.setMaxBonusNumberRT(value);

			value = checkNumber("Insert the maximum value that a bonus on a Reward Token could have.");
			settingsCreator.setMaxValueBonusRT(value);

			settingsCreator.setStart(true);
		} catch (NotBoundException | RemoteException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}
	
	/**
	 * This method check if the value insert is correct
	 * @param message
	 * @return
	 */
	public int checkNumber(String message){
		Scanner input = new Scanner(System.in);
		int value = 0;
		Console.printLine(message);
		try{
			value = input.nextInt();		
			if(value<=0 || value>Constants.MAX_VALUE_INPUT_BONUS)
				throw new InputMismatchException();
		}	
		catch(InputMismatchException e){
			Console.printLine("Input ERROR!! Enter a value between 1 and "+Constants.MAX_VALUE_INPUT_BONUS);
			logger.log(Level.INFO, e.getMessage(), e);
			checkNumber(message);
		}
		return value;
	}


	/**
	 * This is the method that start the player controller 
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public void startMatch() throws RemoteException{

		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(1099);
			controller = (PlayerControllerInterface) registry.lookup(username);
			serverController = (ServerControllerInterface) registry.lookup("serverController"+lobbyID);
		} catch (RemoteException | NotBoundException e2) {
			logger.log(Level.INFO, e2.getMessage(), e2);
		}
		try {
			PlayerController controllerGame = new PlayerController(controller,serverController,myID,lobbyID);
			if(myID==1){
				
				
				/** This thread when all the players have been created starts the server controller */
				Thread thread = new Thread(){

					@Override
					public void run(){
						try {
							serverController.checkPlayers();
							serverController.setTurn();
						} catch (RemoteException | InterruptedException   e) {
							logger.log(Level.INFO, e.getMessage(), e);
						}
					}
				};
				thread.start();
			}

			while(!serverController.isStart()){
				//waiting for all the players have set their username
				Thread.sleep(500);
			}
			controllerGame.start();
		} catch (IllegalArgumentException | NullPointerException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
		catch (NoSuchMethodException | SecurityException e1) {
			logger.log(Level.INFO, e1.getMessage(), e1);
		} catch (NotBoundException e3) {
			logger.log(Level.INFO, e3.getMessage(), e3);
		} catch (InterruptedException e) {
			logger.log(Level.INFO, e.getMessage(), e);
			Thread.currentThread().interrupt();
		}

	}

}
