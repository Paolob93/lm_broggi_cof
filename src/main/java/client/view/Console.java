package client.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.interfaces.LobbyInterface;
import common.interfaces.PlayerControllerInterface;
import common.interfaces.ServerControllerInterface;

/**
 * The Console class.
 * 
 * The class that is used to write and read to the user input stream. 
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Console {

	private static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	private static Logger logger = Logger.getLogger(Console.class.getName());
	private static final String LOGO = "\n"+" #####  ####### #     # #     #  #####  ### #          ####### #######    ####### ####### #     # ######\n" +
											"#     # #     # #     # ##    # #     #  #  #          #     # #          #       #     # #     # #     #\n"+ 
											"#       #     # #     # # #   # #        #  #          #     # #          #       #     # #     # #     #\n"+
											"#       #     # #     # #  #  # #        #  #          #     # #####      #####   #     # #     # ######\n" +
											"#       #     # #     # #   # # #        #  #          #     # #          #       #     # #     # #   #\n"+
											"#     # #     # #     # #    ## #     #  #  #          #     # #          #       #     # #     # #    #\n"+
											" #####  #######  #####  #     #  #####  ### #######    ####### #          #       #######  #####  #     #";
	private Console(){}

	/**
	 * The method that shows the Main Actions.
	 */
	public static void showMainOptionAction(){
		Console.printLine("Main Actions");
		Console.printLine("1 - Elect a Councillor");
		Console.printLine("2 - Acquire a Business Permit Tile");
		Console.printLine("3 - Build an Emporium using a Permit Tile");
		Console.printLine("4 - Build an Emporium with the help of the King");
	}

	/**
	 * The method that shows the Quick Actions.
	 */
	public static void showQuickAction(){

		Console.printLine("Quick Actions");
		Console.printLine("5 - Engage an Assistant");
		Console.printLine("6 - Change Business Permit Tile");
		Console.printLine("7 - Send an Assistant to Elect a Councillor");
		Console.printLine("8 - Perform an Additional Main Action");
		Console.printLine("0 - I don't want to perform a Quick Action");

	}

	/**
	 * The method that shows the Informative Actions.
	 */
	public static void showInfoActions(){
		Console.printLine("n - See the Nobility Track");
		Console.printLine("m - See the Map");
		Console.printLine("c - See the Councils");
		Console.printLine("t - See the Business Permit Tiles");
	}

	/**
	 * The method that shows the sell Market Actions.
	 */
	public static void showActionMarketSell(){
		Console.printLine("What do you want to sell?");
		Console.printLine("1 - Politic Card");
		Console.printLine("2 - Permit Tile");
		Console.printLine("3 - Assistant");
		Console.printLine("0 - I don't want to sell anything");
		Console.printLine("Insert the number corrisponding to what you want to do.");
	}

	/**
	 * The method that shows the cards owned by a Player.
	 * @param controller
	 */
	public static void showCardPlayer(PlayerControllerInterface controller){

		String[] colors;
		try {
			colors = controller.getColorCard();
			Console.printLine("----OWNED CARDS----");
			for(String color : colors)
				Console.print(color+"\t");
			Console.printLine("");
		} catch (RemoteException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows the Item that a Player could sell in the Market phase.
	 * @param controller
	 * @throws RemoteException
	 */
	public static void showItemPlayerMarket(PlayerControllerInterface controller) throws RemoteException{

		String[] colors = controller.getColorCard();

		for(String card : colors){
			Console.print(card+"\t");
		}
		Console.printLine("");

		String[] tilePlayer = controller.getPlayerPermitTile();

		for(String tile: tilePlayer)
			Console.printLine(tile);

		Console.printLine("Assistants number: "+controller.getNumAssistant());
	}

	/**
	 * The method that shows all the available Permit Tiles owned by a Player.
	 * @param controller
	 * @return
	 * @throws RemoteException
	 */
	public static int getPermitTilePlayer(PlayerControllerInterface controller) throws RemoteException{

		String[] tilePlayer = controller.getPlayerPermitTile();

		for(String tile: tilePlayer)
			Console.printLine(tile);
		return controller.getPlayerPermitTile().length;
	}

	/**
	 * The method that shows the Nobility Track of the Lobby.
	 * @param lobbyID
	 */
	public static void showNobilityTrack(int lobbyID) {

		Registry registry;
		LobbyInterface lobby ;
		try {
			registry = LocateRegistry.getRegistry(1099);
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			//Console.printLine("Position 0 -");
			Console.printLine(lobby.getNobilityTrack());
		} catch (RemoteException | NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows all the Councils.
	 * @param lobbyID
	 * @param king
	 */
	public static void showCouncils(int lobbyID, boolean king){
		try{
			Registry registry = LocateRegistry.getRegistry(1099);

			LobbyInterface lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);

			printLine("Sea council:");
			String[] colors = lobby.showCouncilsGame("SEA");
			for(String s: colors)
				printLine("\t"+s);

			printLine("Hill council:");
			colors = lobby.showCouncilsGame("HILL");
			for(String s: colors)
				printLine("\t"+s);

			printLine("Mountain council:");
			colors = lobby.showCouncilsGame("MOUNTAIN");
			for(String s: colors)
				printLine("\t"+s);
			if(king){
				printLine("King's council:");
				colors = lobby.showCouncilsGame("KING");
				for(String s: colors)
					printLine("\t"+s);
			}
		}catch(RemoteException| NotBoundException e){
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows the map.
	 * @param lobbyID
	 */
	public static void showMap(int lobbyID) {
		Registry registry;
		LobbyInterface lobby;
		try {
			registry = LocateRegistry.getRegistry(1099);
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			List<String> cities;
			printLine("Sea: ");
			cities = lobby.showCitiesRegion("SEA");
			for(String s: cities)
				printLine(s);
			printLine("Hill: ");
			cities = lobby.showCitiesRegion("HILL");
			for(String s: cities)
				printLine(s);
			printLine("Mountain: ");
			cities = lobby.showCitiesRegion("MOUNTAIN");
			for(String s: cities)
				printLine(s);
		} catch (RemoteException | NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows the 6 Business Permit Tiles.
	 * @param lobbyID
	 * @throws RemoteException
	 */
	public static void showPermitTiles(int lobbyID) throws RemoteException{

		Registry registry = LocateRegistry.getRegistry(1099);

		LobbyInterface lobby;
		try {
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			String[] tile;
			printLine("Sea Permit Tiles: \n");
			tile = lobby.showPermitTiles("SEA");
			for(int i=0;i<2;i++){
				printLine("Tile["+i+"]");
				printLine(tile[i]);
			}

			printLine("Hill Permit Tiles: \n");
			tile = lobby.showPermitTiles("HILL");
			for(int i=0;i<2;i++){
				printLine("Tile["+i+"]");
				printLine(tile[i]);
			}

			printLine("Mountain Permit Tiles: \n");
			tile = lobby.showPermitTiles("MOUNTAIN");
			for(int i=0;i<2;i++)
				printLine("Tile["+i+"]"+tile[i]);
		} catch (NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows all the Items that are available in the market.
	 * @param lobbyID
	 */
	public static void showItemInMarket(int lobbyID) {
		Registry registry;
		LobbyInterface lobby;
		try {
			registry = LocateRegistry.getRegistry(1099);
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			printLine("Available items in the market:");
			String item = lobby.showItemMarket();
			printLine(item);
		} catch (RemoteException | NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that reads a String.
	 * @return
	 */
	public static String read(){
		String inputString = "";
		try {
			inputString = input.readLine();
		} catch (IOException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
		return inputString;
	}

	/**
	 * The method that reads an int.
	 * @return
	 */
	public static int readInt(){
		int inputNum = 0;
		try {
			inputNum = input.read();
		} catch (IOException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
		return inputNum;
	}

	/**
	 * A custom printer used to print an empty Line.
	 */
	public static void printEmptyLins(){
		System.out.println();
	}
	/**
	 * A custom printer that print the parameter as new Line.
	 * @param message
	 */
	public static void printLine(String message){
		System.out.println(message);
	}

	/**
	 * A custom printer.
	 * @param message
	 */
	public static void print(String message){
		System.out.print(message);
	}

	/**
	 * The method that shows the status of all the players.
	 * @param lobbyID
	 */
	public static void showRanking(int lobbyID) {

		Registry registry;
		LobbyInterface lobby;
		try {
			registry = LocateRegistry.getRegistry(1099);
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);

			String[] info = lobby.getStatusPlayer();
			for(String s: info)
				printLine(s);
		} catch (RemoteException | NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows the Councillors still available in the GameBoard.
	 * @param lobbyID
	 */
	public static void showCouncillors(int lobbyID){
		try{
			Registry registry = LocateRegistry.getRegistry(1099);

			LobbyInterface lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			printLine("\nAvailable councillors:\n ");
			String[] colors = lobby.getColorStringAvaiableCounselor();
			for(String s: colors)
				printLine(s);
		}
		catch(RemoteException| NotBoundException e){
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that shows the King's council.
	 * @param lobbyID
	 * @throws RemoteException
	 */
	public static void showKingCouncil(int lobbyID) throws RemoteException{

		Registry registry = LocateRegistry.getRegistry(1099);

		LobbyInterface lobby;
		try {
			lobby = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			printLine("King's council:");
			String[] colors = lobby.showCouncilsGame("KING");
			for(String s: colors)
				printLine("\t"+s);
		} catch (NotBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that signals the end of the game.
	 * @param controllerServer
	 * @throws RemoteException
	 */
	public static void showFinalRanking(ServerControllerInterface controllerServer) throws RemoteException{
		printLine("--- MATCH ENDED ---");
		String winners = controllerServer.getWinner();
		printLine(winners);
	}


	/**
	 * This method show some info of the match
	 * @param input
	 * @param lobbyID
	 * @throws RemoteException
	 */
	public static void showInfo(char input , int lobbyID) throws RemoteException{
		if(input=='n')
			showNobilityTrack(lobbyID);
		else if (input == 'm')
			showMap(lobbyID);
		else if  (input == 'c')
			showCouncils(lobbyID,true);
		else if(input == 't')
			showPermitTiles(lobbyID);
	}
	

	public static void printLOGO(){
		System.out.println(LOGO);
	}





}









