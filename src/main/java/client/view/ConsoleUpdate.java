package client.view;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import client.Client;
import client.rmi.MainAction;
import common.interfaces.ConsoleUpdateInterface;

/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 */
public class ConsoleUpdate extends UnicastRemoteObject implements ConsoleUpdateInterface{

	
	private static final long serialVersionUID = 2648204300927336693L;
	private transient FileHandler fh;
	private transient Logger loggerConsoleUpdate = Logger.getLogger(ConsoleUpdate.class.getName());


	/**
	 * The constructor of the class that creates a file logger.
	 * @throws RemoteException
	 */
	public ConsoleUpdate() throws RemoteException{
		try {
			fh = new FileHandler(MainAction.class.getName() + ".txt", true);
			fh.setFormatter(new SimpleFormatter());
			loggerConsoleUpdate.addHandler(fh);
			loggerConsoleUpdate.setUseParentHandlers(false);
		} catch (IOException e) {
			loggerConsoleUpdate.log(Level.SEVERE, e.getMessage(), e);
			
		}
	}
	
	/**
	 * This method notifies the player of the disconnection of another player
	 */
	@Override
	public void playerDisconnectedNotifycation(int index){
		Console.printLine("The player "+index+" has been disconnected from the server");
	} 
	
	/**
	 * This method close the system.in of the player-console when the player is disconnected or its time-turn is over.
	 */
	@Override
	public void kill(){
		try {
			System.in.close();
			
		} catch (IOException e) {
			loggerConsoleUpdate.log(Level.SEVERE, e.getMessage(), e);
			
		}finally{
			Console.printLine("YOU HAVE BEEN DISCONNECTED FROM THE SERVER");
			Console.printLine("EVERYTHING YOU WILL INSERT WILL BE USELESS");
			Client.killMe();
		}
	}
	
	
}
