package common.exception;

/**
 * The AssistantRuntimeException.
 * 
 * It is called when there aren't available Assistants in the GameBoard.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class AssistantRuntimeException extends RuntimeException {	
	
	private static final long serialVersionUID = -6479994633006008828L;
	private static final String DETAILS = "There aren't available Assistants.";
	
	public AssistantRuntimeException(NotAvailableAssistantsException e) {
	}

	public static String getDetails() {
		return DETAILS;
	}

}
