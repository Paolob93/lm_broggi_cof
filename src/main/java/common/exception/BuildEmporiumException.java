package common.exception;

/**
 * The BuildEmporiumException.
 * 
 * It is called when the Player cannot build an Emporium in the selected City.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class BuildEmporiumException extends CustomException {
	
	private static final long serialVersionUID = -6977686495327478241L;
	private static final String DETAILS = "It wan't possible to build an emporium here.";

	@Override
	public String getDetails() {
		return DETAILS;
	}
}
