package common.exception;

/**
 * The BuildEmporiumException.
 * 
 * It is called when someone tries to link a City with itself.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class CannotLinkCityException extends CustomException {
	
	private static final long serialVersionUID = 3383802669540883818L;
	private static final String DETAILS = "It wasn't possible to link these cities.";

	@Override
	public String getDetails() {
		return DETAILS;
	}
}
