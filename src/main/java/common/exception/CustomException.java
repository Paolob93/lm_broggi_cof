package common.exception;

/**
 * The CustomException.
 * 
 * It's a custom Exception that has a custom method: getDetails.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class CustomException extends Exception {
	
	private static final long serialVersionUID = -1997069109076644065L;

	public CustomException(){
		super();
	}
	public CustomException(Exception e) {
		super(e.getMessage());
	}
	
	public String getDetails() {
		return new String();
	}

	
}
