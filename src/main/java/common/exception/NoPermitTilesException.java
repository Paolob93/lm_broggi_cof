package common.exception;

/**
 * The NoPermitTilesException.
 * 
 * It's an Exception thrown when a Player does not have any PermitTiles and tries to gain the FaceUpPermitTileBonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NoPermitTilesException extends CustomException {

	private static final long serialVersionUID = -1907405198382841309L;
	private static final String DETAILS = "You don't have Permit Tiles.";

	@Override
	public String getDetails() {
		return DETAILS;
	}
}
