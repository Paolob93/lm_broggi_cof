package common.exception;

/**
 * The NotAvailableAssistantsException.
 * 
 * It is called when there aren't available Assistants in the GameBoard.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NotAvailableAssistantsException extends CustomException {
	
	private static final long serialVersionUID = 710366229318243184L;
	private static final String DETAILS ="There aren't available Assistants.";
	

	public NotAvailableAssistantsException() {
		super();
	}

	public NotAvailableAssistantsException(NotAvailableAssistantsException e) {
		super(e);
	}

	@Override
	public String getDetails() {
		return DETAILS;
	}

}
