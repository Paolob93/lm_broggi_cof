package common.exception;

/**
 * The NotAvailableColorCouncillorsException.
 * 
 * It is called when the Player try to elect a councillor but there aren't councillors available of the chosen color.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NotAvailableColorCouncillorsException extends CustomException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4985482211882425462L;
	private static final String DETAILS = "There aren't available councillors of the selected color.";

	@Override
	public String getDetails() {
		return DETAILS;
	}
}
