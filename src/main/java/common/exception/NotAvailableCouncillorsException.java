package common.exception;

/**
 * The NotAvailableCouncillorsException.
 * 
 * It is called when the Player try to elect a councillor but there aren't councillors available.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NotAvailableCouncillorsException extends CustomException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6564226590759178471L;
	private static final String DETAILS = "There aren't available councillors.";
	
	@Override
	public String getDetails() {
		return DETAILS;
	}
}
