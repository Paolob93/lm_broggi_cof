package common.exception;

/**
 * The WrongCardsException.
 * 
 * It is called when the Player try to buy a PermitTile with the wrong cards.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class WrongCardsException extends CustomException {
	
	private static final long serialVersionUID = 7424015441916333100L;
	
	private static final String DETAILS = "You chose the wrong cards.";

	@Override
	public String getDetails() {
		return DETAILS;
	}

}
