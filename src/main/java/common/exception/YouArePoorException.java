package common.exception;

/**
 * The YouArePoorException.
 * 
 * It is called when the Player hasn't enough money to buy something.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class YouArePoorException extends CustomException {
	
	private static final long serialVersionUID = -5430882077080466480L;

	private static final String DETAILS = "You don't have enough money to perform this action.";

	@Override
	public String getDetails() {
		return DETAILS;
	}
}
