package common.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *
 *
 *The interface ConsoleUpdateInterface.
 *The server controller calls this class.
 *This interface notify the player of the disconnection of other player.
 *Close the System.in of the player that has been disconnected. 
 */
public interface ConsoleUpdateInterface extends Remote {

	
	/**
	 * The method print the player which has disconnected
	 * @param index
	 * @throws RemoteException
	 */
	public void playerDisconnectedNotifycation(int index) throws RemoteException;
	
	/**
	 * The method close the System.in of the player disconnected.
	 * @throws RemoteException
	 */
	public void kill()  throws RemoteException;
}
