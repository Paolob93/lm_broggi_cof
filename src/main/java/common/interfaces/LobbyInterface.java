package common.interfaces;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *The interface LobbyInterface.
 *Create the player controller and return to it with the methods some match info.
 */
public interface LobbyInterface extends Remote{

	/**
	 * Handle the creation of the player controller, it receive the username and the id.
	 * @param username
	 * @param myID
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 */
	public void createPlayerController(String username, int myID) throws RemoteException;


	/**
	 * Returns the number of the player store into the array of player PlayerList.
	 * @return
	 * @throws RemoteException
	 */
	public int getNumPlayerList() throws RemoteException;

	/**
	 * Returns an array of string.
	 * Each string contains the info of the player, money, VP, numEmporiums and the number of assistants.
	 * @return
	 * @throws RemoteException
	 */
	public String[] getStatusPlayer() throws RemoteException;
	
	
	/**
	 * Returns an array of string.
	 * Each string contains the color of the avaiable councillor.
	 * @return
	 * @throws RemoteException
	 */
	public String[] getColorStringAvaiableCounselor() throws RemoteException;
	
	/**
	 * Returns an array of string.
	 * Each String contains the colors of the councillors in the councils.
	 * The interface receive the region of which show the council.
	 * @param region
	 * @return
	 * @throws RemoteException
	 */
	public String[] showCouncilsGame(String region) throws RemoteException;
	
	/**
	 * 
	 * 
	 * Returns an array of string.
	 * Each String contains the details of the permitTile in the region.
	 * The interface receive the region of which show the tiles.
	 * @param region
	 * @return
	 * @throws RemoteException
	 */
	public String[] showPermitTiles(String region) throws RemoteException;
	
	/**
	 * Returns an array List of string.
	 * Each elements is a string which contains all the details of the city.
	 * The interface receive the region of which show the cities.
	 * 
	 * @param region
	 * @return
	 * @throws RemoteException
	 */
	public List<String> showCitiesRegion(String region) throws RemoteException; 
	
	/**
	 * Returns a String.
	 * The String contains all the Item in the market.
	 * @return
	 * @throws RemoteException
	 */
	public String showItemMarket() throws RemoteException;
	
	/**
	 * Returns a string.
	 * The String contains all the position and their details(bonus in each position) of the nobility track.
	 * @return
	 * @throws RemoteException
	 */
	public String getNobilityTrack() throws RemoteException;



}
