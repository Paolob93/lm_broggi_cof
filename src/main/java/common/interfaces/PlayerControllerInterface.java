package common.interfaces;

import java.rmi.*;

import common.exception.BuildEmporiumException;
import common.exception.NoPermitTilesException;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import common.exception.WrongCardsException;
import common.exception.YouArePoorException;



/**
 * The Player Controller Server Interface
 * Contains all of the methods that the player controller on the cliet can call.
 * @author DavideDamonti
 * @author PaoloBroggi
 */
public interface PlayerControllerInterface extends Remote{
	/**
	 * Return the last drawed card.
	 * @return
	 * @throws RemoteException
	 */
	public String drawCard() throws RemoteException;
	/**
	 * This method allows the player to elect a councillor.
	 * @param color
	 * @param region
	 * @throws NotAvailableCouncillorsException
	 * @throws RemoteException
	 * @throws NotAvailableColorCouncillorsException
	 */
	public void electCouncillor(String color, String region) throws NotAvailableCouncillorsException, RemoteException, NotAvailableColorCouncillorsException;
	/**
	 * This method allow the player to elect a councillor in the King board council
	 * @param color
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 * @throws RemoteException
	 */
	public void electKingCouncillor(String color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, RemoteException;
	/**
	 * This  method allows the player to add a card into an array list for satisfy a council
	 * @param color
	 * @throws RemoteException
	 * @throws WrongCardsException 
	 */
	public void addCard(String[] color) throws RemoteException, WrongCardsException;
	/**
	 * c build an Emporium
	 * @param city
	 * @param index
	 * @throws RemoteException
	 * @throws BuildEmporiumException
	 * @throws NotAvailableAssistantsException
	 */
	public void buildEmporium(String city, int index) throws RemoteException, BuildEmporiumException, NotAvailableAssistantsException;
	/**
	 * This method allows the player to buy a permit tile.
	 * @param region
	 * @param position
	 * @throws YouArePoorException
	 * @throws WrongCardsException
	 * @throws RemoteException
	 */
	public void buyPermitTile(String region, int position) throws YouArePoorException, WrongCardsException,RemoteException;
	/**
	 * This method allows the player to build an emporium whit the help of the king
	 * @param city
	 * @throws RemoteException
	 * @throws YouArePoorException
	 * @throws WrongCardsException
	 * @throws NotAvailableAssistantsException
	 * @throws BuildEmporiumException
	 */
	public void buildEmporiumKing(String city) throws RemoteException, YouArePoorException, WrongCardsException, NotAvailableAssistantsException, BuildEmporiumException;
	/**
	 * This method does a backup of the most vulnerable parameters and object befor call a method that can be interrupt meanwhile
	 * @throws RemoteException
	 */
	public void backup() throws RemoteException;
	/**
	 * This method restores the backup
	 * @throws RemoteException
	 */
	public void restore() throws RemoteException;
	/**
	 * This method allows the player to ingage an assistant
	 * @throws NotAvailableAssistantsException
	 * @throws YouArePoorException
	 * @throws RemoteException
	 */
	public void ingageAssistant() throws NotAvailableAssistantsException, YouArePoorException, RemoteException;
	/**
	 * This method allows the player to change the tile on a region
	 * @param region
	 * @throws NotAvailableAssistantsException
	 * @throws RemoteException
	 */
	public void changeTile(String region) throws NotAvailableAssistantsException, RemoteException;
	/**
	 * This method allows the player to elect a coucillor whit an assistant
	 * @param color
	 * @param region
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 * @throws NotAvailableAssistantsException
	 * @throws RemoteException
	 */
	public void electCounselorAssistant(String color, String region) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException, RemoteException;
	/**
	 * Return the boolean of the quick action attribute of the player
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean getQuickActionDone() throws RemoteException;
	/**
	 * Return the boolean of the main action attribute of the player
	 * @return
	 * @throws RemoteException
	 */
	public boolean getMainActionDone() throws RemoteException;
	/**
	 * Set the boolean of the quick action attribute of the player
	 * @param done
	 * @throws RemoteException
	 */
	public void setQuickActionDone(boolean done) throws RemoteException;
	/**
	 * Set the boolean of the main action attribute of the player
	 * @param done
	 * @throws RemoteException
	 */
	public void setMainActionDone(boolean done) throws RemoteException;
	/**
	 * This method draw a num, as parameter, of card at the beginnig of the match
	 * @param num
	 * @throws RemoteException
	 */
	public void drawCard(int num) throws RemoteException;
	/**
	 * Get and array of string that contains the colors of the player's cards 
	 * @return
	 * @throws RemoteException
	 */
	public String[] getColorCard() throws RemoteException;
	/**
	 * Get and array of string that contains the details of the player's permit tiles
	 * @return
	 * @throws RemoteException
	 */
	public String[] getPlayerPermitTile() throws RemoteException;
	/**
	 * Get the num of player's assistant
	 * @return
	 * @throws RemoteException
	 */
	public int getNumAssistant() throws RemoteException;
	/**
	 * Allow the player to sell a politic card
	 * @param color
	 * @param price
	 * @throws RemoteException
	 */
	public void sellPoliticCard(String color, int price) throws RemoteException;
	/**
	 * Allow the player to sell a permit tile
	 * @param index
	 * @param price
	 * @throws RemoteException
	 */
	public void sellPermitTile(int index, int price ) throws RemoteException;
	/**
	 * Allow the player to sell an assistant
	 * @param price
	 * @throws NotAvailableAssistantsException
	 * @throws RemoteException
	 */
	public void sellAssistant(int price) throws NotAvailableAssistantsException,RemoteException;
	/**
	 * Allow the player to buy an item from the market
	 * @param index
	 * @throws YouArePoorException
	 * @throws RemoteException
	 */
	public void buyItem(int index) throws YouArePoorException, RemoteException;
	/**
	 * Allow the player to clear the temporary array list of cards before fill whit other cards
	 * @throws RemoteException
	 */
	public void clearHand() throws RemoteException;
	/**
	 * Give money to the player.
	 * @param money
	 * @throws RemoteException
	 */
	public void addMoney(int money) throws RemoteException;
	/**
	 * Add an assistant to the play, increment its counter
	 * @param num
	 * @throws RemoteException
	 * @throws NotAvailableAssistantsException
	 */
	public void addAssistant(int num) throws RemoteException , NotAvailableAssistantsException;
	/**
	 * This methods check the boolean attribute of the FaceUpBonus
	 * @return
	 * @throws RemoteException
	 */
	public boolean checkFaceUpBonus() throws RemoteException;
	/**
	 * Return a string that contains the details of the tile for this bonus
	 * @return
	 * @throws RemoteException
	 */
	public String showFUTiles() throws RemoteException;
	/**
	 * This method allow the player to choose which tile obtain
	 * @param index
	 * @throws RemoteException
	 */
	public void chooseFaceUpPT(int index) throws RemoteException;
	/**
	 * This methods check the boolean attribute of the FaceDownBonus
	 * @return
	 * @throws RemoteException
	 */
	public boolean checkFaceDownBonus() throws RemoteException;
	/**
	 * Return a string that contains the details of the tile for this bonus
	 * @return
	 * @throws NoPermitTilesException
	 * @throws RemoteException
	 */
	public String showFDTiles() throws NoPermitTilesException, RemoteException;
	/**
	 * This method allow the player to choose which tile from that obtain their bonus
	 * @param index
	 * @throws NoPermitTilesException
	 * @throws RemoteException
	 */
	public void chooseDownTile(int index) throws NoPermitTilesException, RemoteException;
	/**
	 * This methods check the RtBonus is > 0
	 * @return
	 * @throws RemoteException
	 */
	public boolean checkTokenReward() throws RemoteException;
	/**
	 * Return a string that contains the details of the bonus's token
	 * @return
	 * @throws RemoteException
	 */
	public String  showRewardTokens() throws RemoteException;
	/**
	 * This method allow the player to choose from which token obtain the bonus
	 * @param index
	 * @throws RemoteException
	 */
	public void chooseRewardTokenBonus(int index) throws RemoteException;

	/**
	 * Get the value of RtBonus
	 * @return
	 * @throws RemoteException
	 */
	public int getRtBonus() throws RemoteException;
	/**
	 * Get the number of the avaiable FDTiles
	 * @return
	 * @throws RemoteException
	 */
	public int getNumberFdTile() throws RemoteException;
	/**
	 * Get an array of int which contanis all the index of availables RewardToken
	 * @return
	 * @throws RemoteException
	 */
	public int[] geNumberRT() throws RemoteException;
	
}

