package common.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *The Server controller Interface.
 *This interface is used to start the controller of the server, and for set and get from the player the booleans
 */
public interface ServerControllerInterface extends Remote {
	
	/**
	 * Get the index of the turn if the player who has to play 
	 * @return
	 * @throws RemoteException
	 */
	public int playerTurn() throws RemoteException;
	/**
	 * This method set the End Turn boolean when the player end his action
	 * @param endTurn
	 * @throws RemoteException
	 */
	public void setEndTurnPlayer(boolean endTurn) throws RemoteException;
	/**
	 * This method start the server controller which manage the turns of the match
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public void setTurn() throws RemoteException ,InterruptedException;
	/**
	 * Set a Start boolean, this allow the player 1 in rmiclienthandler to start the servercontroller
	 * @param start
	 * @throws RemoteException
	 */
	public void setStart(boolean start) throws RemoteException;
	/**
	 * Get a boolean of the start attribute
	 * @return
	 * @throws RemoteException
	 */
	public boolean isStart() throws RemoteException;
	/**
	 * Get the boolean EndPlayer attribute. 
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public boolean getEndPlayer() throws RemoteException;
	/**
	 * This method is used to wait that all the player are added to the PlayerList
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public void checkPlayers() throws RemoteException, InterruptedException;
	/**
	 * Get the boolean of the StartMarketAttribute
	 * @return
	 * @throws RemoteException
	 */
	public boolean getStartMarketSell() throws RemoteException;
	/**
	 * This method is used to know when a player biul 10 emporiums
	 * @return
	 * @throws RemoteException
	 */
	public boolean isEnd() throws RemoteException;
	/**
	 * This method retrun a boolean, is used to know when all the player have terminate their last turn
	 * @return
	 * @throws RemoteException
	 */
	public boolean isEndGame() throws RemoteException;
	/**
	 * Set this boolean when a player start his turn of the sell phase market
	 * @param start
	 * @throws RemoteException
	 */
	public void setStartMarketSell(boolean start) throws RemoteException;
	/**
	 * get the turn player that has to play the market phase
	 * @return
	 * @throws RemoteException
	 */
	public int getTurnIDMarket() throws RemoteException;
	/**
	 * Get the boolean of the StartMarketBuy boolean
	 * @return
	 * @throws RemoteException
	 */
	public boolean getStartMarketBuy() throws RemoteException;
	/**
	 * This methods set a boolean of the attribute when a player start the buy phase market turn
	 * @param start
	 * @throws RemoteException
	 */
	public void setStartMarketBuy(boolean start) throws RemoteException;
	
	/**
	 * This meethod returns a string that contains the final ranking
	 * @return
	 * @throws RemoteException
	 */
	public String getWinner() throws RemoteException;
	
	}
