package common.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author DavideDamonti
 *@author PaoloBroggi
 */
public interface SettingsCreatorInterface extends Remote{
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxBonusNumberRT
	 * @throws RemoteException
	 */
	public void setMaxBonusNumberRT(int maxBonusNumberRT) throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxValueBonusRT
	 * @throws RemoteException
	 */
	public void setMaxValueBonusRT(int maxValueBonusRT) throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxBonusNumberPT
	 * @throws RemoteException
	 */
	public void setMaxBonusNumberPT(int maxBonusNumberPT) throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxValueBonusPT
	 * @throws RemoteException
	 */
	public void setMaxValueBonusPT(int maxValueBonusPT)throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxCitiesInPT
	 * @throws RemoteException
	 */
	public void setMaxCitiesInPT(int maxCitiesInPT)throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxBonusNumberNT
	 * @throws RemoteException
	 */
	public void setMaxBonusNumberNT(int maxBonusNumberNT)throws RemoteException;
	/**
	 * This method is used from the player 1 to set the parameter of the match
	 * @param maxValueBonusNT
	 * @throws RemoteException
	 */
	public void setMaxValueBonusNT(int maxValueBonusNT)throws RemoteException;
	/**
	 * This method is used from the player 1 to set the boolean when finishes to insert the parameters
	 * @param start
	 * @throws RemoteException
	 */
	public void setStart(boolean start) throws RemoteException;
	/**
	 * This method is used from the players to know when the player 1 finish to insert the parameters
	 * @return
	 * @throws RemoteException
	 */
	public boolean getStart() throws RemoteException;
}
