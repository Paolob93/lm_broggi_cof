package common.interfaces;

import java.rmi.*;

import common.exception.CannotLinkCityException;

/**
 * 
 * @author DavideDamonti
 *@author PaoloBroggi
 *
 *The Waiting Room Interface.
 *This interface is used from the rmiclienthandlerì. The player 1 and 2 , are the player who create the lobby and create the match
 */
public interface WaitingRoomInterface extends Remote{

	
	/**
	 * Increment the number of the player that are waiting to start a match
	 * @throws RemoteException
	 */
	public void incrementPlayer() throws RemoteException;
	/**
	 * Used from the rmiclienthandler to create the lobby and the match
	 * @param id
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws InterruptedException
	 * @throws CannotLinkCityException
	 */
	public void setLobby(int id)  throws RemoteException, AlreadyBoundException, InterruptedException, CannotLinkCityException;
	/**
	 * Get the effective number of the player in a lobby.
	 * If this is > of the max num of player for a match, another lobby be created
	 * @return
	 * @throws RemoteException
	 */
	public int getActiveNumPlayer() throws RemoteException;

	/**
	 * Get the lobby ID of the lobby
	 * @return
	 * @throws RemoteException
	 * @throws CannotLinkCityException
	 */
	public int getLobbyID() throws RemoteException, CannotLinkCityException;
	
	/**
	 * Reset the waiting lobby room
	 * @throws RemoteException
	 */
	public void resetRoom() throws RemoteException;
	/**
	 * Get a boolean , it allow the player to know if the timer of player 2 is ended
	 * @return
	 * @throws RemoteException
	 */
	public boolean getCanStart() throws RemoteException;
	/**
	 * Get the match ID of the current match, it is the same of the lobby
	 * @return
	 * @throws RemoteException
	 */
	public int getMatchID() throws RemoteException;
	/**
	 * The player 1 set this boolean to true when he has finished to insert the parameters
	 * @param canContinue
	 * @throws RemoteException
	 */
	public void setCanContinue(boolean canContinue) throws RemoteException;
	
	
	/**
	 * This method allow the rmiclienthanler to create his player controller
	 * @param username
	 * @param myID
	 * @param lobbyID
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public void createPlayerControllerServer(String username,int myID, int lobbyID) throws RemoteException,AlreadyBoundException, NotBoundException;
}
