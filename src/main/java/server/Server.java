package server;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import server.rmi.ServerConsole;
import server.rmi.WaitingRoom;



/**
 * 
 * @author DavideDamonti
 *@author PaoloBroggi
 *
 *
 *The class Server.
 *In the class is presente the main method that start the server
 */
public class Server  {

	private WaitingRoom waitingRoom;
	private Logger logger = Logger.getLogger(Server.class.getName());
	
	
	/**
	 * This method create a registry, and the waiting room , than does the bind on the registry of the waiting room class
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 */
	public void startRMI(){
		
		Registry registry;
		try {
			registry = LocateRegistry.createRegistry(1099);
			waitingRoom = new WaitingRoom();
			registry.bind("waitingRoom",waitingRoom);
		} catch (RemoteException | AlreadyBoundException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
		
		
		
		
	}
	
	/**
	 * This is the methods that start all the server ( in this case only the rmi server)
	 * @throws AlreadyBoundException
	 * @throws IOException
	 */
	public void startServer() throws IOException{
		ServerConsole.printLine("Server Started");	
		startRMI();
		ServerConsole.printLine("Server RMI Started");
		ServerConsole.printLine("waiting for client connects....");
		System.in.close();
	
	}
	
	
	/**
	 * The main method
	 * @param args
	 * @throws NamingException
	 * @throws AlreadyBoundException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void main(String[] args) throws NamingException, AlreadyBoundException, InterruptedException, IOException{
	
		Server server = new Server();
		server.startServer();
		
		
		
	}
		
		

}
