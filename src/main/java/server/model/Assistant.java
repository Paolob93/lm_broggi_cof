package server.model;

/**
 * The Assistant class.
 * 
 * The class that represents an Assistant.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Assistant implements Tradable {

	private boolean avaiable;
	
	/**
	 * The constructor. 
	 * Sets the availability of the Assistant to true.
	 */
    public Assistant() {
    	this.setAvaiable(true);
    }

	public boolean getAvaiable() {
		return avaiable;
	}

	public void setAvaiable(boolean avaiable) {
		this.avaiable = avaiable;
	}

	/**
	 * The method that allows a Player to buy an Assistant from the market.
	 */
	@Override
	public void buy(Player player) {
		player.getAssistants().add(this);
	}
    
    
}