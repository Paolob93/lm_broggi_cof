package server.model;

import common.exception.AssistantRuntimeException;
import common.exception.NotAvailableAssistantsException;

/**
 * The Assistant Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class AssistantBonus extends Bonus {


	/**
	 * The constructor of the class that sets the type to AssistantBonus.
	 */
    public AssistantBonus() {
    	this.type = "AssistantBonus";
    }
    
    /**
     * The creator of the Bonus.
     * Sets the number of Assistants that a Player will earn if he gets the Bonus.
     * @param num
     */
    public AssistantBonus(int num) {
    	this.valueBonus = num;
    	this.type = "AssistantBonus";
    	
    }

    /**
     * Adds to the Player a number of Assistants that is the ValueBonus parameter.
     */
    @Override
    public void giveBonus(Player p) {
        try {
			p.addAssistant(valueBonus);
		} catch (NotAvailableAssistantsException e) {
			throw new AssistantRuntimeException(e);
		}
    }
}