package server.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The BackUpGameBoard class.
 * 
 * The class that contains a backup of all the important informations 
 * contained in the GameBoard in order to restore them if an exception occurred.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class BackUpGameBoard {
	private List<Councillor> kingCouncil = new ArrayList<>();
	private List<Councillor> seaCouncil = new ArrayList<>();
	private List<Councillor> hillCouncil = new ArrayList<>();
	private List<Councillor> mountainCouncil = new ArrayList<>();
	private List<Councillor> avaiableCouncillors = new ArrayList<>();
	private PermitTile[] businessSea = new PermitTile[2];
	private PermitTile[] businessHill = new PermitTile[2];
	private PermitTile[] businessMountain = new PermitTile[2];
	private List<PermitTile> seaPermit = new ArrayList<>();
	private List<PermitTile> hillPermit = new ArrayList<>();
	private List<PermitTile> mountainPermit = new ArrayList<>();
	private PoliticCard[] deck;
	private List<PoliticCard> rejected = new ArrayList<>();
	private Assistant[] setAssistants;
	private KingTileBonus[] kingBonusTile;
	private City kingPosition;
	private Map<String, List<Player>> cities = new HashMap<>();
	
	/**
	 * The constructor that creates the BackUpGameBoard starting from an existing GameBoard.
	 * @param g
	 */
	public BackUpGameBoard(GameBoard g) {
		kingCouncil.addAll(g.getKingBoard().getBalcony().getCouncil());
		seaCouncil.addAll(g.getBoards()[0].getBalcony().getCouncil());
		hillCouncil.addAll(g.getBoards()[1].getBalcony().getCouncil());
		seaPermit.addAll(g.getBoards()[0].getPermitTileDeck());
		hillPermit.addAll(g.getBoards()[1].getPermitTileDeck());
		mountainPermit.addAll(g.getBoards()[2].getPermitTileDeck());
		System.arraycopy(g.getBoards()[0].getBusinessPermitTiles(), 0, businessSea, 0, businessSea.length);
		System.arraycopy(g.getBoards()[1].getBusinessPermitTiles(), 0, businessHill, 0, businessHill.length);
		System.arraycopy(g.getBoards()[2].getBusinessPermitTiles(), 0, businessMountain, 0, businessMountain.length);
		mountainCouncil.addAll(g.getBoards()[2].getBalcony().getCouncil());
		avaiableCouncillors.addAll(g.getSetCouncillors());
		kingPosition = g.getKing().getPosition();
		for (String str: g.getCities().keySet()) {
			List<Player> array = new ArrayList<>();
			array.addAll(g.getCities().get(str).getEmporiums());
			cities.put(str, array);
		}
		setAssistants = new Assistant[g.getSetAssistants().length];
		kingBonusTile = new KingTileBonus[g.getKingTileBonus().length];
		deck = new PoliticCard[g.getDeck().getDeck().length];
		System.arraycopy(g.getSetAssistants(), 0, setAssistants, 0, setAssistants.length);
		System.arraycopy(g.getKingTileBonus(), 0, kingBonusTile, 0, kingBonusTile.length);
		System.arraycopy(g.getDeck().getDeck(), 0, deck, 0, deck.length);
		rejected.addAll(g.getDeck().getRejectedCard());
	}

	public List<Councillor> getKingCouncil() {
		return kingCouncil;
	}

	public List<Councillor> getSeaCouncil() {
		return seaCouncil;
	}

	public List<Councillor> getHillCouncil() {
		return hillCouncil;
	}

	public List<Councillor> getMountainCouncil() {
		return mountainCouncil;
	}

	public List<Councillor> getAvaiableCouncillors() {
		return avaiableCouncillors;
	}

	public PermitTile[] getBusinessSea() {
		return businessSea;
	}

	public PermitTile[] getBusinessHill() {
		return businessHill;
	}

	public PermitTile[] getBusinessMountain() {
		return businessMountain;
	}

	public List<PermitTile> getSeaPermit() {
		return seaPermit;
	}

	public List<PermitTile> getHillPermit() {
		return hillPermit;
	}

	public List<PermitTile> getMountainPermit() {
		return mountainPermit;
	}

	public PoliticCard[] getDeck() {
		return deck;
	}

	public List<PoliticCard> getRejected() {
		return rejected;
	}

	public Assistant[] getSetAssistants() {
		return setAssistants;
	}

	public KingTileBonus[] getKingBonusTile() {
		return kingBonusTile;
	}

	public City getKingPosition() {
		return kingPosition;
	}

	public Map<String, List<Player>> getCities() {
		return cities;
	}
}
