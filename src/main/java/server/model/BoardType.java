package server.model;

/**
 * The BoardType enum.
 * 
 * It contains the 3 kind of Boards that exist in the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public enum BoardType {
    SEA,
    HILL,
    MOUNTAIN
}