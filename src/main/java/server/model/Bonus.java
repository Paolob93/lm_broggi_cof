package server.model;

/**
 * The Abstract Bonus Class.
 * 
 * The superclass of all the kind of Bonus present in the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public abstract class Bonus {

    protected int valueBonus;
    protected String type;
    
    /**
     * The creator that sets the value of the Bonus.
     * @param value
     */
    public Bonus(int value) {
    	this.valueBonus = value;
    }

    public Bonus() {
    	
    }
    
    public String getType() {
    	return this.type;
    }
    
    /**
     * The abstract method that gives the Bonus to a Player.
     * @param p
     */
    public abstract void giveBonus(Player p);

    public int getValueBonus() {
		return valueBonus;
	}
    
    /**
     * The method that sets the value of the bonus to the parameter.
     * @param valueBonus
     */
    public void setValueBonus(int valueBonus) {
    	this.valueBonus = valueBonus;
    }
    
    
    /**
     * The Factory method that returns a Bonus of the specified type.
     * @param type
     * @return
     */
    public static Bonus generateBonus(int type) {
    	switch (type) {
    	case 0:
    		return new AssistantBonus();
    	case 1:
    		return new MainActionBonus();
    	case 2: 
    		return new MoneyBonus();
    	case 3: 
    		return new NobilityTrackBonus();
    	case 4: 
    		return new PoliticCardBonus();
    	case 5:
    		return new VPBonus();
    	case 6: 
    		return new FaceDownPermitTileBonus();
    	case 7: 
    		return new FaceUpPermitTileBonus();
    	default:
    		return new TokenRewardBonus();			
    	}
    }


}