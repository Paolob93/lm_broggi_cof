package server.model;

/**
 * The BonusCityTile.
 * 
 * The tile that gives a VPBonus if a Player has all the cities of the specified color.
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class BonusCityTile extends BonusTile {

	private CityColors color;
	
	/**
	 * The constructor of the class.
	 * It creates a new BonusCityTile with the color and the value of the VPBonus passed as parameter.
	 * @param color
	 * @param value
	 */
    public BonusCityTile(CityColors color, int value) {
    	this.bonus = new VPBonus(value);
    	this.color = color;
    }
    
    /**
     * The method that checks if a Player can take the bonus.
     * @param p
     * @return
     */
    public boolean playerCanTakeTheBonus(Player p) {
    	int numCitiesOfSomeColor = 0;
    	for (City c: p.getCitiesOwned()) {
    		if (c.getColor().equals(this.color)) {
    			numCitiesOfSomeColor++;
    		}
    	}
    	if (numCitiesOfSomeColor == CityColors.getNumCitiesColor(this.color)){
    		return true;
    	}
    	return false;
    }
    
    /**
     * The method that gives the Bonus to the player.
     * @param p
     */
    public void giveBonus(Player p) {
    		this.bonus.giveBonus(p);
    }
}