package server.model;

/**
 * The BonusTile class.
 * It is the SuperClass of BonusCityTile and KingTileBonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class BonusTile {

	protected VPBonus bonus;

}