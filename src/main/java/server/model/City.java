package server.model;

import java.util.ArrayList;
import java.util.List;

import common.exception.CannotLinkCityException;

/**
 * The City class.
 * 
 * The Class that represents a single City. Each City has a color, a name,
 * a list of linked Cities and a RewardToken.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class City {

	private String name;
	private CityColors color;
	private List<Player> emporiums;
	private List<City> nearCities;
	private RewardToken rewardToken;
	
    /**
     * The constructor of the class.
     * It sets the name and the color of the City.
     * It also generate a casual RewardToken.
     * 
     * @param name
     * @param color
     * @param path
     */
	public City (String name, CityColors color, String path) {
    	this.name = name;
    	this.color = color;
    	emporiums = new ArrayList<>();
    	nearCities = new ArrayList<>();
    	rewardToken = new RewardToken(path);
    	if (!this.color.equals(CityColors.VIOLET)) {
    		rewardToken.initRewardToken();
    	}
    	else
    		rewardToken.initVoidToken();
    }


    /**
     * The methods that checks if a Player p has an emporium in this city
     * @param p
     * @return
     */
    public boolean isPresentEmporium(Player p) {
        return this.emporiums.contains(p);
    }


    /**
     * The methods that connects two different cities
     * @param nearCity
     * @throws CannotLinkCityException
     */
    public void createLink(City nearCity) throws CannotLinkCityException {
    	if (nearCity.equals(this)) {
    		throw new CannotLinkCityException();
    	}
    	else if (!nearCities.contains(nearCity)) {
        	nearCities.add(nearCity);
        	nearCity.createLink(this);
        }
    }
    
    /**
     * The method that returns an ArrayList in which are present all the Cities connected
     * with this City that have an Emporium of the Player p
     * @param p
     * @return
     */
    public List<City> nearCitiesPlayer (Player p) {
		ArrayList<City> nearCityPlayer = new ArrayList <>();
		for (City near: this.nearCities) {
			if (near.isPresentEmporium(p) && !(nearCityPlayer.contains(near))) {
				nearCityPlayer.add(near);
				near.nearCitiesPlayer(nearCityPlayer, near, p);
			}
		}
		return nearCityPlayer;
	}
	
    /**
     * The recursive method that searches the all the Cities connected
     * with this City that have an Emporium of the Player p
     * @param nearCityPlayer
     * @param city
     * @param p
     */
	private void nearCitiesPlayer(ArrayList<City> nearCityPlayer, City city, Player p) {
		for (City near: city.nearCities) {
			if (near.isPresentEmporium(p) && !(nearCityPlayer.contains(near))) {
				nearCityPlayer.add(near);
				near.nearCitiesPlayer(nearCityPlayer, near, p);
			}
		}
	}

	/**
	 * The method that checks if a City contains a RewardToken with a NobilityTrackBonus.
	 * @return
	 */
	public boolean isNoble(){
		boolean noble = false;
		for (Bonus b: this.rewardToken.getBonus()) {
			if ("NobilityTrackBonus".equals(b.type))
				noble = true;
		}
		return noble;
	}

	public RewardToken getRewardToken() {
		return rewardToken;
	}


	public void setRewardToken (RewardToken rt) {
		this.rewardToken = rt;
	}


	public CityColors getColor() {
		return this.color;
	}


	public void setColor(CityColors color) {
	    this.color = color;
	}


	public String getName() {
	    return this.name;
	}


	public void setName(String name) {
	    this.name = name;
	}


	public List<City> getNearCities() {
		return this.nearCities;
	}


	public List<Player> getEmporiums(){
		return this.emporiums;
	}
}