package server.model;

/**
 * The CityColors enum.
 * 
 * The enum that contains the colors that a City could have.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public enum CityColors {
    RED,
    GOLD,
    BLUE,
    GREY,
    VIOLET;
    
	/**
	 * The method that returns the number of Cities that have the color
	 * passed as parameter.
	 * @param color
	 * @return
	 */
    public static int getNumCitiesColor(CityColors color) {
    	if (color == RED) {
    		return Constants.CITIES_RED;
    	}
    	else if (color == GOLD) {
    		return Constants.CITIES_GOLD;
    	}
    	else if (color == BLUE) {
    		return Constants.CITIES_BLUE;
    	}
    	else if (color == GREY) {
    		return Constants.CITIES_GREY;
    	}
    	else return Constants.NUM_CITIES_VIOLET;
    }
}