package server.model;

/**
 * The Constants class.
 * The class that contains all the public final and static constants that are used in the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Constants {
	
	public static final int EMPORIUMS_FOR_VICTORY = 10;
	public static final int CITIES_RED = 3;
	public static final int CITIES_GOLD = 5;
	public static final int CITIES_BLUE = 2;
	public static final int CITIES_GREY = 4;
	public static final int NUM_CITIES_VIOLET = 1;
	public static final String INITIAL_MAP_PATH = "./src/main/java/server/model/Map";
	public static final int CITY_NUMBER = 15;
	public static final int REGION_BONUS_VALUE = 5;
	public static final int NUM_ASSISTANTS = 48;
	public static final int NUM_COUNCILLORS_IN_COUNCIL = 4;
	public static final int REWARD_FOR_ELECTION = 4;
	public static final int ASSISTANT_PRICE = 3;
	public static final int ASSISTANTS_FOR_MAIN_ACTION = 3;
	public static final int NUM_TOT_COUNCILLORS = 24;
	public static final int NUM_POLITIC_COLORS = 6;
	public static final int TIME_TURN = 120;
	public static final int MAPS_NUMBER = 8;
	public static final int VP_FIRST_NT = 5;
	public static final int VP_SECOND_NT = 2;
	public static final int VP_TOP_PT = 3;
	public static final int NUM_TILES = 21;
	public static final int CITIES_PER_BOARD = 5;
	public static final int MAX_VALUE_INPUT_BONUS = 5;
	
	private Constants(){
		//Private constructor
	}
	
}
