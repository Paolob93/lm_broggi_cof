package server.model;

import java.util.*;

import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;

/**
 * The Council class.
 * 
 * The class that contains a set of Councillors and let a Player
 * elect a new Councillor.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Council {

	private ArrayList<Councillor> councilBoard;
	private GameBoard gameBoard;
	
	/**
	 * The constructor of the class.
	 * It initializes the ArrayList of Councillors.
	 * @param gameBoard
	 */
    public Council(GameBoard gameBoard) {
    	councilBoard = new ArrayList<>();
    	this.gameBoard = gameBoard;
    	this.initCouncil();
    	
    }
   
    /**
     * The method that adds in the first position a Councillor.
     * @param councillor
     */
    public void addCouncillor(Councillor councillor) {
    	councilBoard.add(0,councillor);
    }
    
    /**
     * The method that removes the councillor in the last position.
     */
    public void removeLastCouncillor (){
    	int position = councilBoard.size() - 1;
    	Councillor removed = councilBoard.get(position);
    	councilBoard.remove(removed);
    	gameBoard.getSetCouncillors().add(removed);
    }
  
    /**
     * The method that initializes the ArrayList of Councillors.
     */
    public void initCouncil(){
    	for(int i=0; i<Constants.NUM_COUNCILLORS_IN_COUNCIL; i++){
    		Councillor c = this.gameBoard.getCouncillor();
    		addCouncillor(c);
    		gameBoard.getSetCouncillors().remove(0);
    	}
    }
    
    public List<Councillor> getCouncil(){
    	return councilBoard;
    }
    
    /**
     * Checks if a set of cards satisfy the council.
     * @param cards
     * @return
     */
    public boolean satisfyCouncil(List<PoliticCard> cards) {
    	if(cards.size() > councilBoard.size() || cards.isEmpty()) {
    		return false;
    	}
    	boolean satisfyCouncil = false;
    	boolean satisfyCouncillor;
    	List<PoliticCard> colorCards = new ArrayList<>();
    	for (PoliticCard card: cards) {
    		if (!card.getJolly()) {
    			colorCards.add(card);
    		}
    	}
    	for (Councillor councillor: councilBoard) {
    		satisfyCouncillor = false;
    		for (Iterator<PoliticCard> iter = colorCards.iterator(); iter.hasNext() && !satisfyCouncillor;) {
    			PoliticCard card = iter.next();
    			if (councillor.getColorCouncillor().equals(card.getColor())) {
    				satisfyCouncillor = true;
    				colorCards.remove(card);
    			}
    		}
    	}
    	if (colorCards.isEmpty()) {
    		satisfyCouncil = true;
    	}
    	return satisfyCouncil;
    }
    
    /**
     * The method that elects a Councillor of the specitfied color in this Council.
     * @param color
     * @throws NotAvailableCouncillorsException
     * @throws NotAvailableColorCouncillorsException
     */
    public void elect(PoliticColors color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException {
    	Councillor electingCouncillor = this.gameBoard.getFirstAvailableColorCouncillor(color);
    	this.addCouncillor(electingCouncillor);
    	this.gameBoard.getSetCouncillors().remove(electingCouncillor);
    	this.removeLastCouncillor();
    }
    

}