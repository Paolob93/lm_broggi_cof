package server.model;

/**
 * The Councillor class.
 * 
 * The class that represents a Councillor that is characterized by a PoliticColor.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Councillor {
	
	private PoliticColors color;
	
	/**
	 * The empty constructor of the class: it is empty because it's preferable
	 * to set the color of the Councillor later,
	 */
	public Councillor(){
		//Empty Constructor
	}
    
	/**
	 * The other constructor of the class: it creates a new Councillor
	 * with the specified Color.
	 * @param color
	 */
    public Councillor(PoliticColors color){
    	this.color = color;
    }
    
    public PoliticColors getColorCouncillor(){
    	return color;
    }
    
    public String getStringColor(){
    	return color.toString();
    }
}