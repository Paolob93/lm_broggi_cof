package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The PoliticCard class.
 * 
 * The Class that contains a Deck of PoliticCard and a set of rejected PoliticCard.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class DeckPoliticCards {

	private PoliticCard[] deck;
	private List<PoliticCard> rejectedCards = new ArrayList<>();
	private String settingsPath;
	private int numPlayers;
	private Logger loggerDeck = Logger.getLogger(DeckPoliticCards.class.getName());

	/**
	 * The constructor of the class.
	 * It reads the number of color and jolly cards in the file at the position 
	 * specified from the given path.
	 * @param path
	 * @throws RemoteException
	 */
	public DeckPoliticCards(String path) throws RemoteException {
		settingsPath = path;
		try {
			File settings = new File (settingsPath);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			int colorCards = (int)(long) settingsObj.get("numCardsForColor");
			int jollyCards = (int)(long) settingsObj.get("numJollyCards");
			numPlayers = (int)(long) settingsObj.get("numPlayers");
			deck = new PoliticCard[(colorCards * Constants.NUM_POLITIC_COLORS) + jollyCards];
			rejectedCards = new ArrayList<>();
			this.initDeck();
			this.shuffleDeck();
		}
		catch (IOException | ParseException e) {
			loggerDeck.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that initializes every card the deck
	 * @throws RemoteException
	 */
	public void initDeck() throws RemoteException {
		for(int i = 0; i < deck.length; i++){
			deck[i] = new PoliticCard();
			if (i < (deck.length - (numPlayers * 3))) {
				deck[i].setColor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			}
			else {
				deck[i].setJolly();
			}
		}
	}

	/**
	 * The method that shuffles the Deck.
	 */
	public void shuffleDeck() {
		int n = deck.length;
		for(int i=0; i<n; i++){
			int random = (int)(Math.random()*(n-1));
			PoliticCard randomElement = deck[random];
			deck[random] = deck[i];
			deck[i] = randomElement;
		}
	}

	/**
	 * The method that returns the first PoliticCard in the deck and reinitializes the deck 
	 * if there are no cards available.
	 * @return
	 */
	public PoliticCard getPoliticCard(){
		for(PoliticCard c: deck)
			if (c.getInDeck()){
				c.setInDeck(false);
				return c;
			}
		this.reInitDeck();
		this.getPoliticCard();
		return null;
	}


	public PoliticCard[] getDeck() {
		return deck;
	}

	/**
	 * The method that reinitializes the deck using the rejected cards.
	 */
	public void reInitDeck(){
		for (PoliticCard pc: this.getRejectedCard()) {
			if (!(pc.getInDeck())) {
				pc.setInDeck();
			}
		}
		rejectedCards.clear();
		this.shuffleDeck();

	}

	public void addRejectedCard (PoliticCard politicCard){
		rejectedCards.add(politicCard);
	}

	public List<PoliticCard> getRejectedCard(){
		return rejectedCards;
	}

	public void setDeck(PoliticCard[] deck){
		this.deck = deck;
	}

	public void setRejectedCard(List<PoliticCard> rejectedCard){
		this.rejectedCards.clear();
		this.rejectedCards.addAll(rejectedCard);
	}
}