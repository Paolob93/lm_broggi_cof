package server.model;

/**
 * The FaceDownPermitTileBonus class.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class FaceDownPermitTileBonus extends Bonus {

    /**
     * Default constructor that sets the type to FaceDownPermitTileBonus.
     */
    public FaceDownPermitTileBonus() {
    	this.type = "FaceDownPermitTileBonus";
    }

    /**
     * The method that overrides the superclass method and ignores the
     * value inserted because the only allowed value is 1.
     */
    @Override
    public void setValueBonus(int value) {
    	this.valueBonus = 1;
    }
    
    /**
     * The method that gives the bonus to the Player.
     */
    @Override
    public void giveBonus(Player p) {
    	p.setFdBonus(true);
    }
}