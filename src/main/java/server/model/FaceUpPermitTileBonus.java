package server.model;

/**
 * The FaceUpPermitTileBonus class.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class FaceUpPermitTileBonus extends Bonus {

	/**
     * Default constructor that sets the type to FaceUpPermitTileBonus.
     */
    public FaceUpPermitTileBonus() {
    	this.type = "FaceUpPermitTileBonus";
    }

    /**
     * The method that overrides the superclass method and ignores the
     * value inserted because the only allowed value is 1.
     */
    @Override
    public void setValueBonus(int value) {
    	this.valueBonus = 1;
    }
    
    /**
     * The method that gives the bonus to the Player.
     */
    @Override
    public void giveBonus(Player p) {
    	p.setFuBonus(true);
    }
}