package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import common.exception.CannotLinkCityException;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;

/**
 * The GameBoard class.
 * 
 * The class that contains the GameBoard, the RegionBoards and anything "material" that the game needs.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class GameBoard {

	private RegionBoard[] boards;
	private KingBoard kingBoard;
	private DeckPoliticCards deck;
	private List<Councillor> setCouncillors;
	private Assistant[] setAssistants;
	private int numberCouncillors = Constants.NUM_TOT_COUNCILLORS;
	private BonusCityTile[] bonusCityTile;
	private KingTileBonus[] kingBonusTile;
	private King king;
	private int mapNumber = 1;
	private Map<String, City> cities = new HashMap<>();
	private String settingsPath;
	private Logger loggerGameBoard = Logger.getLogger(GameBoard.class.getName());

	/**
	 * The constructor of the GameBoard. It sets the settingsPath to the proper value
	 * and starts the initialization of everything.
	 * @param id
	 * @throws RemoteException
	 */
	public GameBoard(int id) throws RemoteException{
		settingsPath = "./src/main/java/server/model/Match" + id + "Settings.json";
		try {
			File settings = new File (settingsPath);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			mapNumber = (int) (long) settingsObj.get("mapNumber");
		}
    	catch (IOException | ParseException e) {
    		loggerGameBoard.log(Level.INFO, e.getMessage(), e);
		}		
		this.setBoards(new RegionBoard[3]);
		this.bonusCityTile = new BonusCityTile[4];
		this.kingBonusTile = new KingTileBonus[5];
		this.setCouncillors = new ArrayList<>();
		this.king = new King();
		this.initGameBoard();
		
	}
	
	/**
	 * The method that initializes everything.
	 * @throws RemoteException
	 */
	public void initGameBoard() throws RemoteException{
		initSetCouncillors();
		initCities();
		kingBoard = new KingBoard(this);
		getBoards()[0] = new RegionBoard(BoardType.SEA, this, 0);
		getBoards()[1] = new RegionBoard(BoardType.HILL, this, 1);
		getBoards()[2] = new RegionBoard(BoardType.MOUNTAIN, this, 2);
		deck = new DeckPoliticCards(settingsPath);
		setAssistants = new Assistant[Constants.NUM_ASSISTANTS];
		bonusCityTile[0] = new BonusCityTile(CityColors.BLUE,5);
		bonusCityTile[1] = new BonusCityTile(CityColors.RED,8);
		bonusCityTile[2] = new BonusCityTile(CityColors.GREY,12);
		bonusCityTile[3] = new BonusCityTile(CityColors.GOLD,20);
		kingBonusTile[0]  = new KingTileBonus(25);
		kingBonusTile[1]  = new KingTileBonus(18);
		kingBonusTile[2]  = new KingTileBonus(12);
		kingBonusTile[3]  = new KingTileBonus(7);
		kingBonusTile[4]  = new KingTileBonus(3);
		king.setStartPosition(this.findVioletCity());
		initSetAssistants();
		kingBoard.createNobilityTrack();
	}

	/**
	 * The method used for restoring the GameBoard after a custom exception.
	 * @param backup
	 */
	public void restoreGameBoard(BackUpGameBoard backup) {
		kingBoard.getBalcony().getCouncil().clear();
		boards[0].getBalcony().getCouncil().clear();
		boards[1].getBalcony().getCouncil().clear();
		boards[2].getBalcony().getCouncil().clear();
		setCouncillors.clear();
		boards[0].getPermitTileDeck().clear();
		boards[1].getPermitTileDeck().clear();
		boards[2].getPermitTileDeck().clear();
		deck.getRejectedCard().clear();
		kingBoard.getBalcony().getCouncil().addAll(backup.getKingCouncil());
		boards[0].getBalcony().getCouncil().addAll(backup.getSeaCouncil());
		boards[1].getBalcony().getCouncil().addAll(backup.getHillCouncil());
		boards[2].getBalcony().getCouncil().addAll(backup.getMountainCouncil());
		setCouncillors.addAll(backup.getAvaiableCouncillors());
		boards[0].getPermitTileDeck().addAll(backup.getSeaPermit());
		boards[1].getPermitTileDeck().addAll(backup.getHillPermit());
		boards[2].getPermitTileDeck().addAll(backup.getMountainPermit());
		deck.getRejectedCard().addAll(backup.getRejected());
		king.setPosition(backup.getKingPosition());
		for (String str: backup.getCities().keySet()) {
			cities.get(str).getEmporiums().clear();
			cities.get(str).getEmporiums().addAll(backup.getCities().get(str));
		}
		System.arraycopy(backup.getBusinessSea(), 0, getBoards()[0].getBusinessPermitTiles(), 0, getBoards()[0].getBusinessPermitTiles().length);
		System.arraycopy(backup.getBusinessHill(), 0, getBoards()[1].getBusinessPermitTiles(), 0, getBoards()[1].getBusinessPermitTiles().length);
		System.arraycopy(backup.getBusinessMountain(), 0, getBoards()[2].getBusinessPermitTiles(), 0, getBoards()[2].getBusinessPermitTiles().length);
		System.arraycopy(backup.getSetAssistants(), 0, setAssistants, 0, backup.getSetAssistants().length);
		System.arraycopy(backup.getKingBonusTile(), 0, kingBonusTile, 0, backup.getKingBonusTile().length);
		System.arraycopy(backup.getDeck(), 0, deck.getDeck(), 0, backup.getDeck().length);
	}

	/**
	 * The method that adds an Assistant to the GameBoard. It is used when a Player discards an Assistant.
	 * @param assistant
	 */
	public void addAssistant(Assistant assistant){
		for(Assistant a: setAssistants){
			if(a == assistant){
				a.setAvaiable(true);
			}
		}
	}
	
	/**
	 * The method that returns the Violet city that is the city in which the king will be in the first place.
	 * @return
	 */
	public City findVioletCity()  {
		for (City c: cities.values()) {
			if (c.getColor().equals(CityColors.VIOLET)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * The method that gives a KingTileBonus to the Player, if there is still a bonus available.
	 * @param p
	 */
	public void giveKingTileBonus(Player p) {
		for(KingTileBonus bonus: kingBonusTile){
			if(!bonus.getUsed()){
				bonus.giveBonus(p);
				bonus.setUsed();
				break;
			}
		}
	}
	
	/**
	 * The method that returns the first Councillor in the sets of available Councillors.
	 * @return
	 */
	public Councillor getCouncillor() {
		return this.setCouncillors.get(0);
	}

	/**
	 * The method that returns a Councillor of the chosen color if it's available, otherwise it throws an exception.
	 * @param color
	 * @return
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 */
	public Councillor getFirstAvailableColorCouncillor(PoliticColors color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException {
		Councillor councillor;
		if (!setCouncillors.isEmpty()) {
			Iterator<Councillor> iter = setCouncillors.iterator();
			while (iter.hasNext()) {
				councillor = iter.next();
				if (councillor.getColorCouncillor().equals(color)) {
					return councillor;
				}
			}
			throw new NotAvailableColorCouncillorsException();
		}
		else throw new NotAvailableCouncillorsException();
	}

	/**
	 * The method that returns the RegionBoard corresponding to the BoardType specified.
	 * @param type
	 * @return
	 */
	public RegionBoard getRegionBoardType(BoardType type) {
		for ( int i=0; i<getBoards().length; i++){
			if (getBoards()[i].getType() == type)
				return getBoards()[i];
		}
		return null;
	}

	/**
	 * The method that returns the first available Assistant.
	 * @return
	 * @throws NotAvailableAssistantsException
	 */
	public Assistant getAssistant() throws NotAvailableAssistantsException{
		for(int i=0; i<setAssistants.length; i++){
			if(setAssistants[i].getAvaiable()){
				setAssistants[i].setAvaiable(false);
				return setAssistants[i];
			}
		}
		throw new NotAvailableAssistantsException();
	}

	/**
	 * The method that returns a formatted String containing all the RewardTokens.
	 * @return
	 */
	public String showRewardTokens() {
		String showtokens = new String();
		int i= 0;
		for (RewardToken rt: this.getAllRewardTokens()) {
			showtokens = showtokens + "Reward Token #" + i + " " + rt.getRewardTokenDetails();
			i++;
		}
		return showtokens;
	}

	/**
	 * The method that returns an ArrayList containing all the RewardTokens.
	 * @return
	 */
	public List<RewardToken> getAllRewardTokens() {
		List<RewardToken> tokens = new ArrayList<>();
		Iterator<City> citIt = cities.values().iterator();
		while (citIt.hasNext()) {
			tokens.add(citIt.next().getRewardToken());
		}
		return tokens;
	}

	/**
	 * The method that returns the bonus corresponding to the specified color.
	 * @param color
	 * @return
	 */
	public BonusCityTile getBonusCityTile(CityColors color){

		if (color == CityColors.BLUE)
			return bonusCityTile[0];
		if (color == CityColors.RED)
			return bonusCityTile[1];
		if (color == CityColors.GREY)
			return bonusCityTile[2];
		else 
			return bonusCityTile[3];
	}

	/**
	 * The method that creates all the cities and saves them in the HashMap using the CityName as key
	 * @throws CannotLinkCityException
	 */
	@SuppressWarnings("unchecked")
	private void initCities()  {
		try {
			File mapFile = new File (Constants.INITIAL_MAP_PATH.concat(String.valueOf(mapNumber)).concat(".json"));
			JSONParser parser = new JSONParser();
			JSONArray citiesArray = (JSONArray) parser.parse(new FileReader(mapFile));
			Iterator<JSONObject> iterCity = citiesArray.iterator();
			while (iterCity.hasNext()) {
				JSONObject city = iterCity.next();
				String name = (String) city.get("Name");
				CityColors color = CityColors.valueOf((String) city.get("Color"));
				getCities().put(name, new City(name, color, this.getSettingsPath()));
			}
			Iterator<JSONObject> iter2 = citiesArray.iterator();		// adding near cities
			while (iter2.hasNext()) {
				JSONObject city = iter2.next();
				String name = (String) city.get("Name");
				JSONArray near = (JSONArray) city.get("NearCities");
				Iterator<String> nearIterator = near.iterator();
				while (nearIterator.hasNext()) {
					String nameNear = nearIterator.next();
					getCities().get(name).createLink(getCities().get(nameNear));
				}
			}
		} 
		catch (IOException | ParseException e) {
			loggerGameBoard.log(Level.INFO, e.getMessage(), e);
		}catch(CannotLinkCityException e1){
			loggerGameBoard.log(Level.INFO, e1.getMessage(), e1);
		}
	}

	/**
	 * The method that creates all the Councillors with the proper color and shuffles them.
	 */
	private void initSetCouncillors(){
		for(int i=0; i<numberCouncillors; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			setCouncillors.add(c);
		}
		Collections.shuffle(setCouncillors);
	}
	
	/**
	 * The method that creates all the Assistants.
	 */
	private void initSetAssistants(){
		for (int i = 0; i < Constants.NUM_ASSISTANTS; i++) {
			setAssistants[i] = new Assistant();
		}
	}
	
	/**
	 * The method that returns the RegionBoard that contains the given city.
	 * @param city
	 * @return
	 */
	public RegionBoard getCityRegion(City city) {
		for (RegionBoard b: boards) {
			for (City c: b.getCitiesInBoard()){
				if (c.equals(city))
					return b;
			}
		}
		return null;
	}
	

	public void setSetConucillors(List<Councillor> councillors) {
		this.setCouncillors = councillors;
	}

	public void setBoards(RegionBoard[] boards) {
		this.boards = boards;
	}

	public KingTileBonus[] getKingTileBonus(){
		return kingBonusTile;
	}

	public List<Councillor> getSetCouncillors() {
		return setCouncillors;
	}

	public KingBoard getKingBoard(){
		return this.kingBoard;
	}

	public DeckPoliticCards getDeck(){
		return deck;
	}

	public RegionBoard[] getBoards() {
		return boards;
	}

	public King getKing() {
		return king;
	}

	public int getMapNumber() {
		return mapNumber;
	}

	public Map<String, City> getCities() {
		return cities;
	}

	public String getSettingsPath() {
		return this.settingsPath;
	}

	public Assistant[] getSetAssistants(){
		return this.setAssistants;
	}
}