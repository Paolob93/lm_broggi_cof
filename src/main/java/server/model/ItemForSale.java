package server.model;

import common.exception.YouArePoorException;

/**
 * The ItemForSale class.
 * 
 * The class used for selling and buying items from the market.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class ItemForSale {
	private Tradable item;
	private int price;
	private Player seller;
	private String itemDetails;
	
	/**
	 * The constructor of the class.
	 * It takes the tradable item to sell, the price, the Player 
	 * that sells the item and the details of the item.
	 * @param item
	 * @param price
	 * @param seller
	 * @param details
	 */
	public ItemForSale(Tradable item, int price, Player seller, String details){
		this.item = item;
		this.price = price;
		this.seller = seller;
		this.itemDetails = details;
	}
	
	/**
	 * The method that adds to the Player given as parameter the correct item.
	 * If the Player don't have enough money this method will throw a YouArePoorException.
	 * @param player
	 * @throws YouArePoorException
	 */
	public void buy(Player player) throws YouArePoorException {
		this.item.buy(player);
		player.removeMoney(price);
		this.seller.addMoney(price);
		seller.getMarket().getAvailableItems().remove(this);
	}
	
	public Tradable getItem() {
		return item;
	}
	
	public int getPrice() {
		return price;
	}
	
	public Player getSeller() {
		return seller;
	}
	
	public void returnUnsoldItem () {
		this.item.buy(seller);
	}
	
	public String getItemDetails(){
		return itemDetails;
	}
}
