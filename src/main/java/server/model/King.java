package server.model;

import java.util.*;

/**
 * The King class.
 * 
 * The class that represents the king that has a position and let a Player calculate the minimum distance from 
 * the current position and a given City.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class King {
	
	private City position;
	private boolean initialized = false;
	
    /**
     * The method that computes the distance between the current City
     * in which the king is and the City given as parameter.
     * @param destination
     * @return
     */
    public int getDistance(City destination) {
    	List<City> near = new ArrayList<>();
    	int distance = 0;
    	if (this.getPosition().equals(destination)) {
    		return distance;
    	}
    	else {
    		Iterator<City> iter1 = this.getPosition().getNearCities().iterator();
    		while (iter1.hasNext()) {
    			near.add(iter1.next());
    		}
    		return getDistance(distance, destination, near);
    	}
    }

    /**
     * The recursive method that is called from the previous method.
     * @param value
     * @param destination
     * @param near
     * @return
     */
	private int getDistance(int value, City destination, List<City> near) {
		int distance = value;
		distance++;
		if (near.contains(destination)) {
			return distance;
		}
		else {
			List<City> added = new ArrayList<>();
			for (City c: near) {
				Iterator<City> iter2 = c.getNearCities().iterator();
	    		while (iter2.hasNext()) {
	    			City cityNext = iter2.next();
	    			if (!near.contains(cityNext)); {
	    				added.add(cityNext);
	    			}
	    		}
			}
			near.addAll(added);
			return getDistance(distance, destination, near);
		}
	}

	public City getPosition() {
		return position;
	}

	public void setPosition(City position) {
		this.position = position;
	}
	
	/**
	 * The method that sets the initial position of the King in the given City.
	 * @param position
	 */
	public void setStartPosition(City position) {
		if (!initialized) {
			this.position = position;
			initialized = true;
		}
	}

}