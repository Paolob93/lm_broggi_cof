package server.model;

import java.rmi.RemoteException;

/**
 * The KingBoard class.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class KingBoard {

	private Council balcony;
	private NobilityTrack nobilityTrack;
	private GameBoard gameBoard;
	
	/**
	 * The constructor of the class that also calls the method that initializes the nobility track
	 * @param gameBoard
	 * @throws RemoteException 
	 */
    public KingBoard(GameBoard gameBoard) throws RemoteException {
    	this.setGameBoard(gameBoard);
    	balcony = new Council(gameBoard);
    }
    
    /**
     * The methods that creates and initializes the NobilityTrack
     */
    public void createNobilityTrack(){
    	setNobilityTrack(new NobilityTrack(gameBoard.getSettingsPath()));
    	getNobilityTrack().initNobilityTrack();	
    }
    
    
    public Council getBalcony(){
    	return this.balcony;
    }


	public NobilityTrack getNobilityTrack() {
		return nobilityTrack;
	}


	public void setNobilityTrack(NobilityTrack nobilityTrack) {
		this.nobilityTrack = nobilityTrack;
	}

	
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
    
	
}