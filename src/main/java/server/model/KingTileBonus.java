package server.model;

/**
 * The KingTileBonus
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class KingTileBonus extends BonusTile {

	private boolean used;
	
	/**
	 * The constructor of the class that creates a new VPBonus of the specified
	 * value and sets the value of used to false so the tile is usable.
	 * @param value
	 */
    public KingTileBonus(int value) {
    	bonus = new VPBonus(value);
    	used = false;
    }
    
    public boolean getUsed(){
    	return used;
    }

    public void setUsed(){
    	used = true;
    }
    
    /**
     * The method that gives the bonus of the VPBonus to the given Player.
     * @param p
     */
    public void giveBonus(Player p) {
    	this.bonus.giveBonus(p); 		
    }
    
}
