package server.model;


import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.interfaces.LobbyInterface;
import server.rmi.PlayerControllerServer;
import server.rmi.ServerController;


/**
 * The Lobby class.
 * 
 * The class that contains an instance of the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Lobby extends UnicastRemoteObject implements LobbyInterface{

	private static final long serialVersionUID = -7857386300361738946L;
	private  transient Match  match;
	private  transient PlayerList players;
	private ServerController serverController;
	private SettingsCreator settings;
	private static Logger loggerLobby = Logger.getLogger(Lobby.class.getName());

	/**
	 * The creator of the class that instantiates a new Match and creates a new serverController and a new PlayerList.
	 * @param lobbyID
	 * @throws RemoteException
	 * @throws InterruptedException
	 * @throws AlreadyBoundException
	 */
	public Lobby(int lobbyID) throws RemoteException, InterruptedException,AlreadyBoundException{
		players = new PlayerList(); 
		match = new Match(players, lobbyID);
		serverController = new ServerController(players,match, lobbyID);
		settings = new SettingsCreator();
		settings.setSettingsID(lobbyID);
		Registry registry = LocateRegistry.getRegistry(1099);
		registry.bind("settings"+lobbyID,settings);
	}

	/**
	 * The method that checks if a match can start.
	 * @return
	 */
	public boolean canStart() {
		return false;
	}

	/**
	 * The method that creates a PlayerController for the specified Player.
	 */
	@Override
	public void createPlayerController(String username,int myID) throws RemoteException{		
		Player player = new Player(username,PlayerColors.values()[myID],match,myID);
		players.addPlayer(player);
		Registry registry = LocateRegistry.getRegistry(1099);
		PlayerControllerServer playerController = new PlayerControllerServer(player, getMatch());
		try {
			registry.bind(username, playerController);
		} catch (AlreadyBoundException e) {
			loggerLobby.log(Level.INFO, e.getMessage(), e);
		}
	} 

	/**
	 * The method that returns a String containing the position in the NobilityTrack of every Player.
	 * @return
	 */
	public String getNobilityPosition(){
		String positions = "";
		for(Player p:  players.getPlayers()){
			positions = positions+p.getName()+" - NobilityTrackPosition = "+p.getNobilityTrackPosition()+ "\n";
		}
		return positions;
	}

	/**
	 * The method that returns a String version of the NobilityTrack.
	 */
	@Override
	public String getNobilityTrack(){

		NobilityTrackTile[] nobilityTrack = match.getGameBoard().getKingBoard().getNobilityTrack().getNobilityTrackArray();
		String details = "";
		int i=0;
		for(NobilityTrackTile tile : nobilityTrack){
			details = details+"Position "+i+" - "+tile.getNobilityTrackDetails()+" \n";
			i++;
		}
		return details+getNobilityPosition();
	}

	/**
	 * The method that returns a String[] with the details of every Player.
	 */
	@Override
	public String[] getStatusPlayer(){
		int dim = players.getNumPlayer();
		String[] info = new String[dim];
		List<Player> playersGame = players.getPlayers();
		for(int i =0; i<dim ; i++){
			Player p = playersGame.get(i);
			info[i] = p.getName()+" - money: "+p.getMoney()+", VP: "+p.getVictoryPoints()+", Emporia owned: "+p.getNumEmporiums()+", Assistants owned: "+p.getAssistants().size();
		}

		return info;
	}

	/**
	 * The method that returns a String[] containing the Councillors still available.
	 */
	@Override
	public String[] getColorStringAvaiableCounselor(){
		String[] councillors = new String[6];
		int i=0;
		int num;
		for(PoliticColors color: PoliticColors.values()){
			num = 0;
			for(Councillor c: match.getGameBoard().getSetCouncillors())
				if(c.getColorCouncillor().equals(color))
					num++;
			councillors[i]= color.toString()+": "+num;
			i++;
		}
		return councillors;
	}

	/**
	 * The method that returns a String[] containing the Councillors elected in a Council of the specified Region.
	 */
	@Override
	public String[] showCouncilsGame(String region){
		String[] councillors =  new String[4];
		int i=0;
		if("KING".equals(region)){
			for(Councillor c:match.getGameBoard().getKingBoard().getBalcony().getCouncil()){
				councillors[i] = c.getStringColor();	
				i++;
			}
			return councillors;
		}
		for(Councillor c:match.getGameBoard().getRegionBoardType(BoardType.valueOf(region)).getBalcony().getCouncil()){
			councillors[i] = c.getStringColor();	
			i++;
		}
		return councillors;
	}

	/**
	 * The method that returns a string containing the details of the Business 
	 * Permit Tiles of the given Region.
	 */
	@Override
	public String[] showPermitTiles(String region){

		String[] tiles = new String[2];

		for(int i=0;i<2;i++)
			tiles[i]=match.getGameBoard().getRegionBoardType(BoardType.valueOf(region)).getTile(i).getPermitTileDetails();



		return tiles;
	}
	
	/**
	 * The method that returns a well formatted ArrayList<String> that contains the details of all the Cities in a given Region.
	 */
	@Override
	public List<String> showCitiesRegion(String region){

		List<String> cityString = new ArrayList<>();
		String name;
		String color;
		String bonus;

		for(int i=0; i<5; i++){
			City city = match.getGameBoard().getRegionBoardType(BoardType.valueOf(region)).getCitiesInBoard()[i];
			name = city.getName();
			color =  city.getColor().toString();

			if(match.getGameBoard().getKing().getPosition().equals(city)){
				name = name + "\t-------------------------------------> KING'S CITY";
			}
			cityString.add("\tName: "+name);
			cityString.add("\t\tColor: "+color);
			addStringEmporiums(cityString,region,i);
			addStringCitiesConnected(cityString,region,i);
			bonus = city.getRewardToken().getRewardTokenDetails();
			cityString.add("\t\tReward Token:");
			cityString.add(bonus);
		}

		return cityString;
	}

	/**
	 * The method that adds the name of the Players that have an Emporium in a city to the previous ArrayList. 
	 * @param city
	 * @param region
	 * @param index
	 */
	public void addStringEmporiums(List<String> city, String region, int index){
		List<Player> player =  match.getGameBoard().getRegionBoardType(BoardType.valueOf(region)).getCitiesInBoard()[index].getEmporiums();
		city.add("\t\tPlayers that have an Emporium here: ");
		for(Player p : player){
			city.add("\t\t\t"+p.getName());
		}
	}

	/**
	 * The method that adds the connections to the previous ArrayList.
	 * @param city
	 * @param region
	 * @param index
	 */
	public void addStringCitiesConnected(List<String> city, String region, int index){
		List<City> connections = match.getGameBoard().getRegionBoardType(BoardType.valueOf(region)).getCitiesInBoard()[index].getNearCities();
		city.add("\t\tCity linked with: ");
		for(City c: connections){
			city.add("\t\t\t"+c.getName());
		}
	}

	/**
	 * The method that returns a String containing the details of the Market.
	 */
	@Override
	public String showItemMarket(){
		Market market = match.getMarket();
		return market.showMarket();
	}



	public Match getMatch(){
		return match;
	}

	public PlayerList getPlayerList(){
		return players;
	}

	public ServerController getServerController(){
		return serverController;
	}

	@Override
	public int getNumPlayerList(){
		return players.getNumPlayer();
	}

	public SettingsCreator getSettings(){
		return this.settings;
	}
}