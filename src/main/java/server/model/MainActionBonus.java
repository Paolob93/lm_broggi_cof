package server.model;

/**
 * The MainActionBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class MainActionBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to MainActionBonus 
	 * and the ValueBonus to 1 which is the only value allowed.
	 */
    public MainActionBonus() {
    	this.valueBonus = 1;
    	this.type = "MainActionBonus";
    }
    
    /**
	 * The constructor of the class that sets the type to MainActionBonus 
	 * and ignores the value inserted because the only allowed value is 1.
	 */
    public MainActionBonus(int i) {
    	this.valueBonus = 1;
    	this.type = "MainActionBonus";
    }

    /**
     * The method that gives the bonus to the Player.
     */
    @Override
    public void giveBonus(Player p) {
    	p.setBonusAction(true);
    }
    
    /**
     * The method that overrides the superclass method and ignores the
     * value inserted because the only allowed value is 1.
     */
    @Override
    public void setValueBonus(int valueBonus) {
    	this.valueBonus = 1;
    }


}