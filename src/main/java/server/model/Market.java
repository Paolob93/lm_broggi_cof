package server.model;

import java.util.*;

/**
 * The Market class
 * 
 * The class that is used to perform the Market Actions. There is only one Market per Match.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Market {

	private List<ItemForSale> availableItems = new ArrayList<>();
    private PlayerList playerList;
    
    /**
     * The constructor of the class, it sets the PlayerList with the one given.
     * @param playerList
     */
    public Market(PlayerList playerList) {
    	this.playerList = playerList;
    }

    /**
     * The method that returns a String containing the details of all the available Items.
     * @return
     */
    public String showMarket(){
    	String marketDetails = new String();
    	if (!availableItems.isEmpty()){
    		for(ItemForSale item: getAvailableItems()){
    			marketDetails = marketDetails + "Item " + availableItems.indexOf(item) + " -> Player "+item.getSeller().getName()+" sells "+item.getItemDetails() +" for "+item.getPrice() + " coins.\n";
    		}
    	}
    	else {
    		marketDetails = "Sorry, there are no items avaiable!\n";
    	}
    	return marketDetails;
    }
    
    /**
     * The method that is called when the Market phase ends: it returns all the Item to the corrisponding seller.
     */
    public void returnItemAtOwner(){
    	for(ItemForSale item: getAvailableItems()){
    		item.returnUnsoldItem();
    	}
    	availableItems.clear();
    }
       
    /**
     * The method that generates a casual order for the Buy Phase of the Market.
     * @return
     */
    public int[] generateRandomOrder() {
    	int n = playerList.getNumPlayer();
    	int[] order = new int[n];
    	for(int i=0; i<n; i++){
    		order[i]=i;
    	}
    	for(int i=0; i<n; i++){
    		int random = (int)Math.random()*(n-1);
    		int temp = order[random];
    		order[random]=order[i];
    		order[i] = temp;
    	}
    	
    	return order;  
    }

	public List<ItemForSale> getAvailableItems() {
		return availableItems;
	}




    
   
    	
}