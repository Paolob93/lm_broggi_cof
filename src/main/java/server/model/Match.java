package server.model;

import java.rmi.RemoteException;


/**
 * The Match class.
 * 
 * The class that creates a new GameBoard and a new Market for every session of the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class Match {
  
    private transient PlayerList players;
    private transient GameBoard gameBoard;
    private transient Market market;
    private int lobbyID;

    /**
     * The constructor of the class. It creates a new Match and a new Market.
     * @param players
     * @param id
     * @throws RemoteException
     */
    public Match(PlayerList players, int id) throws RemoteException{
    	this.lobbyID = id;
    	this.players = players;
    	market = new Market(players);	
    }

    /**
     * The method that creates a new GameBoard with the given LobbyID.
     * @throws RemoteException
     */
    public void initMatch() throws RemoteException {
    	gameBoard = new GameBoard(lobbyID);
    }

    /**
     * The method that gives the VP to the two PlayerList that are in the best position.
     * of the NobilityTrack.
     */
    public void assignPointNobilityTrack(){
    	
    	int maxPosition = findTopPlayerNobilityTrack();
    	if(!checkMoreEqualsPosition(maxPosition, Constants.VP_FIRST_NT)){
    		int second = findSecondPlaceNobilityTrack(maxPosition);
    		checkMoreEqualsPosition(second, Constants.VP_SECOND_NT);
    	}
    }

    /**
     * The method that returns the position of the first Player in the NobilityTrack.
     * @return
     */
    public int findTopPlayerNobilityTrack(){
    	int max = 0;
    	for(Player p:players.getPlayers()){
    		if(p.getNobilityTrackPosition()>max){
    			max=p.getNobilityTrackPosition();
    		}
    	}
    	return max;
    }
    
    /**
     * The method that checks if there is more than one Player in the given position of
     * the NobilityTrack and that gives him or them the bonus.
     * @param max
     * @param point
     * @return
     */
    public boolean checkMoreEqualsPosition(int max, int point){
    	int cont = 0;
    	for(Player p:players.getPlayers()){
    		if(p.getNobilityTrackPosition()==max){
    			p.addVictoryPoints(point);
    			cont++;
    		}
    	}
    	if(cont==1)
    		return false;
    	return true;
    	
    }
    
    /**
     * The method that returns the position of the second Player in the NobilityTrack.
     * @return
     */
    public int findSecondPlaceNobilityTrack(int max){
    	int second =0;
    	for(Player p:players.getPlayers()){
    		if(p.getNobilityTrackPosition()>second && p.getNobilityTrackPosition()<max)
    			second=p.getNobilityTrackPosition();
    	}
    	
    	return second;
    }
    
    /**
     * The method that find the Player whit the highest number of PermitTiles and gives him the proper reward.
     */
    public void permitTileTopPlayer(){
    	int max = 0;
    	Player top=players.getPlayers().get(0);
    	for(Player p:players.getPlayers()){
    		int num = p.getPermitTiles().size() + p.getUsedPermitTiles().size();
    		if(num>max){
    			max=num;
    			top=p;
    		}
    	}
    	top.addVictoryPoints(Constants.VP_TOP_PT);
    }
    
    /**
     * The method that find the Player with the highest number of VP.
     * @return
     */
    public Player findWinner(){
    	int max=0;
    	Player winner = null;
    	for(Player p:players.getPlayers()){
    		if(p.getVictoryPoints()>max)
    			max=p.getVictoryPoints();
    	}
    	int cont=0;
    	for(Player p:players.getPlayers()){
    		if(p.getVictoryPoints()==max){
    			cont++;
    			winner=p;
    		}
    	}
    	if(cont!=1){
    		int max1 =0;
    		for(Player p: players.getPlayers())
    			if((p.getAssistants().size() + p.getPoliticCards().size()) > max1){
    				max1 = p.getAssistants().size() + p.getPoliticCards().size();
    				winner=p;
    			}
    	}
    	return winner;
    }

	public GameBoard getGameBoard() {
		return gameBoard;
	}

	public Market getMarket() {
		return market;
	}

	public int getNumPlayer(){
		return players.getNumPlayer();
	}

	public void setMarket(Market market) {
		this.market = market;
	}

}