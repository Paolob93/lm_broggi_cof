package server.model;

/**
 * The VPBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class MoneyBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to MoneyBonus.
	 */
    public MoneyBonus() {
    	this.type = "MoneyBonus";
    }
    
    /**
     * The creator of the Bonus.
     * Sets the number of Money that a Player will earn if he gets the Bonus.
     * @param num
     */
    public  MoneyBonus (int value) {
    	this.valueBonus = value;
    	this.type = "MoneyBonus";
    }
    
    /**
     * Adds to the Player the Money that is the ValueBonus parameter.
     */
    @Override
    public void giveBonus(Player p) {
    	p.addMoney(valueBonus);
    }


}