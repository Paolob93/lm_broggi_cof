package server.model;

/**
 * The NobilityTrack class.
 * 
 * The class that contains all the nobilityTrackTiles.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 *
 */
public class NobilityTrack {

	private NobilityTrackTile[] nobilityTrackArray;
	private String settingsPath;

	/**
	 * The constructor of the class that creates the nobilityTrackArray and sets the proper path.
	 * @param path
	 */
	public NobilityTrack(String path){
		this.settingsPath = path;
		nobilityTrackArray = new NobilityTrackTile[Constants.NUM_TILES];
	}

	/**
	 * The method that initializes every single tile.
	 */
	public void initNobilityTrack(){
		nobilityTrackArray[0] = new NobilityTrackTile(settingsPath);
		for(int i=1; i<nobilityTrackArray.length; i++ ){
			nobilityTrackArray[i] = new NobilityTrackTile(settingsPath);
			nobilityTrackArray[i].initTrackTile();
		}
	}


	public NobilityTrackTile[] getNobilityTrackArray() {
		return nobilityTrackArray;
	}

	public void setNobilityTrackArray(NobilityTrackTile[] nobilityTrackArray) {
		this.nobilityTrackArray = nobilityTrackArray;
	}




}
