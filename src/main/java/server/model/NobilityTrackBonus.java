package server.model;

/**
 * The NobilityTrackBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NobilityTrackBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to NobilityTrackBonus.
	 */
    public NobilityTrackBonus() {
    	this.type = "NobilityTrackBonus";
    }

    /**
     * The creator of the Bonus.
     * Sets the number of position on the NobilityTrack that a Player will earn if he gets the Bonus.
     * @param num
     */
    public NobilityTrackBonus (int points) {
    	this.valueBonus = points;
    	this.type = "NobilityTrackBonus";
    }

    /**
     * The method that moves forward the Player on the NobilityTrack.
     */
    @Override
    public void giveBonus(Player p) {
        p.incrementNobilityTrackPosition(valueBonus);
    }


}