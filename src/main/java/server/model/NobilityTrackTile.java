package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The NobilityTrackTile class.
 * 
 * The class that represents a single tile that may contain a Bonus[] or not.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class NobilityTrackTile {

	private Bonus[] rewards;
	private int bonusNumber;
	private boolean exist;
	private String nobilityTileDetails = new String(""); 
	private int maxValueBonusNT;
	private int maxBonusNumberNT;
	private String settingsPath;
	private Logger loggerDeck = Logger.getLogger(DeckPoliticCards.class.getName());


	/**
	 * The constructor that sets maxValueBonusNT and maxBonusNumberNT by reading them from the settings file
	 */
	public NobilityTrackTile(String path){
		settingsPath = path;
		try {
			File settings = new File (settingsPath);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			maxValueBonusNT = (int)(long) settingsObj.get("maxValueBonusNT");
			maxBonusNumberNT = (int)(long) settingsObj.get("maxBonusNumberNT");
		}
		catch (IOException | ParseException e) {
			loggerDeck.log(Level.INFO, e.getMessage(), e);
		}
		setExist(false);
	}

	/**
	 * L'è ul metud c'al fa' partì la tessera.
	 * Al decìd se la tessera ga d'avè ul bonus ol gal'ha mia d'avé:
	 * sal'ga l'ha cumincia a creàl!
	 */
	public  void initTrackTile(){
		int i;
		i = (int)(Math.random() * 2);
		if(i==1){
			setExist(true);
			generateBonusNumber();
			createBonus();
		}
	}


	/**
	 * Non modo hoc ratio efficere numerum adiumentatorum forte genitum concedit, verum etiam illud in NobilityTileDetails verbos opportunos disponit
	 */
	public void createBonus() {
		rewards = new Bonus[bonusNumber];
		boolean exit;
		for (int i=0; i < this.bonusNumber; i++) {
			do {
				exit = false;
				int bonusType = (int) (9 * Math.random());
				while (bonusType == 3) {
					bonusType = (int) (9 * Math.random());
				}
				rewards[i] = Bonus.generateBonus(bonusType);
				rewards[i].setValueBonus((int) (maxValueBonusNT * Math.random()) + 1);
				exit = checkDuplicatedBonuses(i);
			}
			while (exit);
			nobilityTileDetails = nobilityTileDetails + "Bonus type: " + rewards[i].getType() + " -> value: " + rewards[i].getValueBonus() + "\n\t\t";	
		}	
	}
	
	
	public String getNobilityTrackDetails() {
		return nobilityTileDetails;
	}

	/**
	 * The method that generates randomly the number of the bonus in the tile.
	 */
	private void generateBonusNumber() {
		bonusNumber = (int) ((maxBonusNumberNT) * Math.random()) + 1;
	}

	/**
	 * The method that gives all the bonus to the Player.
	 * @param p
	 */
	public void giveRewards(Player p){
		for(int i=0; i<bonusNumber; i++){
			rewards[i].giveBonus(p);
		}
	}

	/**
	 * The method that checks if the generated bonus is already present in the Bonus[].
	 * @param i
	 * @return
	 */
	private boolean checkDuplicatedBonuses(int i){
		boolean exit = false;
		for (int k = 0; k < i; k++) {
			if (rewards[i].getType().equals(rewards[k].getType())){
				exit = true;
			}
		}
		return exit;
	}
	
	public boolean getExist(){
		return this.exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}


}