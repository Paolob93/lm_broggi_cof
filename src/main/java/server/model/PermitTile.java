package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The PermitTile class.
 * 
 * It contains the bonus[] that the Player gains when he buys the tile and the Cities in which the 
 * Player can build an Emporium.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class PermitTile implements Tradable {

	private Bonus[] bonus;
	private City[] buildingTiles;
	private int citiesNumber;
	private int bonusNumber;
	private RegionBoard region;
	private int maxBonusNumberPT;
	private int maxValueBonusPT;
	private String permitTileDetails = new String("");
	private String settingsPath;
	private Logger loggerPermitTile = Logger.getLogger(PermitTile.class.getName());

	/**
	 * The constructor of the class. It takes the region in which the Tile will be
	 * and sets the bonuses.
	 * @param region
	 */
	public PermitTile(RegionBoard region) {
		this.region = region;
		settingsPath = region.getGameboard().getSettingsPath();
		try {
			File settings = new File(settingsPath);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			maxValueBonusPT = (int)(long) settingsObj.get("maxValueBonusPT");
			maxBonusNumberPT = (int)(long) settingsObj.get("maxBonusNumberPT");
		}
		catch (IOException | ParseException e) {
			loggerPermitTile.log(Level.INFO, e.getMessage(), e);
		}
		this.permitTileDetails = new String();
		this.generateBonusNumber();
		this.generateBonus();
	}

	/**
	 * An helper constructor used during the tests.
	 * @param cities
	 * @param path
	 */
	public PermitTile(City[] cities, String path) {
		try {
			File settings = new File(path);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			maxValueBonusPT = (int)(long) settingsObj.get("maxValueBonusPT");
			maxBonusNumberPT = (int)(long) settingsObj.get("maxBonusNumberPT");
		}
		catch (IOException | ParseException e) {
			loggerPermitTile.log(Level.INFO, e.getMessage(), e);
		}
		this.buildingTiles = cities;
		this.permitTileDetails = new String();
		this.generateBonusNumber();
		this.generateBonus();
		this.citiesNumber = cities.length;
	}

	/**
	 * This method generates randomly the number of bonuses present on this permit tile
	 */
	private void generateBonusNumber() {
		this.bonusNumber = (int) (maxBonusNumberPT * Math.random()) + 1;
	}

	/**
	 * This method generates randomly a set of bonuses by calling the static method in the Bonus class
	 * and sets the proper details in the permitTileDetails.
	 */
	public void generateBonus() {
		bonus = new Bonus[bonusNumber];
		boolean exit;
		int i;
		for (i = 0; i< this.bonusNumber; i++) {
			do {
				exit = false;
				int bonusType = (int) (6 * Math.random());
				bonus[i] = Bonus.generateBonus(bonusType);
				bonus[i].setValueBonus((int) (maxValueBonusPT * Math.random()) + 1);
				exit = checkDuplicatedBonuses(i);
			}
			while (exit);
			permitTileDetails = permitTileDetails + "\tBonus type: " + bonus[i].getType() + " -> value: " + bonus[i].getValueBonus() + "\n";
		}
	}

	/**
	 * The method that sets the Cities in which a Player can build with this Tile by reading them from a file.
	 * @param tile
	 */
	public void setBuildingTiles(int tile) {
		String type = region.getType().toString();
		try {
			File fileTiles = new File("./src/main/java/server/model/PermitTiles.json");
			JSONParser parser = new JSONParser();
			JSONArray regions = (JSONArray) parser.parse(new FileReader(fileTiles));
			JSONObject currentRegion = (JSONObject) regions.get(region.getRegionNumber());
			JSONArray citiesArray = (JSONArray) currentRegion.get(type);
			JSONObject currentCity = (JSONObject) citiesArray.get(tile);
			JSONArray currentCities = (JSONArray) currentCity.get("Cities");
			Iterator<?> citiesIterator = currentCities.iterator();
			this.citiesNumber = currentCities.size();
			buildingTiles = new City[citiesNumber];
			int position = 0;
			while (citiesIterator.hasNext()) {
				String nextCity = (String) citiesIterator.next();
				City cit = region.getGameboard().getCities().get(nextCity);
				buildingTiles[position] = cit;
				position++;
			}
			for (int k = 0; k < this.getCitiesNumber(); k++) {
				permitTileDetails = permitTileDetails + "\tYou can build in: " + getBuildingTiles()[k].getName() + "\n";
			}
		}
		catch (IOException | ParseException e) {
			loggerPermitTile.log(Level.INFO, e.getMessage(), e);
		}
	}
	
	/**
	 * The method that checks if the generated bonus is already present in the Bonus[].
	 * @param i
	 * @return
	 */
	private boolean checkDuplicatedBonuses(int i){
		boolean exit = false;
		for (int k = 0; k < i; k++) {
			if (bonus[i].getType().equals(bonus[k].getType())){
				exit = true;
			}
		}
		return exit;
	}

	/**
	 * The method that allows the Player to buy a Permit tile from the Market.
	 */
	@Override
	public void buy(Player player) {
		player.getPermitTiles().add(this);		
	}
	
	public String getPermitTileDetails() {
		return this.permitTileDetails;
	}


	public Bonus[] getBonus() {
		return this.bonus;
	}

	public void giveBonus(Player p) {
		for (Bonus b: bonus) {
			b.giveBonus(p);
		}
	}

	public City[] getBuildingTiles() {
		return buildingTiles;
	}


	

	public int getCitiesNumber() {
		return citiesNumber;
	}


	public void setBonus(Bonus[] b) {
		String permitTileDetails1 = new String("");
		this.bonus = b;
		this.bonusNumber = b.length;
		for (int i = 0; i < bonusNumber; i++) {
			permitTileDetails1 = permitTileDetails1 + "\tBonus type: " + bonus[i].getType() + " -> value: " + bonus[i].getValueBonus() + "\n";
		}
		permitTileDetails = permitTileDetails1;
	}

}
