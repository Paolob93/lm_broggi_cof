package server.model;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import common.exception.BuildEmporiumException;
import common.exception.NoPermitTilesException;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import common.exception.WrongCardsException;
import common.exception.YouArePoorException;

/**
 * The Player class.
 * 
 * THE CLASS that contains all the methods that a Player could call during the Game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 *
 */
public class Player {

	private String name;
	private PlayerColors color;
	private int money;
	private int victoryPoints;
	private int nobilityTrackPosition;
	private int iD;
	private List<PoliticCard> politicsCardPlayer = new ArrayList<>();
	private List<Assistant> assistants = new ArrayList<>();
	private List<PermitTile> permitTiles = new ArrayList<>();
	private List<PermitTile> usedPermitTiles = new ArrayList<>();
	private List<City> citiesOwned = new ArrayList<>();    
	private Match match;
	private GameBoard gameBoard;
	private NobilityTrack nobilityTrack;
	private Market market;
	private boolean mainActionDone;
	private boolean quickActionDone;
	private boolean bonusAction = false;
	private boolean fuBonus = false;
	private boolean fdBonus = false;
	private int rtBonus = 0;

	/**
	 * The constructor of the class that sets all the given values to the proper field.
	 * @param name
	 * @param color
	 * @param match
	 * @param iD
	 * @throws RemoteException
	 */
	public Player(String name, PlayerColors color, Match match, int iD) throws RemoteException {
		this.iD = iD;
		this.setName(name);
		this.setColor(color);
		this.money = 0;
		this.victoryPoints = 0;
		this.nobilityTrackPosition = 0;
		this.match = match;
		this.gameBoard = match.getGameBoard();
		this.nobilityTrack = this.gameBoard.getKingBoard().getNobilityTrack();
		this.market = match.getMarket();
		this.setMainActionDone(false);
		this.setQuickActionDone(false);
	}

	/**
	 * The constructor of a sad Player, used during the tests.
	 * @param name
	 * @param m
	 * @throws RemoteException
	 */
	public Player(String name, Match m)throws RemoteException{
		this.setName(name);
		this.money = 0;
		this.victoryPoints = 0;
		this.nobilityTrackPosition = 0;
		this.match = m;
		this.gameBoard = match.getGameBoard();
		this.nobilityTrack = gameBoard.getKingBoard().getNobilityTrack();
		this.market = match.getMarket();
		this.setMainActionDone(false);
		this.setQuickActionDone(false);
	}
	
	/**
	 * The constructor that copies a player p into a new Player
	 * @param p
	 */
	public Player(Player p) {
		this.name = p.getName();
		this.color = p.getColor();
		this.money = p.money;
		this.victoryPoints = p.victoryPoints;
		this.nobilityTrackPosition = p.nobilityTrackPosition;
		this.iD = p.iD;
		this.politicsCardPlayer.addAll(p.politicsCardPlayer);
		this.permitTiles.addAll(p.permitTiles);
		this.assistants.addAll(p.assistants);
		this.copyThings(p);
	}

	/**
	 * The method that restores a player after an exception
	 * @param p
	 */
	public void restorePlayer(Player p) {
		this.permitTiles.clear();
		this.assistants.clear();
		this.politicsCardPlayer.clear();
		this.usedPermitTiles.clear();
		this.citiesOwned.clear();
		this.money = p.money;
		this.victoryPoints = p.victoryPoints;
		this.nobilityTrackPosition = p.nobilityTrackPosition;
		this.permitTiles.addAll(p.permitTiles);
		this.assistants.addAll(p.assistants);
		this.politicsCardPlayer.addAll(p.politicsCardPlayer);
		this.copyThings(p);
	}

	/**
	 * The first action that a player must do in every round.
	 */
	public void drawPoliticCard() {
		politicsCardPlayer.add(gameBoard.getDeck().getPoliticCard());
	}
	
	
	/* *****	-	****	-	***** */
	/* ***** 	MAIN ACTIONS 	***** */
	/* *****	-	****	-	***** */

	/**
	 * This method allows a player to elect a councillor in a specific region as a main action
	 * @param color
	 * @param region
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 */
	public void electCouncillor(PoliticColors color, RegionBoard region) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException  {
	
		region.getBalcony().elect(color);	
		this.addMoney(Constants.REWARD_FOR_ELECTION);
		if (getBonusAction()) {
			this.setBonusAction(false);
		}
		else {
			this.setMainActionDone(true);
		}
	}
	
	/**
	 * This method allows a player to elect a councillor only in the King Council as a main action
	 * @param color
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 */
	public void electKingCouncillor(PoliticColors color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
	
			this.gameBoard.getKingBoard().getBalcony().elect(color);
	
		this.addMoney(Constants.REWARD_FOR_ELECTION);
		if (getBonusAction()) {
			this.setBonusAction(false);
		}
		else {
			this.setMainActionDone(true);
		}
	}  

	/**
	 * This method allows a player to buy a permit tile as a main action
	 * @param region
	 * @param tile
	 * @param cards
	 * @throws YouArePoorException
	 * @throws WrongCardsException
	 */
	public void buyPermitTile(RegionBoard region, int tile, List<PoliticCard> cards) throws YouArePoorException, WrongCardsException {
		int jollyCardsCounter = PoliticCard.getJollyCardsNumber(cards); 
		
		if (region.getBalcony().satisfyCouncil(cards)) {
			int totalExpense;
			if (cards.size() <= 3) {
				totalExpense = 10 - (((cards.size()) - 1) * 3) + jollyCardsCounter;
			}
			else {
				totalExpense = jollyCardsCounter;
			}
			
			PermitTile ptChoosen = region.getTile(tile);
			region.changeTileUsed( tile);
			this.getPermitTiles().add(ptChoosen);
			for (PoliticCard card: cards) {
				this.discardPoliticCard(card);
			}
			this.removeMoney(totalExpense);
			ptChoosen.giveBonus(this);
			if (getBonusAction()) {
				this.setBonusAction(false);
			}
			else {
				this.setMainActionDone(true);
			}
		}
		else throw new WrongCardsException();
	}

	/**
	 * This method allows a player to build an emporium and gives the bonus to the player, it also eventually gives the bonus available for having 
	 * @param city
	 * @param permitTile
	 * @throws BuildEmporiumException
	 * @throws NotAvailableAssistantsException
	 */
	public void buildEmporium(City city, PermitTile permitTile) throws BuildEmporiumException, NotAvailableAssistantsException {
		boolean canBuild = false;
		for (int i = 0; i < permitTile.getCitiesNumber(); i++) {
			if (permitTile.getBuildingTiles()[i].equals(city) && !city.getEmporiums().contains(this)) {
				canBuild = true;
			}
		}
		if (canBuild) {
			try {
				for (int i = 0; i < city.getEmporiums().size(); i++) {
					this.useAssistant();
				}
				city.getEmporiums().add(this);
				this.getPermitTiles().remove(permitTile);
				this.usedPermitTiles.add(permitTile);
				addCityAndGiveBonus(city);
			}
			catch (NotAvailableAssistantsException e) {
				throw new NotAvailableAssistantsException(e);
			}	
		}
		else {
			throw new BuildEmporiumException();
		}
	}

	/**
	 * This method allows to build an emporium using the king, 
	 * so you don't have to own the correct permit tile in order to perform this action.
	 * @param city
	 * @param cards
	 * @throws YouArePoorException
	 * @throws WrongCardsException
	 * @throws NotAvailableAssistantsException
	 * @throws BuildEmporiumException 
	 */
	public void buildEmporiumByKing(City city, List<PoliticCard> cards) throws YouArePoorException, WrongCardsException, NotAvailableAssistantsException, BuildEmporiumException {
	if(!city.getEmporiums().contains(this)){
		int moneyTotal;
		int jollyCardsCounter = PoliticCard.getJollyCardsNumber(cards); 
		if (this.gameBoard.getKingBoard().getBalcony().satisfyCouncil(cards)) {
			if (cards.size() <= 3) {
				moneyTotal =  10 - (((cards.size()) - 1) * 3) + jollyCardsCounter;
			}
			else {
				moneyTotal =  jollyCardsCounter;
			}
			moneyTotal = moneyTotal + 2 * this.gameBoard.getKing().getDistance(city);

			for (PoliticCard card: cards) {
				this.discardPoliticCard(card);
			}
			this.gameBoard.getKing().setPosition(city);
			removeMoney(moneyTotal);
			for (int i = 0; i < city.getEmporiums().size(); i++) {
				this.useAssistant();
			}
			city.getEmporiums().add(this);
			addCityAndGiveBonus(city);
		}
		else throw new WrongCardsException();
	}
	else throw new BuildEmporiumException();
}

	/* *****	-	****	-	***** */
	/* ***** 	QUICK ACTIONS 	***** */
	/* *****	-	****	-	***** */

	/**
	 * This method allows a player to elect a councillor in a specific region as a quick action
	 * @param color
	 * @param region
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 * @throws NotAvailableAssistantsException
	 */
	public void electCouncillorByAssistant(PoliticColors color, RegionBoard region) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException {
		region.getBalcony().elect(color);
		this.useAssistant();
		this.setQuickActionDone(true);
		if (getBonusAction()) {
			this.setBonusAction(false);
			this.setMainActionDone(false);
		}
	}

	/**
	 * This method allows a player to elect a councillor only in the King Council as a quick action
	 * @param color
	 * @throws NotAvailableCouncillorsException
	 * @throws NotAvailableColorCouncillorsException
	 * @throws NotAvailableAssistantsException
	 */
	public void electKingCouncillorByAssistant(PoliticColors color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException {
		this.gameBoard.getKingBoard().getBalcony().elect(color);
		this.useAssistant();
		this.setQuickActionDone(true);
		if (getBonusAction()) {
			this.setBonusAction(false);
			this.setMainActionDone(false);
		}
	}

	/**
	 * This method allows a player to buy an assistant as a quick action
	 * @throws NotAvailableAssistantsException
	 * @throws YouArePoorException
	 */
	public void buyAssistant() throws NotAvailableAssistantsException, YouArePoorException{
		assistants.add(this.gameBoard.getAssistant());
		this.removeMoney(Constants.ASSISTANT_PRICE);
		this.setQuickActionDone(true);
		if (getBonusAction()) {
			this.setBonusAction(false);
			this.setMainActionDone(false);
		}
	}

	/**
	 * This method allows a player to change the face up permit tiles available in a RegionBoard
	 * @param region
	 * @throws NotAvailableAssistantsException
	 */
	public void changeTiles(RegionBoard region) throws NotAvailableAssistantsException {
		region.changeTile();
		useAssistant();
		setQuickActionDone(true);
		if (getBonusAction()) {
			this.setBonusAction(false);
			this.setMainActionDone(false);
		}
	}

	/**
	 * This method allows the player to perform another main action
	 * @throws NotAvailableAssistantsException
	 */
	public void getAnotherMainAction() throws NotAvailableAssistantsException {
		for (int i = 0; i < Constants.ASSISTANTS_FOR_MAIN_ACTION; i++){
			this.useAssistant();
		}
		this.setMainActionDone(false);
		this.setQuickActionDone(true);
	}
	
	/* *****	-	****	-	***** */
	/* *****	MARKET STUFF	***** */
	/* *****	-	****	-	***** */
	
	/**
	 * This method allows the player to sell an Assistant
	 * @param price
	 * @throws NotAvailableAssistantsException
	 */
	public void sellAssistant(int price) throws NotAvailableAssistantsException {
		sell(this.getFirstAvailableAssistant(), price);
	}
	
	/**
	 * This method allows the player to sell a certain number of Assistants
	 * @param number
	 * @param price
	 * @throws NotAvailableAssistantsException
	 */
	public void sellAssistant(int number, int price) throws NotAvailableAssistantsException {
		for (int i= 0; i < number; i++) {
			sell(this.getFirstAvailableAssistant(), price);
		}
	}
	
	/**
	 * This method allows the player to sell an Assistant
	 * @param assistant
	 * @param price
	 */
	private void sell(Assistant assistant, int price) {
		ItemForSale saleObject = new ItemForSale(assistant, price, this, assistant.getClass().getSimpleName());
		getMarket().getAvailableItems().add(saleObject);
		assistants.remove(assistant);
	}

	/**
	 * This method allows the player to sell a Politic Card
	 * @param card
	 * @param price
	 */
	public void sell(PoliticCard card, int price) {
		String details = card.getClass().getSimpleName() + ": " + card.getStringColor();
		ItemForSale saleObject = new ItemForSale(card, price, this, details);
		getMarket().getAvailableItems().add(saleObject);
		politicsCardPlayer.remove(card);
	}

	/**
	 * This method allows the player to sell a Permit Tile
	 * @param permitTile
	 * @param price
	 */
	public void sell(PermitTile permitTile, int price) {
		String details = permitTile.getClass().getSimpleName() +": "+ permitTile.getPermitTileDetails();
		ItemForSale saleObject = new ItemForSale(permitTile, price, this, details);
		getMarket().getAvailableItems().add(saleObject);
		permitTiles.remove(permitTile);
	}

	/**
	 * This method allows the player to buy something from the market
	 * @param item
	 * @throws YouArePoorException
	 */
	public void buyItem(ItemForSale item) throws YouArePoorException{
		item.buy(this);
	}

	/* *****	-	****	-	***** */
	/* *****	BONUS STUFF		***** */
	/* *****	-	****	-	***** */

	/**
	 * This method allows a player to draw a specific number of cards as established in the bonus
	 * @param numOfCards
	 */
	public void drawPoliticCard(int numOfCards) {
		for (int i = 0; i < numOfCards; i++) {
			politicsCardPlayer.add(gameBoard.getDeck().getPoliticCard());
		}
	}

	public void addAssistant(int assistNumber) throws NotAvailableAssistantsException {
		for (int i = 0; i < assistNumber; i++) {
			assistants.add(this.gameBoard.getAssistant());
		}
	}

	/**
	 * This method adds a certain amount of money
	 * @param cash
	 */
	public void addMoney(int cash) {
		this.money += cash;
	}

	/**
	 * This method adds some Victory Points
	 * @param points
	 */
	public void addVictoryPoints(int points) {
		this.victoryPoints += points;
	}

	/**
	 * This method increases the position on the Nobility Track
	 * @param points
	 */
	public void incrementNobilityTrackPosition (int points) {
		this.nobilityTrackPosition += points;
		this.checkNobilityTrackPosition();
	}

	/**
	 * This method returns a string containing the details of all player's available Permit Tiles
	 * @return
	 * @throws NoPermitTilesException 
	 */
	public String showFDTiles() throws NoPermitTilesException {
		String showtiles = new String();
		int i= 0;
		for (PermitTile pt: createAvailablePT()) {
			showtiles = showtiles + "Permit Tile #" + i + " " + pt.getPermitTileDetails();
			i++;
		}
		return showtiles;	
	}
	
	/**
	 * The method that returns the number of available and used PermitTiles.
	 * @return
	 */
	 public int getNumberOkFDTiles(){
		 return getPermitTiles().size() + getUsedPermitTiles().size();
	 }
	
	/**
	 * This method returns a Permit Tile that will be used in the FaceDownPermitTileBonus
	 * @return
	 * @throws NoPermitTilesException 
	 */
	public PermitTile chooseTile(int index) throws NoPermitTilesException {
		return createAvailablePT().get(index);
	}

	/**
	 * This method returns a string containing the details of all GameBoard's available Permit Tiles
	 * @return
	 */
	public String showFUTiles() {
		String showtiles = new String();
		int i= 0;
		for (PermitTile pt: createAvailableGameboardPT()) {
			showtiles = showtiles + "Permit Tile #" + i + " " + pt.getPermitTileDetails();
			i++;
		}
		return showtiles;		
	}
	
	/**
	 * This method allows a player to choose a face up Permit Tile from a RegionBoard
	 * and adds it to the set of Permit Tiles of the player 
	 */
	public void chooseFaceUpPT(int index) {
		PermitTile permitAdded = this.gameBoard.getBoards()[index/2].getTile(index%2);
		this.permitTiles.add(permitAdded);
		permitAdded.giveBonus(this);
	}

	/**
	 * This method allows a player to gain all the bonuses from a reward token on the gameboard
	 * @return
	 */
	public RewardToken chooseTokenReward(int index) {
		return this.citiesOwned.get(index).getRewardToken();
	}
	
	/**
	 * This method returns a string containing the details of all the tokens that
	 * are in the cities in which the player has an emporium
	 * @return
	 */
	public String showPlayerRewardTokens() {
		String showtokens = new String();
		int i= 0;
		for (City c: this.getCitiesOwned()) {
			if (!c.isNoble()){
				showtokens = showtokens + "Reward Token #" + i + " " + c.getRewardToken().getRewardTokenDetails();
			}
			i++;
		}
		return showtokens;
	}
	
	/**
	 * The method that returns an array containing all the numbers of the RewardTokens allowed to be called.
	 * @return
	 */
	public int[] getNumberOkRT() {
		int lenght = 0;
		for (City c: this.getCitiesOwned())
			if (!c.isNoble())
				lenght++;
		int[] numbersOk = new int[lenght];
		int ok = 0;
		for (City c: this.getCitiesOwned()) {
			if (!c.isNoble())
				numbersOk[ok] = ok;
			ok++;
		}
		return numbersOk;
	}

	/**
	 * This method removes money from the player
	 * @param cash
	 * @throws YouArePoorException
	 */
	public void removeMoney(int cash) throws YouArePoorException {
		if (this.money - cash >= 0) {
			this.money -= cash;
		}
		else throw new YouArePoorException();
	}

	/* *****	-	********	-	***** */
	/* *****	GETTERS AND SETTERS	***** */
	/* *****	-	********	-	***** */
	
	public int getMoney() {
		return this.money;
	}

	
	public int getVictoryPoints() {
		return this.victoryPoints;
	}


	public List<City> getCitiesOwned() {
		return citiesOwned;
	}

	public PlayerColors getColor() {
		return color;
	}


	public List<PermitTile> getPermitTiles() {
		return permitTiles;
	}

	public List<Assistant> getAssistants(){
		return assistants;
	}


	public List<PoliticCard> getPoliticCards(){
		return politicsCardPlayer;
	}

	public String getName() {
		return name;
	}


	public boolean getMainActionDone() {
		return mainActionDone;
	}


	public boolean getQuickActionDone() {
		return quickActionDone;
	}


	public Market getMarket() {
		return market;
	}

	public int getNumEmporiums() {
		return getCitiesOwned().size();
	}

	public int getNobilityTrackPosition() {
		return nobilityTrackPosition;
	}

	public void setMainActionDone(boolean mainActionDone) {
		this.mainActionDone = mainActionDone;
	}

	public void setQuickActionDone(boolean quickActionDone) {
		this.quickActionDone = quickActionDone;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColor(PlayerColors color) {
		this.color = color;
	}
	
	public List<PermitTile> getUsedPermitTiles() {
		return this.usedPermitTiles;
	}

	public boolean getBonusAction() {
		return bonusAction;
	}

	public void setBonusAction(boolean bonusAction) {
		this.bonusAction = bonusAction;
	}
	
	public int getID(){
		return iD;
	}

	/**
	 * @return the fuBonus
	 */
	public boolean isFuBonus() {
		return fuBonus;
	}

	/**
	 * @param fuBonus the fuBonus to set
	 */
	public void setFuBonus(boolean fuBonus) {
		this.fuBonus = fuBonus;
	}

	/**
	 * @return the fdBonus
	 */
	public boolean isFdBonus() {
		return fdBonus;
	}

	/**
	 * @param fdBonus the fdBonus to set
	 */
	public void setFdBonus(boolean fdBonus) {
		this.fdBonus = fdBonus;
	}

	/**
	 * @return the rtBonus
	 */
	public int getRtBonus() {
		return rtBonus;
	}

	/**
	 * @param rtBonus the rtBonus to set
	 */
	public void setRtBonus(int rtBonus) {
		this.rtBonus = rtBonus;
	}
	
	public GameBoard getGameBoard() {
		return this.gameBoard;
	}
	
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
	

	/* *****	-	****	-	***** */
	/* *****	PRIVATE METHODS	***** */
	/* *****	-	****	-	***** */
	

	/**
	 * Create an ArrayList containing all the Permit Tile owned - available and used - by the player
	 * @return
	 * @throws NoPermitTilesException
	 */
	private List<PermitTile> createAvailablePT() throws NoPermitTilesException {
		if ((permitTiles.size() + usedPermitTiles.size()) > 0) {
			ArrayList<PermitTile> availablePT = new ArrayList<>();
			availablePT.addAll(this.getPermitTiles());
			availablePT.addAll(this.usedPermitTiles);
			return availablePT;
		}
		else 
			throw new NoPermitTilesException();
	}

	/**
	 * This method create an ArrayList containing all the Permit Tiles visible on the GameBoard
	 * @return
	 */
	private List<PermitTile> createAvailableGameboardPT(){
		List<PermitTile> availablePT = new ArrayList<>();
		for (RegionBoard region: this.gameBoard.getBoards()) {
			for (PermitTile pt: region.getBusinessPermitTiles()) {
				availablePT.add(pt);
			}
		}
		return availablePT;
	}

	/**
	 * This method returns the first available Assistant in the set of assistants owned by the player
	 * @return
	 * @throws NotAvailableAssistantsException
	 */
	private Assistant getFirstAvailableAssistant() throws NotAvailableAssistantsException {
		if (!assistants.isEmpty()) {
			return assistants.get(0);
		}
		else {
			throw new NotAvailableAssistantsException();
		}
	}

	/**
	 * This method checks if there is a bonus in the current position of the nobility track 
	 * and, if the bonus is present, gives the bonus to the player
	 */
	private void checkNobilityTrackPosition() {
		if (this.nobilityTrack.getNobilityTrackArray()[this.nobilityTrackPosition].getExist())
			this.nobilityTrack.getNobilityTrackArray()[this.nobilityTrackPosition].giveRewards(this);
	}

	/**
	 * This method discards a politic card, is used when the player has to buy a Permit Tile 
	 * or has to build an emporium using the King
	 * @param card
	 */
	private void discardPoliticCard(PoliticCard card) {
		this.gameBoard.getDeck().addRejectedCard(card);
		this.politicsCardPlayer.remove(card);
	}

	/**
	 * This method allows the player to use an Assistant and remove it from the set of assistants
	 * @throws NotAvailableAssistantsException
	 */
	private void useAssistant() throws NotAvailableAssistantsException {
		this.gameBoard.addAssistant(getFirstAvailableAssistant());
		assistants.remove(getFirstAvailableAssistant());
	}

	/**
	 * The method that adds a city to the sets of cities of the player and give all the necessary bonuses.
	 * @param city
	 */
	private void addCityAndGiveBonus(City city){
		this.citiesOwned.add(city);
		city.getRewardToken().giveBonus(this);
		for (City near: city.nearCitiesPlayer(this)) {
			if (!(near.equals(city))) {
				near.getRewardToken().giveBonus(this);
			}
		}
		if (!(city.getColor().equals(CityColors.VIOLET)) && this.gameBoard.getBonusCityTile(city.getColor()).playerCanTakeTheBonus(this)) {
			this.gameBoard.getBonusCityTile(city.getColor()).giveBonus(this);
			this.gameBoard.giveKingTileBonus(this);
		}
		this.gameBoard.getCityRegion(city).getBonusAllCities(this);
		if (getBonusAction()) {
			this.setBonusAction(false);
		}
		else {
			this.setMainActionDone(true);
		}
	}
	
	/**
	 * The method that copies the needed values in the proper way.
	 * @param p
	 */
	private void copyThings(Player p) {
		this.usedPermitTiles.addAll(p.usedPermitTiles);
		this.citiesOwned.addAll(p.citiesOwned);
		this.match = p.match;
		this.gameBoard = p.gameBoard;
		this.nobilityTrack = p.nobilityTrack;
		this.market = p.market;
		this.mainActionDone = p.mainActionDone;
		this.quickActionDone = p.quickActionDone;
		this.bonusAction = p.bonusAction;
		this.fuBonus = p.fuBonus;
		this.fdBonus = p.fdBonus;
		this.rtBonus = p.rtBonus;
	}

}