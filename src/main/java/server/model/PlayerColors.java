package server.model;

/**
 * The PlayerColors enum.
 * 
 * The class that contains the colors that a Player can assume.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public enum PlayerColors {
	ORANGE,
	WHITE,
	BLACK,
	BLUE,
	PINK,
	VIOLET,
	RED,
	PURPLE,
	GREY,
	BROWN,
	YELLOW        
}