package server.model;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * The PlayerList class.
 * 
 * The class that contains a List of Players that are playing the same session of the game.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class PlayerList {

	private List<Player> playerListMatch;
	private int playersNumber=0;
	
	/**
	 * The constructor of the class that instantiates a new ArrayList of Player.
	 * @throws RemoteException
	 */
	public PlayerList() throws RemoteException{
		playerListMatch = new ArrayList<>(2);
	}
    
	/**
	 * The method that add a Player to the ArrayList of Players and increment  the playerNumber.
	 * @param player
	 */
    public void addPlayer(Player player) {
    	playerListMatch.add(player);
    	playersNumber++;
    }
    
    public int getNumPlayer(){
    	return playersNumber;
    }
 
    public List<Player> getPlayers(){
    	return playerListMatch;
    }
}