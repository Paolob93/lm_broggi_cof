package server.model;

import java.rmi.RemoteException;
import java.util.List;

/**
 * The PoliticCard class.
 * 
 * The class that represents a PoliticCard, it's characterized by a color or a boolean that says
 * that the PoliticCard can be used for every color.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class PoliticCard implements Tradable {
	
	private PoliticColors color;
	private boolean inDeck;
	private boolean jolly;

	/**
	 * The default constructor of the class that sets the jolly flag to false, sets the card in the Deck and
	 * don't assign any color.
	 * @throws RemoteException
	 */
    public PoliticCard() throws RemoteException{
    	inDeck = true;
    	jolly = false;
    	color = null;
    }
    
    /**
     * The constructor for a jolly card.
     * @param jolly
     * @throws RemoteException
     */
    public PoliticCard(boolean jolly)throws RemoteException{
    	inDeck = true;
    	this.jolly = jolly;
    	color = null;
    }
    
    /**
     * The constructor for a color card.
     * @param color
     * @throws RemoteException
     */
    public PoliticCard(PoliticColors color) throws RemoteException{
    	inDeck = true;
    	jolly = false;
    	this.color = color;
    }

    /**
     * The static method that counts the number
     * of jolly cards present in a gives List of PoliticCard.
     * @param cards
     * @return
     */
    public static int getJollyCardsNumber(List<PoliticCard> cards) {
    	int jollyCardsNumber = 0;
    	for (PoliticCard card: cards) {
    		if (card.getJolly()) {
    			jollyCardsNumber++;
    		}
    	}
    	return jollyCardsNumber;
    }

    /**
     * The method that allows a Player to buy a PoliticCard from the Market.
     */
    @Override
	public void buy(Player player) {
		player.getPoliticCards().add(this);
	}
    
    public boolean getInDeck() {
        return inDeck;
    }

    public void setInDeck() {
        inDeck = true;
    }
    
    public void setInDeck(boolean inDeck){
    	this.inDeck = inDeck;
    }

    public void setColor(PoliticColors color) {
    	this.color = color;
    }
    
    public boolean getJolly() {
        return jolly;
    }

    public void setJolly() {
        jolly = true;
    }
    
    public PoliticColors getColor() {
        return color;
    }
    
    public String getStringColor(){
    	if(jolly)
    		return "JOLLY";
    	return color.toString();
    }
}