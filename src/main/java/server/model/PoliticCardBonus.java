package server.model;

/**
 * The PoliticCardBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class PoliticCardBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to PoliticCardBonus.
	 */
    public PoliticCardBonus() {
    	this.valueBonus = 1;
    	this.type = "PoliticCardBonus";
    }

    /**
     * The creator of the Bonus.
     * Sets the number of cards that a Player will draw if he gets the Bonus.
     * @param num
     */
    public PoliticCardBonus(int i) {
    	this.valueBonus = i;
    	this.type = "PoliticCardBonus";
    }

    /**
     * Makes the Player draw a valueBonus number of cards.
     */
    @Override
    public void giveBonus(Player p) {
        p.drawPoliticCard(valueBonus);
    }
}