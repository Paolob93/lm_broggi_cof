package server.model;

/**
 * The PoliticColors enum.
 * 
 * The class that contains the color that could be find on a PoliticCard.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public enum PoliticColors {
	ORANGE,
	WHITE,
	BLACK,
	BLUE,
	PINK,
	VIOLET;	
}