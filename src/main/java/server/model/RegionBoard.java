package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The RegionBoard class.
 * 
 * The class that has a BoardType and contains a Council and a set of PermitTiles.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class RegionBoard {

	private int regionNumber = 0;
	private BoardType type;
	private PermitTile[] businessPermitTiles;
	private City[] citiesInBoard;
	private List<PermitTile> permitTileDeck;
	private Council balcony;
	private VPBonus regionBonus = new VPBonus(Constants.REGION_BONUS_VALUE);
	private GameBoard gameBoard;
	private Logger loggerRegionBoard = Logger.getLogger(RegionBoard.class.getName());

	/**
	 * The constructor of the class that sets the BoardType to the given one, 
	 * it also initializes a new Council and creates the PermitTiles.
	 * @param type
	 * @param gameBoard
	 * @param number
	 */
	public RegionBoard(BoardType type, GameBoard gameBoard, int number) {
		this.regionNumber = number;
		this.type = type;
		this.gameBoard = gameBoard;
		setCitiesInBoard(new City[Constants.CITIES_PER_BOARD]);
		balcony = new Council(gameBoard);
		setPermitTileDeck(new ArrayList<>());
		businessPermitTiles = new PermitTile[2];
		this.initCities();
		this.initPermitTileDeck();
		Collections.shuffle(getPermitTileDeck());
		this.setBusinessPermitTileMetods();
	}

	/**
	 * The method that puts the cities of the regionBoard in the array citiesInBoard
	 */
	private void initCities() {
		for (int i = 0; i < Constants.CITY_NUMBER/3; i++) {
			try {
				File mapFile = new File (Constants.INITIAL_MAP_PATH.concat(String.valueOf(gameBoard.getMapNumber())).concat(".json"));
				JSONParser parser = new JSONParser();
				JSONArray citiesArray = (JSONArray) parser.parse(new FileReader(mapFile));
				JSONObject city = (JSONObject) citiesArray.get((regionNumber * Constants.CITY_NUMBER/3) + i);
				String name = (String) city.get("Name");
				citiesInBoard[i] = gameBoard.getCities().get(name);
			} catch (IOException | ParseException e) {
				loggerRegionBoard.log(Level.INFO, e.getMessage(), e);;
			}
		}
	}

	/**
	 * The method used for getting one of the businessPermitTiles.
	 * @param index
	 * @return
	 */
	public PermitTile getTile(int index) {
		PermitTile permitTileUsed = businessPermitTiles[index];
		return permitTileUsed;
	}
	
	/**
	 * The method that change the businessPermitTile used.
	 * @param index
	 */
	public void changeTileUsed(int index){
		getBusinessPermitTiles()[index] = getPermitTileDeck().get(0);
		getPermitTileDeck().remove(0);
	}

	/**
	 * The method that creates the PermitTile deck.
	 */
	public void initPermitTileDeck(){	
		for (int i=0; i<15; i++) {
			PermitTile pt = new PermitTile(this);
			pt.setBuildingTiles(i);
			getPermitTileDeck().add(pt);
		}
	}

	/**
	 * The method that initially sets the two businessPermitTiles.
	 */
	public void setBusinessPermitTileMetods(){
		for(int i=0; i<2; i++){
			getBusinessPermitTiles()[i] = getPermitTileDeck().get(0);
			getPermitTileDeck().remove(0);	
		}
	}

	/**
	 * The method that changes the two businessPermitTiles and puts the old ones in the PermitTile deck.
	 */
	public void changeTile(){
		getPermitTileDeck().add(businessPermitTiles[0]);
		getPermitTileDeck().add(businessPermitTiles[1]);
		for(int i=0; i<2; i++){
			getBusinessPermitTiles()[i] = getPermitTileDeck().get(0);
			getPermitTileDeck().remove(0);
		}
	}

	/**
	 * The method that returns the number of the Cities in which the given Player has an Emporium.
	 * @param p
	 * @return
	 */
	private int getNumCityPlayer(Player p) {
		List<City> citiesPlayer = p.getCitiesOwned();
		int numCity=0;
		for(City cp: getCitiesInBoard())
			if(citiesPlayer.contains(cp))
				numCity++;

		return numCity;
	}

	/**
	 * The method that checks if a Player has an Emporim in all the Cities in the RegionBoard and, 
	 * if the condition is satisfied, gives him the regionBonus.
	 * @param p
	 */
	public void getBonusAllCities(Player p){
		if(getNumCityPlayer(p) == Constants.CITIES_PER_BOARD){
			regionBonus.giveBonus(p);
			gameBoard.giveKingTileBonus(p);
		}
	}


	public BoardType getType() {
		return type;
	}

	public Council getBalcony() {
		return balcony;
	}


	public City[] getCitiesInBoard() {
		return citiesInBoard;
	}


	public void setCitiesInBoard(City[] citiesInBoard) {
		this.citiesInBoard = citiesInBoard;
	}


	public PermitTile[] getBusinessPermitTiles() {
		return businessPermitTiles;
	}

	public void setBusinessPermitTiles(PermitTile[] businessPermitTiles) {
		this.businessPermitTiles = businessPermitTiles;
	}

	public int getRegionNumber(){
		return this.regionNumber;
	}

	public GameBoard getGameboard(){
		return this.gameBoard;
	}


	public List<PermitTile> getPermitTileDeck() {
		return permitTileDeck;
	}


	public void setPermitTileDeck(List<PermitTile> permitTileDeck) {
		this.permitTileDeck = permitTileDeck;
	}

}