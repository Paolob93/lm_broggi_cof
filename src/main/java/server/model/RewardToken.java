package server.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The RewardToken class.
 * 
 * The class that represents the token that is in a City. It has a Bonus[] that
 * the Player earns when he builds an Emporium in the City in which the RewardToken is.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class RewardToken {

	private Bonus[] bonus;
	private int bonusNumber;
	private int maxBonusNumberRT;
	private int maxValueBonusRT;
	private String rewardTokenDetails = new String("");
	private Logger loggerRewardToken = Logger.getLogger(RewardToken.class.getName());

	/**
	 * The constructor of the class that reads the maximum value of every bonus and 
	 * the maximum number of bonuses that can be find in the token.
	 * @param path
	 */
	public RewardToken(String path) {
		try {
			File settings = new File(path);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			maxValueBonusRT = (int)(long) settingsObj.get("maxValueBonusRT");
			maxBonusNumberRT = (int)(long) settingsObj.get("maxBonusNumberRT");
		}
		catch (IOException | ParseException e) {
			loggerRewardToken.log(Level.INFO, e.getMessage(), e);
		}
	}

	/**
	 * The method that initializes the Token by creating the bonusNumber and generating the bonuses.
	 */
	public void initRewardToken() {
		this.generateBonusNumber();
		this.generateBonus();
	}

	/**
	 * The method that initializes the void token that is present in the Violet City.
	 */
	public void  initVoidToken() {
		setBonusNumber(0);
		this.rewardTokenDetails = "\t\t\tThere are no bonus here";
		bonus = new Bonus[0];
	}

	/**
	 * The method that generates a casual bonus number.
	 */
	public void generateBonusNumber() {	
		this.setBonusNumber((int) (maxBonusNumberRT * Math.random()) + 1);
	}

	/**
	 * This method generates randomly a set of bonuses by calling the static method in the Bonus class and 
	 * sets the proper details in the rewardTokenDetails.
	 */
	public void generateBonus() {
		bonus = new Bonus[bonusNumber];
		boolean exit;
		for (int i=0; i< this.bonusNumber; i++) {
			do {
				exit = false;
				int bonusType = (int) (6 * Math.random());
				bonus[i] = Bonus.generateBonus(bonusType);
				bonus[i].setValueBonus((int) (maxValueBonusRT * Math.random()) + 1);
				for (int k = 0; k < i; k++) {
					if (bonus[i].getType().equals(bonus[k].getType())){
						exit = true;
					}
				}
			}
			while (exit);
			rewardTokenDetails = rewardTokenDetails + "\t\t\tBonus type: " + bonus[i].getType() + " -> value: " + bonus[i].getValueBonus() + "\n";
		}
	}

	public void setBonusNumber(int value) {
		this.bonusNumber = value;
	}

	public String getRewardTokenDetails() {
		return this.rewardTokenDetails;    	
	}

	public void giveBonus(Player p) {
		for (Bonus b: bonus) {
			b.giveBonus(p);
		}
	}

	public Bonus[] getBonus() {
		return this.bonus;
	}

	public void setBonus(Bonus[] b) {
		this.bonus = b;
	}

}