package server.model;

import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import common.interfaces.SettingsCreatorInterface;

/**
 * The SettingsCreator class. 
 * 
 * The class that creates a JSON file with all the settings required in the game
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class SettingsCreator extends UnicastRemoteObject implements SettingsCreatorInterface{
	
	
	private static final long serialVersionUID = 1L;
	private boolean start = false;
	private String path = "./src/main/java/server/model/MatchSettings.json";
	private int mapNumber = (int) (Constants.MAPS_NUMBER * Math.random()) + 1;
	private int settingsID;
	private int maxBonusNumberRT = 3;
	private int maxValueBonusRT = 5;
	private int maxBonusNumberPT = 3;
	private int maxValueBonusPT = 3;
	private int maxCitiesInPT = 3;
	private int maxBonusNumberNT = 3;
	private int maxValueBonusNT = 8;
	private int numCardsForColor = 13;
	private int numJollyCards = 12;
	private int numPlayers = 2;
	private static Logger logger = Logger.getLogger(SettingsCreator.class.getName());
	
	/**
	 * Settings creator constructor
	 * @throws RemoteException
	 */
	public SettingsCreator() throws RemoteException {
		//It is void because usually we add the values later.
	}

	/**
	 * The class that creates a JSON file with all the settings required in the game
	 */
	
	
	/**
	 * The method that creates the file in the positions specified from the Path.
	 */
	@SuppressWarnings("unchecked")
	public void createSettingsFile() {
		JSONObject settings = new JSONObject();
		settings.put("mapNumber", mapNumber);
		settings.put("maxBonusNumberRT", maxBonusNumberRT);
		settings.put("maxValueBonusRT", maxValueBonusRT);
		settings.put("maxBonusNumberPT", maxBonusNumberPT);
		settings.put("maxValueBonusPT", maxValueBonusPT);
		settings.put("maxCitiesInPT", maxCitiesInPT);
		settings.put("maxBonusNumberNT", maxBonusNumberNT);
		settings.put("maxValueBonusNT", maxValueBonusNT);
		settings.put("numCardsForColor", numCardsForColor);
		settings.put("numJollyCards", numJollyCards);
		settings.put("numPlayers", numPlayers);
		
		try (FileWriter fileWr = new FileWriter(path)) {
			fileWr.write(settings.toJSONString());
		} catch (IOException e) {
			logger.log(Level.INFO, e.getMessage(), e);
		}
	}
	
	@Override
	public void setMaxBonusNumberRT(int maxBonusNumberRT) {
		this.maxBonusNumberRT = maxBonusNumberRT;
	}
	@Override
	public void setMaxValueBonusRT(int maxValueBonusRT) {
		this.maxValueBonusRT = maxValueBonusRT;
	}
	@Override
	public void setMaxBonusNumberPT(int maxBonusNumberPT) {
		this.maxBonusNumberPT = maxBonusNumberPT;
	}
	@Override
	public void setMaxValueBonusPT(int maxValueBonusPT) {
		this.maxValueBonusPT = maxValueBonusPT;
	}
	@Override
	public void setMaxCitiesInPT(int maxCitiesInPT) {
		this.maxCitiesInPT = maxCitiesInPT;
	}
	@Override
	public void setMaxBonusNumberNT(int maxBonusNumberNT) {
		this.maxBonusNumberNT = maxBonusNumberNT;
	}
	@Override
	public void setMaxValueBonusNT(int maxValueBonusNT) {
		this.maxValueBonusNT = maxValueBonusNT;
	}

	private void setNumCardsForColor(int numCardsForColor) {
		this.numCardsForColor = numCardsForColor;
	}

	private void setNumJollyCards(int numJollyCards) {
		this.numJollyCards = numJollyCards;
	}

	/**
	 * The method that sets the number of the Players and 
	 * also sets the number of colored and jolly cards using the function described.
	 * @param numPlayers
	 */
	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
		if (numPlayers > 4) {
			setNumCardsForColor((numPlayers * 3) + 1);
			setNumJollyCards(numPlayers * 3);
		}
	}

	/**
	 * The method that sets the Path in which the file will be written from the given settingsID.
	 * @param settingsID
	 */
	public void setSettingsID(int settingsID) {
		this.settingsID = settingsID;
		this.path = "./src/main/java/server/model/Match" + this.settingsID + "Settings.json";
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPath() {
		return path;
	}

	public int getMapNumber() {
		return mapNumber;
	}

	public int getSettingsID() {
		return settingsID;
	}

	public int getMaxBonusNumberRT() {
		return maxBonusNumberRT;
	}

	public int getMaxValueBonusRT() {
		return maxValueBonusRT;
	}

	public int getMaxBonusNumberPT() {
		return maxBonusNumberPT;
	}

	public int getMaxValueBonusPT() {
		return maxValueBonusPT;
	}

	public int getMaxCitiesInPT() {
		return maxCitiesInPT;
	}

	public int getMaxBonusNumberNT() {
		return maxBonusNumberNT;
	}

	public int getMaxValueBonusNT() {
		return maxValueBonusNT;
	}

	public int getNumCardsForColor() {
		return numCardsForColor;
	}

	public int getNumJollyCards() {
		return numJollyCards;
	}

	public int getNumPlayers() {
		return numPlayers;
	}
	
	@Override
	public boolean getStart(){
		return start;
	}
	
	@Override
	public void setStart(boolean start){
		this.start = start;
	}
	
	
}
