package server.model;

/**
 * The TokenRewardBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class TokenRewardBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to TokenRewardBonus.
	 */
    public TokenRewardBonus() {
    	this.type = "TokenRewardBonus";
    }
    
    /**
     * The creator of the Bonus.
     * Sets the number of TokenRewardBonus that a Player will choose if he gets the Bonus.
     * @param num
     */
    public TokenRewardBonus(int value) {
    	this.valueBonus = value;
    	this.type = "TokenRewardBonus";
    }

    /**
     * The method that gives the bonus to the Player.
     */
    @Override
    public void giveBonus(Player p) {
       p.setRtBonus(valueBonus);
        }


}