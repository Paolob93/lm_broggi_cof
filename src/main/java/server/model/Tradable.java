package server.model;

/**
 * The Tradable interface.
 * 
 * The interface that every object that can be sold in the Market must implement.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
@FunctionalInterface
public interface Tradable {
	public void buy(Player player);
}
