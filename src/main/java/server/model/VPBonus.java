package server.model;

/**
 * The VPBonus Bonus.
 * 
 * @author PaoloBroggi
 * @author DavideDamonti
 */
public class VPBonus extends Bonus {

	/**
	 * The constructor of the class that sets the type to VPBonus.
	 */
    public VPBonus() {
    	this.type = "VPBonus";
    }

    /**
     * The creator of the Bonus.
     * Sets the number of VPBonus that a Player will earn if he gets the Bonus.
     * @param num
     */
    public VPBonus(int value) {
    	this.valueBonus = value;
    	this.type = "VPBonus";
    }

    /**
     * Adds to the Player the VPBonus that is the ValueBonus parameter.
     */
    @Override
    public void giveBonus(Player p) {
        p.addVictoryPoints(this.valueBonus);
    }


}