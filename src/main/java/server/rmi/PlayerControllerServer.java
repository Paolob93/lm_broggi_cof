package server.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import common.exception.BuildEmporiumException;
import common.exception.NoPermitTilesException;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import common.exception.WrongCardsException;
import common.exception.YouArePoorException;
import common.interfaces.PlayerControllerInterface;
import server.model.BackUpGameBoard;
import server.model.BoardType;
import server.model.City;
import server.model.GameBoard;
import server.model.ItemForSale;
import server.model.Match;
import server.model.PermitTile;
import server.model.Player;
import server.model.PoliticCard;
import server.model.PoliticColors;
import server.model.RegionBoard;


/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *
 *The player controller server class.
 *This class contains all the methods that a client (player controller) can call throught its interface
 */
public class PlayerControllerServer extends UnicastRemoteObject implements PlayerControllerInterface {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient Match match;
	private transient Player player;
	private transient Player playerBackup;
	private transient BackUpGameBoard gameBoardBackup;
	private transient List<PoliticCard> cards = new ArrayList<>();
	private transient GameBoard gameBoard;
	private transient List<ItemForSale> itemForSaleBackup;

	/**
	 * public constructor of the class
	 * @param player
	 * @param match
	 * @throws RemoteException
	 */
	public PlayerControllerServer(Player player, Match match) throws RemoteException{
		this.player = player;
		this.match = match;
		gameBoard = match.getGameBoard();
	
	}
	
	/**
	 * Add the money to the player
	 */
	@Override
	public void addMoney(int money){
		player.addMoney(money);
	}
	
	/**
	 * Draw a card and return its color string, and add the card to the hand of player
	 */
	@Override
	public String drawCard(){
		player.drawPoliticCard();
		
		int index = player.getPoliticCards().size();
		String returnString;
		if(!player.getPoliticCards().get(index-1).getJolly()){
			returnString = ""+player.getPoliticCards().get(index-1).getColor().toString();
		}
		else{
			returnString = "JOLLY";
		}
		return returnString;
		
	}
	/**
	 * Draw a card
	 */
	@Override
	public void drawCard(int num){
		player.drawPoliticCard(num);
	}
	/**
	 * This method elects a councillor of a color  in a council of a region
	 */
	@Override
	public void electCouncillor(String color, String region) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		RegionBoard regionBoard = match.getGameBoard().getRegionBoardType(BoardType.valueOf(region));
		player.electCouncillor(PoliticColors.valueOf(color),regionBoard);
	}
	/**
	 * This method elects a councillor of a color in King council
	 */
	@Override
	public void electKingCouncillor(String color) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		player.electKingCouncillor(PoliticColors.valueOf(color));
		
	}
	
	/**
	 * This method add a card of a coloro in the temporary cardHand
	 * @throws WrongCardsException 
	 */
	@Override
	public void addCard(String[] color) throws WrongCardsException{
		boolean stop;
		
		List<PoliticCard> cardsHand = new ArrayList<>();
		cardsHand.addAll(player.getPoliticCards());
		int dim2 = color.length;
		for(int j=0; j< color.length;j++){
			stop = true;
			for(int i=0; i<cardsHand.size() && stop; i++){
				if("JOLLY".equals(color[j])){
					if(cardsHand.get(i).getJolly()){
						cards.add(cardsHand.get(i));
						cardsHand.remove(i);
						stop=false;
					}
				}else{
					if(cardsHand.get(i).getStringColor().equals(color[j])){
						cards.add(cardsHand.get(i));
						cardsHand.remove(i);
						stop=false;
					}	
				}
			}
			
		}
		if(cards.size() != dim2)
			throw new WrongCardsException();
			
	}
	/**
	 * Clear the temporary hand of card
	 */
	@Override
	public void clearHand(){
		cards.clear();
	}
	
	/**
	 * This methods buy a tile from a region and in a position (right or left)
	 */
	@Override
	public void buyPermitTile(String region, int position) throws YouArePoorException, WrongCardsException{
		
		String regionUp = region.toUpperCase();
		RegionBoard regionBoard = match.getGameBoard().getRegionBoardType(BoardType.valueOf(regionUp));
		
		player.buyPermitTile(regionBoard, position, cards);
		
		cards.clear();
	}
	
	/**
	 * Return the tile and the datails of the player
	 */
	@Override
	public String[] getPlayerPermitTile(){
		List<PermitTile> tiles = player.getPermitTiles();
		int dim = tiles.size();
		String[] permitTiles = new String[dim];
		for(int i=0; i<dim; i++){
			permitTiles[i] = "Tile["+i+"]";
			for(int j=0; j<tiles.get(i).getBuildingTiles().length;j++)
				permitTiles[i]= permitTiles[i] + "\t" + tiles.get(i).getBuildingTiles()[j].getName();
		}
			
		return permitTiles;
	}
	/**
	 * This method biuld an emporium in a city from a tile
	 */
	@Override
	public void buildEmporium(String cityName, int index) throws BuildEmporiumException, NotAvailableAssistantsException{
		
		City city = gameBoard.getCities().get(cityName);
		PermitTile tile = player.getPermitTiles().get(index);
		player.buildEmporium(city, tile);
	}
	
	/**
	 * This method biuld the emporium with the help oh the kin
	 */
	@Override
	public void buildEmporiumKing(String cityName) throws YouArePoorException, WrongCardsException, NotAvailableAssistantsException, BuildEmporiumException{
		
		City city = gameBoard.getCities().get(cityName);
		player.buildEmporiumByKing(city, cards);
		cards.clear();
		
	}
	
	/**
	 * This method ingage an assistant
	 */
	@Override
	public void ingageAssistant() throws NotAvailableAssistantsException, YouArePoorException{
		player.buyAssistant();
	}
	
	/**
	 * This method allow the player to change the tiles in a region
	 */
	@Override
	public void changeTile(String region) throws NotAvailableAssistantsException{
		player.changeTiles(gameBoard.getRegionBoardType(BoardType.valueOf(region)));
	}
	
	
	/**
	 * This method allow the player to elect a councillor whit an assistant
	 */
	@Override
	public void electCounselorAssistant(String color, String region) throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException{
		if("KING".equals(region)){
			player.electKingCouncillorByAssistant(PoliticColors.valueOf(color));
		}else
			player.electCouncillorByAssistant(PoliticColors.valueOf(color), gameBoard.getRegionBoardType(BoardType.valueOf(region)));
		
	}
	@Override
	public boolean getQuickActionDone(){
		return player.getQuickActionDone();
	}
	@Override
	public boolean getMainActionDone(){
		return player.getMainActionDone();
	}
	@Override
	public void setQuickActionDone(boolean done){
		player.setQuickActionDone(done);
	}
	@Override
	public void setMainActionDone(boolean done){
		player.setMainActionDone(done);
	}
	
	/**
	 * This method return all the color card of the hand player
	 */
	@Override
	public String[] getColorCard(){
		int dim = player.getPoliticCards().size();
		String[] color = new String[dim];
		for(int i=0; i<dim; i++){
			if(player.getPoliticCards().get(i).getJolly())
				color[i]="JOLLY";
			else
				color[i] = player.getPoliticCards().get(i).getStringColor();
		}
		return color;
	}
	@Override
	public int getNumAssistant(){
		return player.getAssistants().size();
	}
	
	/**
	 * This method allow the  player to sell a politic card of a color with a price
	 */
	@Override
	public void sellPoliticCard(String color, int price){	
		PoliticCard card;
		boolean exit = false;
		for(int i=0; i<player.getPoliticCards().size() && !exit;i++){
			card=player.getPoliticCards().get(i);
			if("JOLLY".equals(color)){
				if(card.getJolly()){
					player.sell(card, price);
					exit = true;;
				} 
			}
			else{
				if(PoliticColors.valueOf(color).equals(card.getColor())){
					player.sell(card, price);
					exit = true;
				}
			}
		}
	}
	@Override
	public void addAssistant(int num) throws NotAvailableAssistantsException{
		player.addAssistant(num);
	}
	@Override
	public void sellPermitTile(int index, int price){
		player.sell(player.getPermitTiles().get(index),price);
	}
	@Override
	public void sellAssistant(int price) throws NotAvailableAssistantsException{
		player.sellAssistant(price);
	}
	
	@Override
	public void buyItem(int index) throws YouArePoorException{
		player.buyItem(match.getMarket().getAvailableItems().get(index));
	}
	
	/**
	 * The method that is used in order to create a backup of the player and of the gameBoard.
	 */
	@Override
	public void backup() {
		playerBackup = new Player(player);
		gameBoardBackup = new BackUpGameBoard(gameBoard);
		itemForSaleBackup = new ArrayList<>();
		itemForSaleBackup.addAll(match.getMarket().getAvailableItems());
	}
	
	/**
	 * The method that restores a player and a gameBoard if an exception happens.
	 */
	@Override
	public void restore(){
		player.restorePlayer(playerBackup);
		gameBoard.restoreGameBoard(gameBoardBackup);
		match.getMarket().getAvailableItems().clear();
		match.getMarket().getAvailableItems().addAll(itemForSaleBackup);
	}
	
	
	
	@Override
	public boolean checkFaceUpBonus(){
		return player.isFuBonus();
	}
	@Override
	public String showFUTiles(){
		return player.showFUTiles();
	}
	
	/**
	 * This method allow the player to choose and get a tile from the FaceUpBonus
	 */
	@Override
	public void chooseFaceUpPT(int index){
		player.chooseFaceUpPT(index);
		player.setFuBonus(false);
	}
	
	
	@Override
	public boolean checkFaceDownBonus(){
		return player.isFdBonus();
	}
	@Override
	public String showFDTiles() throws NoPermitTilesException{
		return player.showFDTiles();
	}
	
	/**
	 * This method allow the player to get the bonus from a FaceDownBonus
	 */
	@Override
	public void chooseDownTile(int index) throws NoPermitTilesException{
		player.chooseTile(index).giveBonus(player);
		player.setFdBonus(false);
		
	}
	
	@Override
	public boolean checkTokenReward(){
		if(player.getRtBonus()>0)
			return true;
		return false;
	}
	@Override
	public String  showRewardTokens(){
		return player.showPlayerRewardTokens();
	}
	
	/**
	 * This method allow the player to choose and obtain the bonus in a token thanks the Reward Token bonus
	 */
	@Override
	public void chooseRewardTokenBonus(int index){
		player.chooseTokenReward(index).giveBonus(player);
		int num = player.getRtBonus();
		player.setRtBonus(num-1);
	}
	
	@Override
	public int getRtBonus(){
		return player.getRtBonus();
	}
	
	@Override
	public int getNumberFdTile(){
		return player.getNumberOkFDTiles();
	}
	@Override
	public int[] geNumberRT(){
		return player.getNumberOkRT();
	}

}
