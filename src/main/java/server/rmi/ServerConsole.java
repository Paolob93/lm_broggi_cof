package server.rmi;
/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 */
public class ServerConsole {
	/**
	 * Private contrusctor to hide the implicit public one
	 */
	
	private ServerConsole(){}
	/**
	 * This static method print on the console a string message, used in the server side
	 * @param message
	 */
	public static void printLine(String message){
		System.out.println(message);
	}

}
