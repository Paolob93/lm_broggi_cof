package server.rmi;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import client.rmi.MainAction;
import common.interfaces.ConsoleUpdateInterface;
import common.interfaces.ServerControllerInterface;
import server.model.Constants;
import server.model.Market;
import server.model.Match;
import server.model.Player;
import server.model.PlayerList;


/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *
 *The ServerController class.
 *This class contain the method for manage the turns of main quick action , sell market and buy market action.
 *Check every turn if a player has build 10 emporiums.
 *
 */
public class ServerController extends UnicastRemoteObject implements ServerControllerInterface {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5039379580867942661L;
	private transient Timer timer;
	private int lobbyID;
	private boolean[] statusPlayer; 
	private boolean endTurnPlayer;
	private boolean end = false;
	private boolean start = false;
	private boolean startMarketSell = false;
	private boolean startMarketBuy = false;
	private transient PlayerList players;
	private int numPlayers;
	private boolean endGame = false;
	private int turnID = 1;
	private int turnIDMarket;
	private transient Match match;
	private transient Market market;
	private transient String settingsPath;
	private transient Logger loggerServerController = Logger.getLogger(ServerController.class.getName());
	private transient FileHandler fh;
	
	
	/**
	 * The constructor of the class , create a file logger
	 * @param players
	 * @param match
	 * @param lobbyID
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public ServerController(PlayerList players, Match match, int lobbyID) throws RemoteException, InterruptedException{
		this.players = players;	
		this.match = match;
		market = match.getMarket();
		this.lobbyID=lobbyID;
		
		settingsPath = "./src/main/java/server/model/Match" + lobbyID + "Settings.json";
		
		try {
			fh = new FileHandler(MainAction.class.getName() + ".txt", true);
			fh.setFormatter(new SimpleFormatter());
			loggerServerController.addHandler(fh);
			loggerServerController.setUseParentHandlers(false);
		} catch (IOException e) {
			loggerServerController.log(Level.SEVERE, e.getMessage(), e);;
		}
	}
	
	
	/**
	 * This method check if all the player that have to play are all connected
	 */
	@Override
	public void checkPlayers() throws InterruptedException{
		
		Thread.sleep(500);
		boolean canstart= false;
		try {
			File settings = new File(settingsPath);
			JSONParser parser = new JSONParser();
			JSONObject settingsObj = (JSONObject) parser.parse(new FileReader(settings));
			numPlayers = (int)(long) settingsObj.get("numPlayers");
		}
		catch (IOException | ParseException e) {
			loggerServerController.log(Level.INFO, e.getMessage(), e);
		}
		while(!canstart){
			if(players.getNumPlayer() == numPlayers){
				
				canstart = true;	
			}
			Thread.sleep(500);
		}
		setStart(true);
	}
	public void initStatusPlayer(){
		for(int i=0; i<players.getNumPlayer();i++)
			statusPlayer[i]=true;
	}
	
	
	/**
	 * This method menage the alternate turn of the player, when a player build 10 emporiums he block the market phase
	 * and it does make the last turn, and then create the final ranking
	 */
	@Override
	public void setTurn() throws InterruptedException{
		
		statusPlayer = new boolean[players.getNumPlayer()];
		initStatusPlayer();
		for(int i=1; i<=players.getNumPlayer(); i++){
			ServerConsole.printLine("Turn Player "+i);
			turnID = i;
			setEndTurnPlayer(false);
			Thread.sleep(1000);
			if(statusPlayer[i-1]){
				startTimer(i);
				while(!endTurnPlayer){
					Thread.sleep(1000);	
				}
				stopTimer();
			}
			for(Player player: players.getPlayers()){
				if(player.getNumEmporiums()==Constants.EMPORIUMS_FOR_VICTORY && !end){
					player.addVictoryPoints(3);
					ServerConsole.printLine("The Player "+player.getName()+" built 10 emporiums");
					end = true;
				}
			}
			
			startMarketBuy = false;
			startMarketSell = true;
			
			if(i==players.getNumPlayer() && !end){
				sellTurnMarket();
				buyTurnMarket();
				ServerConsole.printLine("Market Ended");
				i=0;
			}
			if(!checkAllPlayerConnection()){
				ServerConsole.printLine("Match over, nobody player is connected");
				break;
			}
		}
		
		endGame = true;
		Thread.sleep(500);
		giveLastVP();
			
		
	}
	
	/**
	 * This method check if all player are connected, if nobody is connected the turn it is blocked
	 * 
	 * @return
	 */
	public boolean checkAllPlayerConnection(){
		for(int i=0;i<statusPlayer.length;i++)
			if(statusPlayer[i]==true)
				return true;
		return false;
	}
	
	
	/**
	 * Give the last victory points to the players
	 */
	public void giveLastVP(){
		match.assignPointNobilityTrack();
		match.permitTileTopPlayer();
	}
	
	/**
	 * This method stipulates the final ranking and decrees the winner
	 */
	@Override
	public String getWinner(){
		Player winner = match.findWinner();
		List<Player> playersFinal = new ArrayList<>();
		playersFinal.addAll(players.getPlayers());
		
		
		Collections.sort(playersFinal,new Comparator<Player>(){
			@Override
			public int compare (Player p1, Player p2){
				return p2.getVictoryPoints() - p1.getVictoryPoints();
			}
		});
		
		String ranking;
		int i=1;
		ranking= "AND THE WINNER IS:"+winner.getName()+" with "+winner.getVictoryPoints()+" Victory Points\n";
		for(Player p: playersFinal){
			ranking = ranking +"Position "+i+": "+p.getName()+" - Victory Points: "+p.getVictoryPoints()+"\n";
			i++;
		}
		return ranking;
	}
	
	/**
	 * This is the method that manage the sell turns market
	 * @throws InterruptedException
	 */
	public void sellTurnMarket() throws InterruptedException{
		
		turnIDMarket = 10000000;
		startMarketSell = true;
		for(int j=1; j<=players.getNumPlayer();j++){
			turnIDMarket = j;
			ServerConsole.printLine("Turn Player sell Market "+j);
			if(statusPlayer[j-1]){
				startTimer(j);
				while(startMarketSell){
					Thread.sleep(1000);	
				}
				stopTimer();
			}
			startMarketSell = true;
			turnID= 1111111;
		}
	}
	/**
	 * This is the method that manage the but turns market
	 * @throws InterruptedException
	 */
	public void buyTurnMarket() throws InterruptedException{
		
		startMarketBuy = true;
		int[] order = market.generateRandomOrder();
		turnIDMarket = 10000000;
		for(int j=0; j<order.length; j++){
			turnIDMarket = order[j]+1;
			ServerConsole.printLine("Turn Player buy Market "+turnIDMarket);
			if(statusPlayer[order[j]]){
				startTimer(turnIDMarket);
				while(startMarketBuy){
					Thread.sleep(1000);	
				}
				stopTimer();
			}
			startMarketBuy = true;
		}
		
		turnIDMarket=1000000;
	}
	     
	/**
	 * This method starts the timer of the player and the timer that decrete if a player is disconnected
	 * @param index
	 */
	public void startTimer(int index){
		timer = new Timer();
		TimerTask task = new TimerTask(){
			int cont=Constants.TIME_TURN;
			@Override
			public void run(){
				cont--;
				if(cont==0){
					if(!endTurnPlayer){
						endTurnPlayer=true;
						statusPlayer[index-1]=false;
						notifycation(index);
					}
					timer.cancel();
				}
			}
		};

		timer.scheduleAtFixedRate(task, 0, 1000);
	}


	/**
	 * This method notity all the players of a disconnection of a player
	 * @param index
	 */
	public void notifycation(int index){
		Registry registry;
		ConsoleUpdateInterface consoleUpdate;

		try {
			registry = LocateRegistry.getRegistry(1099);
			for(int i=1;i<=players.getNumPlayer(); i++){
				consoleUpdate=(ConsoleUpdateInterface) registry.lookup("consoleUpdate"+lobbyID+i);
				if(i!=index){
					
					consoleUpdate.playerDisconnectedNotifycation(index);
				}else
					consoleUpdate.kill();	
			}

		} catch (RemoteException | NotBoundException e) {
			loggerServerController.log(Level.WARNING, e.getMessage(), e);
			Registry registry1;
			try {
				registry1 = LocateRegistry.getRegistry(1099);
				for(int i=1;i<=players.getNumPlayer(); i++){
					consoleUpdate=(ConsoleUpdateInterface) registry1.lookup("consoleUpdate"+lobbyID+i);
					if(i!=index)
						consoleUpdate.playerDisconnectedNotifycation(index);

				}
			} catch (RemoteException | NotBoundException e1) {
				loggerServerController.log(Level.WARNING, e1.getMessage(), e1);
			}

		}
	}
	
	
	@Override
	public int getTurnIDMarket(){
		return turnIDMarket;
	}
	@Override	
	public void setStartMarketSell(boolean start){
		startMarketSell = start;
	}
	@Override
	public boolean getStartMarketSell(){
		return startMarketSell;
	}
	@Override
	public boolean isStart(){
		return start;
	}
	@Override
	public void setStart(boolean start){
		this.start  = start;
	}
	@Override
	public int playerTurn(){
		return turnID;
	}
	@Override
	public void setEndTurnPlayer(boolean endTurn ){
		this.endTurnPlayer = endTurn;
	}
	@Override
	public boolean getEndPlayer(){
		return endTurnPlayer;
	}
	@Override
	public boolean getStartMarketBuy(){
		return startMarketBuy;
	}
	@Override
	public void setStartMarketBuy(boolean start){
		startMarketBuy = start;
	}
	@Override
	public boolean isEnd(){
		return end;
	}
	@Override
	public boolean isEndGame(){
		return endGame;
	}
	
	public void stopTimer(){
		timer.cancel();
	}
	
	
}
