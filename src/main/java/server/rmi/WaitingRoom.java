package server.rmi;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import common.exception.CannotLinkCityException;
import common.interfaces.LobbyInterface;
import common.interfaces.WaitingRoomInterface;
import server.model.Lobby;


/**
 * 
 * @author DavideDamonti
 * @author PaoloBroggi
 *
 *The Waiting Room class
 *This class receive all the rmiclient connection , create lobby match class
 */
public class WaitingRoom extends UnicastRemoteObject implements WaitingRoomInterface{


	private static final long serialVersionUID = 7977620141868727494L;
	private int lobbyID = 0 ;
	private int activeNumPlayer = 0;
	private int matchID = 0;
	private List<Lobby> lobbies = new ArrayList<>();
	private boolean canStart;
	private List<String> connectedPlayers;
	private boolean canContinue ;
	
	/**
	 * Constructor of the class, create a the array list of all player connected
	 * @throws RemoteException
	 */
	public WaitingRoom() throws RemoteException{
		activeNumPlayer = 0;
		canStart = false;
		connectedPlayers = new ArrayList<>();
	}
	
	/**
	 * Synchronized method used to increment the number of player that are waiting a free lobby to enter
	 */
	@Override
	public synchronized void incrementPlayer(){
		activeNumPlayer++;
	}
	
	/**
	 * This method is used from player 1 to create a new lobby, from the player 2 to wait 20 seconds and tha t the player 1 has insert all the parameters
	 * and than create a match
	 */
	@Override
	public void setLobby( int id) throws RemoteException, AlreadyBoundException, InterruptedException, CannotLinkCityException {
		
		if(id==1){
			canStart = false;
			setNewLobby();
			canContinue = false;
			
			
		}
		
		if(id==2){
			
			canStart = false;
			Thread.sleep(20000);
			canStart = true;
			
			while(!canContinue){
				Thread.sleep(500);
			}
			
			
			lobbies.get(0).getSettings().setNumPlayers(activeNumPlayer);
			lobbies.get(0).getSettings().createSettingsFile();
			lobbies.get(0).getMatch().initMatch();
			lobbies.remove(0);
			resetRoom();
		}
		
		
		

	}
	@Override
	public void setCanContinue(boolean canContinue){
		this.canContinue = canContinue;
	}
	
	@Override
	public int getActiveNumPlayer(){
		return activeNumPlayer;
	}
	@Override
	public void resetRoom(){
		activeNumPlayer = 0;
		
	}
	
	
	
	@Override
	public int getLobbyID(){
		return lobbyID;
	}
	
	/**
	 * This method crate a new lobby and it does the bind of the lobby in the regisrty
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws CannotLinkCityException
	 * @throws InterruptedException
	 */
	public void setNewLobby() throws RemoteException, AlreadyBoundException, CannotLinkCityException, InterruptedException{
		lobbyID++;
		Lobby gameLobby = new Lobby(lobbyID);
		lobbies.add(gameLobby);
		
		
		Registry registry = LocateRegistry.getRegistry(1099);
	
		registry.bind("gameLobby "+lobbyID,gameLobby);
		
		ServerController serverController = gameLobby.getServerController();
	
		registry.bind("serverController"+lobbyID, serverController);
		
		matchID++;
		
	}
	
	
	@Override
	public boolean getCanStart(){
		return canStart;
	}
	@Override
	public int getMatchID(){
		return matchID;
	}
	
	
	/**
	 * Synchronized method for the creation of the player, it check if the username of the player is yet used and than create the player controller
	 */
	@Override
	public synchronized void createPlayerControllerServer(String username, int myID, int lobbyID) throws RemoteException, AlreadyBoundException, NotBoundException{
		
		if(!connectedPlayers.contains(username)){
			
			connectedPlayers.add(username);
			
			Registry registry = LocateRegistry.getRegistry(1099);
			LobbyInterface gameLobbyInt = (LobbyInterface) registry.lookup("gameLobby "+lobbyID);
			
			gameLobbyInt.createPlayerController(username, myID);
			
		}
		else throw new AlreadyBoundException();
	}
	
	
	
	
}
