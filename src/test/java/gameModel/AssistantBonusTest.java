package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import common.exception.AssistantRuntimeException;
import server.model.*;

public class AssistantBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	AssistantBonus ab;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	
	@Test
	public void testAssistantBonus() {
		ab = new AssistantBonus();
		assertTrue(ab instanceof AssistantBonus);
		assertEquals(ab.getType(), "AssistantBonus");
	}

	@Test
	public void testAssistantBonusInt() {
		ab = new AssistantBonus(5);
		assertEquals(ab.getValueBonus(), 5);
		assertEquals(ab.getType(), "AssistantBonus");
	}


	@Test
	public void testGiveBonus() {
		ab = new AssistantBonus(5);
		ab.giveBonus(pippo);
		assertEquals(pippo.getAssistants().size(), 5);
		ab = new AssistantBonus(3);
		ab.giveBonus(pippo);
		assertEquals(pippo.getAssistants().size(), 8);
		ab = new AssistantBonus(8);
		ab.giveBonus(pippo);
		assertEquals(pippo.getAssistants().size(), 16);
	}
	
	@Test(expected = AssistantRuntimeException.class)
	public void testGiveBonusNoAssistants() {
		ab = new AssistantBonus(60);
		ab.giveBonus(pippo);
		ab = new AssistantBonus(40);
	}

}
