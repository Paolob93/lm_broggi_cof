package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import server.model.*;

class TestingBonus extends Bonus {
	TestingBonus(int i){
		super(i);
	}
	TestingBonus(){
		super();
	}
	public void setValueBonus(int v){
		super.setValueBonus(v);
	}
	public void giveBonus(Player p){}
}

public class BonusTest {
	
	Bonus a, b, c, d, e, f, g, h, i, j;
	
	@Before
	public void setUp() {
		g = Bonus.generateBonus(0);
	}

	@Test
	public void testGenerateBonus() {
		a = Bonus.generateBonus(0);
		assertTrue(a instanceof AssistantBonus);
		b = Bonus.generateBonus(1);
		assertTrue(b instanceof MainActionBonus);
		c = Bonus.generateBonus(2);
		assertTrue(c instanceof MoneyBonus);
		d = Bonus.generateBonus(3);
		assertTrue(d instanceof NobilityTrackBonus);
		e = Bonus.generateBonus(4);
		assertTrue(e instanceof PoliticCardBonus);
		f = Bonus.generateBonus(5);
		assertTrue(f instanceof VPBonus);
		h = Bonus.generateBonus(6);
		assertTrue(h instanceof FaceDownPermitTileBonus);
		i = Bonus.generateBonus(7);
		assertTrue(i instanceof FaceUpPermitTileBonus);
		j = Bonus.generateBonus(9245);
		assertTrue(j instanceof TokenRewardBonus);
	}

	
	@Test
	public void testBonus() {
		Bonus bon = new TestingBonus(5);
		assertTrue(bon.getValueBonus() == 5);
	}
	
	@Test
	public void testGetType(){
		assertTrue(g.getType().equals("AssistantBonus"));
	}
	
	@Test
	public void testSetValueBonus () {
		Bonus asd = new TestingBonus();
		asd.setValueBonus(4);
		assertTrue(asd.getValueBonus() == 4);
	}
}
