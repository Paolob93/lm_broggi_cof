package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import common.exception.AssistantRuntimeException;
import common.exception.CannotLinkCityException;
import server.model.*;

public class CityTest {
	PlayerList pl;
	Player testPlayer;
	Player testPlayer2;
	Match match;
	GameBoard gameBoard;
	String path;
	

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		testPlayer = new Player("Pippo", match);
		testPlayer2 = new Player("Pippo2", match);
		gameBoard = match.getGameBoard();
		//gameBoard.setMapNumber(1);
		path = gameBoard.getSettingsPath();
	}

	@Test
	public void testCity() {
		City city = new City("Prova", CityColors.BLUE, path);
		assertTrue(city instanceof City);
	}

	@Test
	public void testGetName() {
		City city = new City("Prova", CityColors.BLUE, path);
		assertEquals(city.getName(), "Prova");
	}

	@Test
	public void testSetName() {
		City city = new City("Cosa", CityColors.BLUE, path);
		city.setName("Prova");
		assertEquals(city.getName(), "Prova");
	}

	@Test
	public void testGetColor() {
		City city = new City("Cosa", CityColors.BLUE, path);
		assertEquals(city.getColor(), CityColors.BLUE);
	}

	@Test
	public void testSetColor() {
		City city = new City("Prova", CityColors.GOLD, path);
		city.setColor(CityColors.BLUE);
		assertEquals(city.getColor(), CityColors.BLUE);
	}

	@Test
	public void testIsPresentEmporium() {
		City city = new City("Prova", CityColors.GOLD, path);
		city.getEmporiums().add(testPlayer);
		assertTrue(city.isPresentEmporium(testPlayer));
	}

	@Test(expected = CannotLinkCityException.class)
	public void testCreateLinkException() throws CannotLinkCityException {
		City city = new City("Prova", CityColors.GOLD, path);
		city.createLink(city);
	}
	
	@Test
	public void testCreateLink() throws CannotLinkCityException {
		City city = new City("Prova", CityColors.GOLD, path);
		City city2 = new City("Cosa", CityColors.BLUE, path);
		city.createLink(city2);
		assertTrue(city.getNearCities().contains(city2));
	}
	

	@Test
	public void testGetEmporiums() {
		City city = new City("Prova", CityColors.GOLD, path);
		city.getEmporiums().add(testPlayer);
		city.getEmporiums().add(testPlayer2);
		assertTrue(city.getEmporiums().contains(testPlayer));
		assertTrue(city.getEmporiums().contains(testPlayer2));
	}

	@Test
	public void testGetRewardToken() {
		try {
			City city = new City("Prova", CityColors.BLUE, path);
			city.getRewardToken().giveBonus(testPlayer);
			assertTrue(testPlayer.getAssistants().size() > 0 || testPlayer.getMoney() > 0 || testPlayer.getPoliticCards().size() > 0 || testPlayer.getNobilityTrackPosition() > 0
					|| testPlayer.getVictoryPoints() > 0 || testPlayer.getBonusAction());
		}
		catch (AssistantRuntimeException e) {
			assertEquals(e.getClass(), AssistantRuntimeException.class);
		}
	}


	@Test
	public void testGetNearCities() throws CannotLinkCityException {
		City city = new City("Prova", CityColors.GOLD, path);
		City city2 = new City("Cosa", CityColors.BLUE, path);
		City city3 = new City("Boh", CityColors.RED, path);
		city.createLink(city2);
		city.createLink(city3);
		assertTrue(city.getNearCities().contains(city2));
		assertTrue(city.getNearCities().contains(city3));
	}

	@Test
	public void testNearCitiesPlayer() {
		City arkon = gameBoard.getCities().get("Arkon");
		City castrum = gameBoard.getCities().get("Castrum");
		City burgen = gameBoard.getCities().get("Burgen");
		City dorful = gameBoard.getCities().get("Dorful");
		City juvelar = gameBoard.getCities().get("Juvelar");
		City graden = gameBoard.getCities().get("Graden");
		arkon.getEmporiums().add(testPlayer);
		castrum.getEmporiums().add(testPlayer);
		burgen.getEmporiums().add(testPlayer);
		dorful.getEmporiums().add(testPlayer);
		juvelar.getEmporiums().add(testPlayer);
		assertTrue(arkon.nearCitiesPlayer(testPlayer).contains(dorful));
		assertTrue(arkon.nearCitiesPlayer(testPlayer).contains(castrum));
		assertTrue(arkon.nearCitiesPlayer(testPlayer).contains(burgen));
		assertFalse(arkon.nearCitiesPlayer(testPlayer).contains(juvelar));
		graden.getEmporiums().add(testPlayer);
		assertTrue(arkon.nearCitiesPlayer(testPlayer).contains(juvelar));
	}

}
