package gameModel;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import server.model.*;

public class CouncilTest {
	PlayerList pl;
	Match match;
	GameBoard gameBoard;
	Council coun;
	

	
	@Before public void setUp() throws Exception  {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		gameBoard = match.getGameBoard();
		//gameBoard.setMapNumber(1);
		coun = new Council(gameBoard);
	}


	@Test
	public void testAddCouncillor() {
		Councillor secondCouncillor = coun.getCouncil().get(0);
		Councillor firstCouncillor = new Councillor();
		coun.addCouncillor(firstCouncillor);
	    //firstCouncillor.setElect(true);
	    assertEquals(coun.getCouncil().get(0), firstCouncillor);
	    assertEquals(coun.getCouncil().get(1), secondCouncillor);
    }
	
	
	@Test
	public void testRemoveCouncillor() {
		Councillor c3 = coun.getCouncil().get(3);
		coun.removeLastCouncillor();
		assertFalse(coun.getCouncil().contains(c3));
	}

	
	@Test
	public void testInitCouncil1() {
		assertTrue(coun.getCouncil().size() == 4);
	}
	
	@Test
	public void testInitCouncil2() {
		Councillor c1 = this.gameBoard.getSetCouncillors().get(0);
		Councillor c2= this.gameBoard.getSetCouncillors().get(1);
		Councillor c3 = this.gameBoard.getSetCouncillors().get(2);
		Councillor c4 = this.gameBoard.getSetCouncillors().get(3);
		coun.initCouncil();
		assertTrue(coun.getCouncil().contains(c4));
		assertTrue(coun.getCouncil().contains(c3));
		assertTrue(coun.getCouncil().contains(c2));
		assertTrue(coun.getCouncil().contains(c1));
	}

	
	@Test
	public void testSatisfyCouncil() throws RemoteException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		extendedCouncillors.add(0, new Councillor(PoliticColors.ORANGE));
		extendedCouncillors.add(0, new Councillor(PoliticColors.WHITE));
		extendedCouncillors.add(0, new Councillor(PoliticColors.BLUE));
		gameBoard.setSetConucillors(extendedCouncillors);
		coun.initCouncil();
		ArrayList<PoliticCard> cards = new ArrayList<>();
		cards.add(new PoliticCard(PoliticColors.ORANGE));
		cards.add(new PoliticCard(PoliticColors.WHITE));
		cards.add(new PoliticCard(PoliticColors.BLUE));
		boolean control = coun.satisfyCouncil(cards);
    	assertTrue(control);
	}
	
	@Test
	public void testSatisfyCouncilVoid() {
		ArrayList<PoliticCard> testVoid = new ArrayList<PoliticCard>();
		assertFalse(coun.satisfyCouncil(testVoid));
	}
	
	@Test
	public void testElect() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		coun.elect(PoliticColors.BLUE);
		assertTrue(coun.getCouncil().get(0).getColorCouncillor().equals(PoliticColors.BLUE));
		assertEquals("BLUE", coun.getCouncil().get(0).getStringColor());

	}
	
	@Test(expected = NotAvailableColorCouncillorsException.class)
	public void testElectExcColor() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24;){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
			i = i + 6;
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		coun.elect(PoliticColors.BLUE);
	}
	
	@Test(expected = NotAvailableCouncillorsException.class)
	public void testElectExcCounc() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		gameBoard.setSetConucillors(extendedCouncillors);
		coun.elect(PoliticColors.BLUE);
	}
	
	

}
