package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DeckPoliticCardsTest {
	PlayerList pl;
	Match match;
	Player pippo;
	GameBoard gameBoard;
	DeckPoliticCards dpc;
	String path;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		gameBoard = match.getGameBoard();
		//gameBoard.setMapNumber(1);
		path = gameBoard.getSettingsPath();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(path);
		}
		NobilityTrack nt = new NobilityTrack(path);
		nt.setNobilityTrackArray(ntt);
		gameBoard.getKingBoard().setNobilityTrack(nt);
	//	gameBoard.setMapNumber(1);
		pippo = new Player("Pippo", match);	
	}

	@Test
	public void testDeckPoliticCards() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		assertTrue(dpc instanceof DeckPoliticCards);
	}


	@Test
	public void testInitDeck() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		dpc.initDeck();
		assertEquals(dpc.getDeck()[3].getColor(), PoliticColors.BLUE);
		assertEquals(dpc.getDeck()[9].getColor(), PoliticColors.BLUE);
		assertEquals(dpc.getDeck()[16].getColor(), PoliticColors.PINK);
		assertEquals(dpc.getDeck()[85].getJolly(), true);
	}

	
	@Test
	public void testGetPoliticCard() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		PoliticCard pc0 = dpc.getDeck()[0];
		assertEquals(pc0, dpc.getPoliticCard());
	}

	
	@Test
	public void testReInitDeck() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		PoliticCard pc0 = dpc.getPoliticCard();
		PoliticCard pc1 = dpc.getPoliticCard();
		PoliticCard pc2 = dpc.getPoliticCard();
		PoliticCard pc3 = dpc.getPoliticCard();
		dpc.addRejectedCard(pc0);
		dpc.addRejectedCard(pc1);
		dpc.addRejectedCard(pc2);
		dpc.addRejectedCard(pc3);
		dpc.reInitDeck();
		assertTrue(pc0.getInDeck());
		assertTrue(pc1.getInDeck());
		assertTrue(pc2.getInDeck());
		assertTrue(pc3.getInDeck());
	}
	
	@Test
	public void testReInitDeck1() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		for (int i = 0; i<50; i++) {
			PoliticCard pc = dpc.getPoliticCard();
			dpc.addRejectedCard(pc);
		}
		for (int i = 0; i<50; i++) {
			PoliticCard pc = dpc.getPoliticCard();
			dpc.addRejectedCard(pc);
		}
		int used = 0;
		for (PoliticCard poca: dpc.getDeck()) {
			if (!(poca.getInDeck())) {
				used++;
			}
		}
		assertEquals(10, used);
	}

	@Test
	public void testAddRejectedCard() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		PoliticCard pc0 = dpc.getPoliticCard();
		PoliticCard pc1 = dpc.getPoliticCard();
		PoliticCard pc2 = dpc.getPoliticCard();
		PoliticCard pc3 = dpc.getPoliticCard();
		assertFalse(dpc.getDeck()[0].getInDeck());
		dpc.addRejectedCard(pc0);
		dpc.addRejectedCard(pc1);
		dpc.addRejectedCard(pc2);
		dpc.addRejectedCard(pc3);
		assertTrue(dpc.getRejectedCard().contains(pc0));
		assertTrue(dpc.getRejectedCard().contains(pc1));
		assertTrue(dpc.getRejectedCard().contains(pc2));
		assertTrue(dpc.getRejectedCard().contains(pc3));
	}
	
	@Test
	public void testsetRejectedCard() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		assertEquals(0, dpc.getRejectedCard().size());
		List<PoliticCard> rej = new ArrayList<>();
		rej.add(new PoliticCard(true));
		rej.add(new PoliticCard(true));
		rej.add(new PoliticCard(true));
		rej.add(new PoliticCard(true));
		rej.add(new PoliticCard(true));
		dpc.setRejectedCard(rej);
		assertEquals(5, dpc.getRejectedCard().size());
	}
	
	@Test
	public void testSetDeck() throws RemoteException {
		dpc = new DeckPoliticCards(path);
		PoliticCard[] testDeck = new PoliticCard[15];
		for (int i = 0; i< testDeck.length; i++) {
			testDeck[i] = new PoliticCard(true);
		}
		dpc.setDeck(testDeck);
		assertTrue(dpc.getDeck()[1].getJolly());
		assertTrue(dpc.getDeck()[2].getJolly());
		assertTrue(dpc.getDeck()[3].getJolly());
		assertTrue(dpc.getDeck()[4].getJolly());
		assertTrue(dpc.getDeck()[5].getJolly());
		assertTrue(dpc.getDeck()[8].getJolly());
		assertTrue(dpc.getDeck()[9].getJolly());
		assertTrue(dpc.getDeck()[12].getJolly());
		assertTrue(dpc.getDeck()[14].getJolly());
	}
}
