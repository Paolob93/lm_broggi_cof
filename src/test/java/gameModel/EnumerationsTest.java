package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import org.junit.Test;

public class EnumerationsTest {

	@Test
	public void test() {
		assertEquals(3, BoardType.values().length);
		assertEquals(BoardType.SEA, BoardType.valueOf("SEA"));
		assertEquals(3, CityColors.getNumCitiesColor(CityColors.RED));
		assertEquals(4, CityColors.getNumCitiesColor(CityColors.GREY));
		assertEquals(1, CityColors.getNumCitiesColor(CityColors.VIOLET));
		assertEquals(CityColors.RED, CityColors.valueOf("RED"));
		assertEquals(11, PlayerColors.values().length);
		assertEquals(PlayerColors.BLUE, PlayerColors.valueOf("BLUE"));
		assertEquals(6, PoliticColors.values().length);
		assertEquals(PoliticColors.ORANGE, PoliticColors.valueOf("ORANGE"));
	}

}
