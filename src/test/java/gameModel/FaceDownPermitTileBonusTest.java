package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import org.junit.Before;
import org.junit.Test;

public class FaceDownPermitTileBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	FaceDownPermitTileBonus fdb;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testFaceDownPermitTileBonus() {
		fdb = new FaceDownPermitTileBonus();
		assertTrue(fdb instanceof FaceDownPermitTileBonus);
		assertEquals(fdb.getType(), "FaceDownPermitTileBonus");
	}
	
	@Test
	public void testSetValueBonus() {
		fdb = new FaceDownPermitTileBonus();
		fdb.setValueBonus(8);
		assertEquals(1, fdb.getValueBonus());
	}

	@Test
	public void testGiveBonus() {
		fdb = new FaceDownPermitTileBonus();
		assertFalse(pippo.isFdBonus());
		fdb.giveBonus(pippo);
		assertTrue(pippo.isFdBonus());
	}
}
