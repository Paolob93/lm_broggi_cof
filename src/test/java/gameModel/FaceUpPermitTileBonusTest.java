package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import org.junit.Before;
import org.junit.Test;

public class FaceUpPermitTileBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	FaceUpPermitTileBonus fub;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testFaceUpPermitTileBonus() {
		fub = new FaceUpPermitTileBonus();
		assertTrue(fub instanceof FaceUpPermitTileBonus);
		assertEquals(fub.getType(), "FaceUpPermitTileBonus");
	}
	
	@Test
	public void testSetValueBonus() {
		fub = new FaceUpPermitTileBonus();
		fub.setValueBonus(8);
		assertEquals(1, fub.getValueBonus());
	}

	@Test
	public void testGiveBonus() {
		fub = new FaceUpPermitTileBonus();
		assertFalse(pippo.isFuBonus());
		fub.giveBonus(pippo);
		assertTrue(pippo.isFuBonus());
	}
}
