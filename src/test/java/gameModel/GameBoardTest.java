package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import org.junit.Before;
import org.junit.Test;

public class GameBoardTest {
	PlayerList pl;
	Player testPlayer, pippo, egli, esso;
	Match match;
	GameBoard gameBoard;
	String path;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		path = match.getGameBoard().getSettingsPath();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(path);
		}
		NobilityTrack nt = new NobilityTrack(path);
		nt.setNobilityTrackArray(ntt);
		match.getGameBoard().getKingBoard().setNobilityTrack(nt);
		testPlayer = new Player("testPlayer", match);
		pippo = new Player("Pippo", match);
		egli = new Player("Egli", match);
		esso = new Player("Esso", PlayerColors.BLUE, match, 5);
		gameBoard = match.getGameBoard();
	}

	@Test
	public void testGetKingTileBonus() {
		KingTileBonus[] bonus = gameBoard.getKingTileBonus();
		bonus[2].giveBonus(pippo);
		assertEquals(12, pippo.getVictoryPoints());
	}

	@Test
	public void testGiveKingTileBonus() {
		gameBoard.giveKingTileBonus(pippo);
		assertEquals(25, pippo.getVictoryPoints());
		gameBoard.giveKingTileBonus(pippo);
		gameBoard.giveKingTileBonus(pippo);
		gameBoard.giveKingTileBonus(pippo);
		gameBoard.giveKingTileBonus(pippo);
		assertEquals(65, pippo.getVictoryPoints());
		gameBoard.giveKingTileBonus(testPlayer);
		assertEquals(0, testPlayer.getVictoryPoints());
	}

	@Test
	public void testGetRegionBoardType() {
		RegionBoard region0 = gameBoard.getBoards()[0];
		RegionBoard region1 = gameBoard.getBoards()[1];
		RegionBoard region2 = gameBoard.getBoards()[2];
		assertEquals(region0, gameBoard.getRegionBoardType(BoardType.SEA));
		assertEquals(region1, gameBoard.getRegionBoardType(BoardType.HILL));
		assertEquals(region2, gameBoard.getRegionBoardType(BoardType.MOUNTAIN));
	}

	@Test
	public void testShowRewardTokens() {
		assertEquals("Reward Token #", gameBoard.showRewardTokens().substring(0, 14));
	}

	@Test
	public void testGetAllRewardTokens() {
		assertEquals(15, gameBoard.getAllRewardTokens().size());
	}

	@Test
	public void testGetBonusCityTile() {
		gameBoard.getBonusCityTile(CityColors.BLUE).giveBonus(pippo);
		gameBoard.getBonusCityTile(CityColors.GOLD).giveBonus(pippo);
		gameBoard.getBonusCityTile(CityColors.GREY).giveBonus(pippo);
		gameBoard.getBonusCityTile(CityColors.RED).giveBonus(pippo);
		assertEquals(45, pippo.getVictoryPoints());
	}
}
