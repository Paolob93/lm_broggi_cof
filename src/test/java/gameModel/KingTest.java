package gameModel;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class KingTest {
	PlayerList pl;
	Match match;
	GameBoard gameboard;
	King king;
	City j;
	City a;
	City f;
	
	@Before
	public void setUp() throws RemoteException {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		gameboard = match.getGameBoard();
		king = gameboard.getKing();
		j = gameboard.findVioletCity();
		a = gameboard.getCities().get("Arkon");
		f = gameboard.getCities().get("Framek");
	}
	
	@Test
	public void testGetDistance() {
		int dist = king.getDistance(j);
		assertEquals(dist, 0);
		assertEquals(king.getDistance(a), 4);
	}


	@Test
	public void testSetPosition() {
		king.setPosition(a);
		assertEquals(king.getPosition(), a);
	}

	@Test
	public void testSetStartPosition() {
		king.setStartPosition(f);
		assertTrue(king.getPosition().equals(gameboard.findVioletCity()));
	}

}
