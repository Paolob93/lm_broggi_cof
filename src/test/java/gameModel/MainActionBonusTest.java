package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class MainActionBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	MainActionBonus ma;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testMainActionBonus() {
		ma = new MainActionBonus();
		assertTrue(ma instanceof MainActionBonus);
		assertEquals(ma.getType(), "MainActionBonus");
	}

	@Test
	public void testMainActionBonusInt() {
		ma = new MainActionBonus(5);
		assertEquals(ma.getValueBonus(), 1);
		assertEquals(ma.getType(), "MainActionBonus");
	}

	@Test
	public void testSetValueBonus() {
		ma = new MainActionBonus();
		ma.setValueBonus(8);
		assertEquals(ma.getValueBonus(), 1);
	}

	@Test
	public void testGiveBonus() {
		ma = new MainActionBonus();
		assertFalse(pippo.getBonusAction());
		ma.giveBonus(pippo);
		assertTrue(pippo.getBonusAction());
	}

}
