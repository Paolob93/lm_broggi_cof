package gameModel;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import common.exception.NotAvailableAssistantsException;
import server.model.*;

public class MarketTest {
	PlayerList pl;
	Player pippo, testPlayer, egli, esso;
	Match match;
	GameBoard gameBoard;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
		testPlayer = new Player("testPlayer", match);
		egli = new Player("egli", match);
		esso = new Player("esso", match);
		pl.addPlayer(pippo);
		pl.addPlayer(testPlayer);
		pl.addPlayer(egli);
		pl.addPlayer(esso);
		gameBoard = match.getGameBoard();
	}

	@Test
	public void testMarket() {
		Market m = new Market(pl);
		assertEquals(4, m.generateRandomOrder().length);
		assertEquals(pippo.getMarket(), egli.getMarket());
	}

	@Test
	public void testShowMarket() throws NotAvailableAssistantsException {
		Market m = pippo.getMarket();
		assertEquals("Sorry, there are no items avaiable!\n", m.showMarket());
		pippo.getAssistants().add(new Assistant());
		pippo.sellAssistant(5);
		assertEquals("Assistant", m.getAvailableItems().get(0).getItem().getClass().getSimpleName());
		assertEquals("Item 0 -> Player Pippo sells Assistant for 5 coins.\n", m.showMarket());
	}

	@Test
	public void testReturnItemAtOwner() throws NotAvailableAssistantsException {
		Market m = pippo.getMarket();
		Assistant ass = new Assistant();
		pippo.getAssistants().add(ass);
		pippo.sellAssistant(5);
		assertFalse(pippo.getAssistants().contains(ass));
		m.returnItemAtOwner();
		assertTrue(pippo.getAssistants().contains(ass));
	}

	@Test
	public void testGetAvailableItems() throws NotAvailableAssistantsException {
		Market m = pippo.getMarket();
		List<ItemForSale> available;
		Assistant ass = new Assistant();
		pippo.getAssistants().add(ass);
		pippo.sellAssistant(5);
		available = m.getAvailableItems();
		assertEquals(5, available.get(0).getPrice());
	}

}
