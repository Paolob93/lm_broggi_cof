package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class MoneyBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	MoneyBonus mb;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testMoneyBonus() {
		mb = new MoneyBonus();
		assertTrue(mb instanceof MoneyBonus);
		assertEquals(mb.getType(), "MoneyBonus");
	}

	@Test
	public void testMoneyBonusInt() {
		mb = new MoneyBonus(5);
		assertEquals(mb.getValueBonus(), 5);
		assertEquals(mb.getType(), "MoneyBonus");
	}

	@Test
	public void testGiveBonus() {
		assertEquals(pippo.getMoney(), 0);
		mb = new MoneyBonus(5);
		mb.giveBonus(pippo);
		assertEquals(pippo.getMoney(), 5);
		mb = new MoneyBonus(3);
		mb.giveBonus(pippo);
		assertEquals(pippo.getMoney(), 8);
	}

}
