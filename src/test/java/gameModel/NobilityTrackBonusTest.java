package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class NobilityTrackBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	NobilityTrackBonus nb;
	GameBoard gameBoard;
	String path;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		gameBoard = match.getGameBoard();
		path = gameBoard.getSettingsPath();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(path);
		}
		NobilityTrack nt = new NobilityTrack(path);
		nt.setNobilityTrackArray(ntt);
		gameBoard.getKingBoard().setNobilityTrack(nt);
		pippo = new Player("Pippo", match);		
	}

	@Test
	public void testNobilityTrackBonus() {
		nb = new NobilityTrackBonus();
		assertTrue(nb instanceof NobilityTrackBonus);
		assertEquals(nb.getType(), "NobilityTrackBonus");
	}

	@Test
	public void testNobilityTrackBonusInt() {
		nb = new NobilityTrackBonus(5);
		assertEquals(nb.getValueBonus(), 5);
		assertEquals(nb.getType(), "NobilityTrackBonus");
	}

	@Test
	public void testGiveBonus() {
		nb = new NobilityTrackBonus(5);
		assertEquals(pippo.getNobilityTrackPosition(), 0);
		nb.giveBonus(pippo);
		assertEquals(pippo.getNobilityTrackPosition(), 5);
		nb = new NobilityTrackBonus(5);
		nb.giveBonus(pippo);
		assertEquals(pippo.getNobilityTrackPosition(), 10);
		nb.giveBonus(pippo);
		assertEquals(pippo.getNobilityTrackPosition(), 15);
	}

}
