package gameModel;

import static org.junit.Assert.*;
import server.model.*;

import org.junit.Before;
import org.junit.Test;

import common.exception.AssistantRuntimeException;

public class PermitTileTest {
	 PermitTile pt;
	 PlayerList pl;
	 Match match;
	 Player pippo;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(match.getGameBoard().getSettingsPath());
		}
		NobilityTrack nt = new NobilityTrack(match.getGameBoard().getSettingsPath());
		nt.setNobilityTrackArray(ntt);
		match.getGameBoard().getKingBoard().setNobilityTrack(nt);
		pt = match.getGameBoard().getBoards()[1].getTile(1);
	}

	
	
	@Test
	public void testGetPermitTileDetails() {
		String details = pt.getPermitTileDetails();
		System.out.println(details.substring(8, 11));
		assertTrue(details.substring(8, 11).equals("ype"));
	}

	
	@Test
	public void testGiveBonus() {
		try {
			pt.giveBonus(pippo);
			pippo.setMainActionDone(true);
			assertTrue(pippo.getAssistants().size() > 0 || pippo.getMoney() > 0 || pippo.getPoliticCards().size() > 0 || pippo.getNobilityTrackPosition() > 0
					|| pippo.getVictoryPoints() > 0 || pippo.getBonusAction());
		}
		catch (AssistantRuntimeException e) {
			assertEquals(e.getClass(), AssistantRuntimeException.class);
		}
	}

	
	@Test
	public void testGetBuildingTiles() {
		String citiesIn = new String("");
		for (City c: pt.getBuildingTiles()) {
			citiesIn = citiesIn + c.getName();
		}
		assertEquals(citiesIn, "Graden");
	}

	
	@Test
	public void testSetBuildingTiles() {
		pt.setBuildingTiles(7);
		assertEquals(pt.getBuildingTiles()[0].getName(), "Hellar");
		assertEquals(pt.getBuildingTiles()[1].getName(), "Indur");
	}

	
	@Test
	public void testGetCitiesNumber() {
		pt.setBuildingTiles(7);
		assertEquals(pt.getCitiesNumber(), 2);
	}

	@Test
	public void testBuy() {
		assertFalse(pippo.getPermitTiles().contains(pt));
		pt.buy(pippo);
		assertTrue(pippo.getPermitTiles().contains(pt));
	}

}
