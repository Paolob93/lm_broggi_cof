package gameModel;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.*;

import common.exception.BuildEmporiumException;
import common.exception.CannotLinkCityException;
import common.exception.NoPermitTilesException;
import common.exception.NotAvailableAssistantsException;
import common.exception.NotAvailableColorCouncillorsException;
import common.exception.NotAvailableCouncillorsException;
import common.exception.WrongCardsException;
import common.exception.YouArePoorException;
import server.model.*;

public class PlayerTest {

	PlayerList pl;
	Player testPlayer, pippo, egli, esso;
	Match match;
	GameBoard gameBoard;
	String path;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		path = match.getGameBoard().getSettingsPath();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(path);
		}
		NobilityTrack nt = new NobilityTrack(path);
		nt.setNobilityTrackArray(ntt);
		match.getGameBoard().getKingBoard().setNobilityTrack(nt);
		testPlayer = new Player("testPlayer", match);
		pippo = new Player("Pippo", match);
		egli = new Player("Egli", match);
		esso = new Player("Esso", PlayerColors.BLUE, match, 5);
		gameBoard = match.getGameBoard();
	}




	@Test
	public void drawPoliticCardTest() {
		assertEquals(0, pippo.getPoliticCards().size());
		pippo.drawPoliticCard();
		assertEquals(1, pippo.getPoliticCards().size());
	}

	@Test
	public void addMoneyTest() {
		testPlayer.addMoney(10);
		assertEquals(10,testPlayer.getMoney());
	}

	/* *************************************************
	 ***************************************************
	 				ELECT COUNCILLOR TEST
	 ***************************************************
	 *************************************************** */
	
	@Test
	public void electCouncillorTest() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
		assertEquals(PoliticColors.ORANGE,gameBoard.getBoards()[1].getBalcony().getCouncil().get(0).getColorCouncillor());
		assertEquals(4,testPlayer.getMoney());
		assertTrue(testPlayer.getMainActionDone());	
		testPlayer.setBonusAction(true);
		testPlayer.setMainActionDone(false);
		testPlayer.electCouncillor(PoliticColors.BLUE, gameBoard.getBoards()[1]);
		assertEquals(PoliticColors.BLUE,gameBoard.getBoards()[1].getBalcony().getCouncil().get(0).getColorCouncillor());
		assertEquals(PoliticColors.ORANGE,gameBoard.getBoards()[1].getBalcony().getCouncil().get(1).getColorCouncillor());
		assertEquals(8 ,testPlayer.getMoney());
		assertFalse(testPlayer.getMainActionDone());
	}
	
	@Test(expected = NotAvailableColorCouncillorsException.class)
	public void electCouncillorTestExcColor() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; ){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS + 1]);
			extendedCouncillors.add(c);
			i = i + 6;
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
	}

	@Test(expected = NotAvailableCouncillorsException.class)
	public void electCouncillorTestExc() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
	}
	
	/* *************************************************
	 ***************************************************
	 			ELECT  KING COUNCILLOR TEST
	 ***************************************************
	 *************************************************** */
	
	@Test
	public void electKingCouncillorTest() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electKingCouncillor(PoliticColors.ORANGE);
		assertEquals(PoliticColors.ORANGE,gameBoard.getKingBoard().getBalcony().getCouncil().get(0).getColorCouncillor());
		assertEquals(4,testPlayer.getMoney());
		assertTrue(testPlayer.getMainActionDone());
		testPlayer.setBonusAction(true);
		testPlayer.setMainActionDone(false);
		testPlayer.electKingCouncillor(PoliticColors.BLUE);
		assertEquals(PoliticColors.BLUE,gameBoard.getKingBoard().getBalcony().getCouncil().get(0).getColorCouncillor());
		assertEquals(PoliticColors.ORANGE,gameBoard.getKingBoard().getBalcony().getCouncil().get(1).getColorCouncillor());
		assertEquals(8 ,testPlayer.getMoney());
		assertFalse(testPlayer.getMainActionDone());
	}
	
	@Test(expected = NotAvailableColorCouncillorsException.class)
	public void electKingCouncillorTestExcColor() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; ){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS + 1]);
			extendedCouncillors.add(c);
			i = i + 6;
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electKingCouncillor(PoliticColors.ORANGE);
	}
	
	@Test(expected = NotAvailableCouncillorsException.class)
	public void electKingCouncillorTestExc() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException{
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		gameBoard.setSetConucillors(extendedCouncillors);
		testPlayer.electKingCouncillor(PoliticColors.ORANGE);
	}

	/* *************************************************
	 ***************************************************
	 			 BUY PERMIT TILE TEST
	 ***************************************************
	 *************************************************** */
	
	@Test
	public void buyPermitTileTest() throws YouArePoorException, WrongCardsException, RemoteException{
		ArrayList<PoliticCard> cards = new ArrayList<>();
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		try {
			testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
			testPlayer.electCouncillor(PoliticColors.BLUE, gameBoard.getBoards()[1]);
		} catch (NotAvailableCouncillorsException | NotAvailableColorCouncillorsException e) {}
		cards.add(new PoliticCard(PoliticColors.ORANGE));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(PoliticColors.BLUE));
		PermitTile tile = gameBoard.getBoards()[1].getBusinessPermitTiles()[0];
		testPlayer.setMainActionDone(false);
		testPlayer.buyPermitTile(gameBoard.getBoards()[1], 0, cards);
		int moneybonus = 0;
		for (Bonus b: tile.getBonus()) {
			if (b.getType().equals("MoneyBonus")) {
				moneybonus = b.getValueBonus();
			}
		}
		assertEquals(3 + moneybonus, testPlayer.getMoney());
		assertTrue(testPlayer.getPermitTiles().contains(tile));
		cards.clear();
		testPlayer.addMoney(10);
		testPlayer.setMainActionDone(false);
		testPlayer.setBonusAction(true);
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		PermitTile tile1 = gameBoard.getBoards()[1].getBusinessPermitTiles()[0];
		testPlayer.buyPermitTile(gameBoard.getBoards()[1], 0, cards);
		int moneybonus1 = 0;
		for (Bonus b: tile1.getBonus()) {
			if (b.getType().equals("MoneyBonus")) {
				moneybonus1 = b.getValueBonus();
			}
		}
		assertFalse(testPlayer.getMainActionDone());
		assertEquals(9 + moneybonus + moneybonus1, testPlayer.getMoney());
		
		//PROVA DELLE COSE COMPLICATE
		
		cards.clear();
		testPlayer = new Player("test", match);
		testPlayer.getAssistants().add(new Assistant());
		testPlayer.getAssistants().add(new Assistant());
		testPlayer.getAssistants().add(new Assistant());
		testPlayer.getPoliticCards().add(new PoliticCard(true));
		cards.addAll(testPlayer.getPoliticCards());
		Player copia = new Player(testPlayer);
		BackUpGameBoard bg = new BackUpGameBoard(testPlayer.getGameBoard());
		testPlayer.addMoney(50);
		assertEquals(0, copia.getMoney());
		testPlayer.getPoliticCards().add(new PoliticCard(true));
		assertEquals(2, testPlayer.getPoliticCards().size());
		testPlayer.getGameBoard().getCities().get("Arkon").getEmporiums().add(testPlayer);
		testPlayer.getCitiesOwned().add(testPlayer.getGameBoard().getCities().get("Arkon"));
		testPlayer.restorePlayer(copia);
		testPlayer.getGameBoard().restoreGameBoard(bg);
		assertFalse(testPlayer.getCitiesOwned().contains(testPlayer.getGameBoard().getCities().get("Arkon")));
		assertFalse(testPlayer.getGameBoard().getCities().get("Arkon").getEmporiums().contains(testPlayer));
		copia.addMoney(5);
		assertEquals(0, testPlayer.getMoney());
		copia.getPoliticCards().clear();
		assertEquals(1, testPlayer.getPoliticCards().size());
		try {
			testPlayer.buyPermitTile(gameBoard.getBoards()[1], 0, cards);
			assertEquals(1, testPlayer.getPermitTiles().size());
		}
		catch (YouArePoorException e) {
			testPlayer.restorePlayer(copia);
			assertTrue(testPlayer.getPermitTiles().isEmpty());
		}
	}
	
	@Test(expected = WrongCardsException.class)
	public void buyPermitTileTestExcWrong() throws YouArePoorException, WrongCardsException, RemoteException{
		ArrayList<PoliticCard> cards = new ArrayList<>();
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		try {
			testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
			testPlayer.electCouncillor(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
			testPlayer.electCouncillor(PoliticColors.BLUE, gameBoard.getBoards()[1]);
			testPlayer.electCouncillor(PoliticColors.BLUE, gameBoard.getBoards()[1]);
		} catch (NotAvailableCouncillorsException | NotAvailableColorCouncillorsException e) {}
		cards.add(new PoliticCard(PoliticColors.BLACK));
		cards.add(new PoliticCard(PoliticColors.BLACK));
		testPlayer.buyPermitTile(gameBoard.getBoards()[1], 0, cards);
	}
	
	@Test(expected = YouArePoorException.class)
	public void buyPermitTileTestExcPoor() throws YouArePoorException, WrongCardsException, RemoteException{
		ArrayList<PoliticCard> cards = new ArrayList<>();
		cards.add(new PoliticCard(true));
		testPlayer.buyPermitTile(gameBoard.getBoards()[1], 0, cards);
	}
	
	/* *************************************************
	 ***************************************************
	 			 BUILD EMPORIUM TEST
	 ***************************************************
	 *************************************************** */
	
	@Test
	public void buildEmporiumTest() throws BuildEmporiumException, NotAvailableAssistantsException, CannotLinkCityException {
		Bonus[] b = new Bonus[2];
		b[0] = new MoneyBonus(2);
		b[1] = new AssistantBonus(3);
		Bonus[] b1 = new Bonus[1];
		b1[0] = new MoneyBonus(5);
		Bonus[] b2 = new Bonus[2];
		b2[0] = new MainActionBonus();
		b2[1] = new PoliticCardBonus(3);
		RewardToken rt = new RewardToken(path);
		rt.setBonus(b);
		RewardToken rt1 = new RewardToken(path);
		rt1.setBonus(b1);
		RewardToken rt2 = new RewardToken(path);
		rt2.setBonus(b2);
		City prova = new City("Prova", CityColors.BLUE, path);
		prova.setRewardToken(rt);
		City prova1 = new City("Prova1", CityColors.BLUE, path);
		prova1.setRewardToken(rt1);
		City prova2 = new City("Prova2", CityColors.GOLD, path);
		prova2.setRewardToken(rt2);
		prova2.getEmporiums().add(testPlayer);
		prova2.createLink(prova1);
		City prova3 = new City("Prova3", CityColors.GOLD, path);
		City prova4 = new City("Prova4", CityColors.GOLD, path);
		prova.createLink(prova1);
		prova1.getEmporiums().add(pippo);
		pippo.getCitiesOwned().add(prova1);
		City[] inBoards = new City[5];
		inBoards[0] = prova;
		inBoards[1] = prova1;
		inBoards[2] = prova2;
		inBoards[3] = prova3;
		inBoards[4] = prova4;
		gameBoard.getBoards()[0].setCitiesInBoard(inBoards);
		City[] buildingTiles = new City[1];
		buildingTiles[0] = prova;
		PermitTile pt = new PermitTile(buildingTiles, path);
		pippo.getPermitTiles().add(pt);
		City[] buildingTiles2 = new City[1];
		buildingTiles2[0] = prova2;
		PermitTile pt2 = new PermitTile(buildingTiles2, path);
		pippo.buildEmporium(prova, pt);
		assertTrue(pippo.getPermitTiles().isEmpty());
		assertTrue(pippo.getUsedPermitTiles().contains(pt));
		assertEquals(7, pippo.getMoney());
		assertEquals(3, pippo.getAssistants().size());
		pippo.setMainActionDone(false);
		pippo.getPermitTiles().add(pt2);
		pippo.getCitiesOwned().add(prova3);
		pippo.getCitiesOwned().add(prova4);
		prova3.getEmporiums().add(pippo);
		prova4.getEmporiums().add(pippo);
		pippo.buildEmporium(prova2, pt2);
		assertFalse(pippo.getMainActionDone());
		assertEquals(5, pippo.getAssistants().size());
		assertEquals(3, pippo.getPoliticCards().size());
		assertEquals(14, pippo.getMoney());	
		assertEquals(53, pippo.getVictoryPoints());
	}

	@Test(expected = BuildEmporiumException.class)
	public void buildEmporiumTestExcBuild() throws BuildEmporiumException, NotAvailableAssistantsException, CannotLinkCityException {
		Bonus[] b = new Bonus[2];
		b[0] = new MoneyBonus(2);
		b[1] = new AssistantBonus(3);
		Bonus[] b1 = new Bonus[1];
		b1[0] = new MoneyBonus(5);
		Bonus[] b2 = new Bonus[2];
		b2[0] = new MainActionBonus();
		b2[1] = new PoliticCardBonus(3);
		RewardToken rt = new RewardToken(path);
		rt.setBonus(b);
		RewardToken rt1 = new RewardToken(path);
		rt1.setBonus(b1);
		RewardToken rt2 = new RewardToken(path);
		rt2.setBonus(b2);
		City prova = new City("Prova", CityColors.BLUE, path);
		prova.setRewardToken(rt);
		City prova1 = new City("Prova1", CityColors.BLUE, path);
		prova1.setRewardToken(rt1);
		City prova2 = new City("Prova2", CityColors.GOLD, path);
		prova2.setRewardToken(rt2);
		prova2.getEmporiums().add(testPlayer);
		prova2.createLink(prova1);
		City prova3 = new City("Prova3", CityColors.GOLD, path);
		City prova4 = new City("Prova4", CityColors.GOLD, path);
		prova1.getEmporiums().add(pippo);
		pippo.getCitiesOwned().add(prova1);
		City[] inBoards = new City[5];
		inBoards[0] = prova;
		inBoards[1] = prova1;
		inBoards[2] = prova2;
		inBoards[3] = prova3;
		inBoards[4] = prova4;
		gameBoard.getBoards()[0].setCitiesInBoard(inBoards);
		City[] buildingTiles = new City[1];
		buildingTiles[0] = prova1;
		PermitTile pt = new PermitTile(buildingTiles, path);
		pippo.getPermitTiles().add(pt);
		pippo.buildEmporium(prova, pt);
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void buildEmporiumTestExcAss() throws BuildEmporiumException, NotAvailableAssistantsException, CannotLinkCityException {
		Bonus[] b = new Bonus[2];
		b[0] = new MoneyBonus(2);
		b[1] = new AssistantBonus(1);
		Bonus[] b1 = new Bonus[1];
		b1[0] = new MoneyBonus(5);
		Bonus[] b2 = new Bonus[2];
		b2[0] = new MainActionBonus();
		b2[1] = new PoliticCardBonus(3);
		RewardToken rt = new RewardToken(path);
		rt.setBonus(b);
		RewardToken rt1 = new RewardToken(path);
		rt1.setBonus(b1);
		RewardToken rt2 = new RewardToken(path);
		rt2.setBonus(b2);
		City prova = new City("Prova", CityColors.BLUE, path);
		prova.setRewardToken(rt);
		City prova1 = new City("Prova1", CityColors.BLUE, path);
		prova1.setRewardToken(rt1);
		City city = new City("Prova2", CityColors.GOLD, path);
		City prova2 = city;
		prova2.setRewardToken(rt2);
		prova2.getEmporiums().add(testPlayer);
		prova2.getEmporiums().add(egli);
		prova2.createLink(prova1);
		City prova3 = new City("Prova3", CityColors.GOLD, path);
		City prova4 = new City("Prova4", CityColors.GOLD, path);
		prova.createLink(prova1);
		prova1.getEmporiums().add(pippo);
		pippo.getCitiesOwned().add(prova1);
		City[] inBoards = new City[5];
		inBoards[0] = prova;
		inBoards[1] = prova1;
		inBoards[2] = prova2;
		inBoards[3] = prova3;
		inBoards[4] = prova4;
		gameBoard.getBoards()[0].setCitiesInBoard(inBoards);
		City[] buildingTiles = new City[1];
		buildingTiles[0] = prova;
		PermitTile pt = new PermitTile(buildingTiles, path);
		pippo.getPermitTiles().add(pt);
		City[] buildingTiles2 = new City[1];
		buildingTiles2[0] = prova2;
		PermitTile pt2 = new PermitTile(buildingTiles2, path);
		pippo.buildEmporium(prova, pt);
		assertTrue(pippo.getPermitTiles().isEmpty());
		assertTrue(pippo.getUsedPermitTiles().contains(pt));
		assertEquals(7, pippo.getMoney());
		assertEquals(1, pippo.getAssistants().size());
		pippo.getPermitTiles().add(pt2);
		pippo.buildEmporium(prova2, pt2);
	}
	
	/* *************************************************
	 ***************************************************
	 			BUILD EMPORIUM BY KING TEST
	 ***************************************************
	 *************************************************** */
	
	@Test
	public void buildEmporiumByKing() throws Exception {
		ArrayList<PoliticCard> cards = new ArrayList<>();
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		cards.add(new PoliticCard(PoliticColors.ORANGE));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(PoliticColors.BLUE));
		pippo.addMoney(50);
		pippo.addAssistant(10);
		pippo.electKingCouncillor(PoliticColors.ORANGE);
		pippo.electKingCouncillor(PoliticColors.BLUE);
		gameBoard.getCities().get("Merkatim").getEmporiums().add(testPlayer);
		// Setting an appropriate RewardToken
		Bonus[] b = new Bonus[1];
		b[0] = new MoneyBonus(5);
		RewardToken rt = new RewardToken(path);
		rt.setBonus(b);
		gameBoard.getCities().get("Merkatim").setRewardToken(rt);
		Bonus[] b1 = new Bonus[2];
		b1[0] = new PoliticCardBonus(5);
		b1[1] = new AssistantBonus(1);
		RewardToken rt1 = new RewardToken(path);
		rt1.setBonus(b1);
		Bonus[] b2 = new Bonus[1];
		b2[0] = new MoneyBonus(1);
		RewardToken rt2 = new RewardToken(path);
		rt2.setBonus(b2);
		gameBoard.getCities().get("Lyram").setRewardToken(rt2);
		assertEquals(0, pippo.getVictoryPoints());
		gameBoard.getCities().get("Arkon").getEmporiums().add(pippo);
		gameBoard.getCities().get("Osium").getEmporiums().add(pippo);
		gameBoard.getCities().get("Osium").setRewardToken(rt1);
		pippo.getCitiesOwned().add(gameBoard.getCities().get("Osium"));
		pippo.getCitiesOwned().add(gameBoard.getCities().get("Arkon"));
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Merkatim"), cards);
		assertEquals(54, pippo.getMoney());
		assertEquals(10, pippo.getAssistants().size());
		assertEquals(5, pippo.getPoliticCards().size());
		assertEquals(30, pippo.getVictoryPoints());
		cards.clear();
		pippo.setMainActionDone(false);
		pippo.setBonusAction(true);
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		cards.add(new PoliticCard(true));
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Lyram"), cards);
		assertEquals(52, pippo.getMoney());
		assertEquals(11, pippo.getAssistants().size());
		assertEquals(10, pippo.getPoliticCards().size());
		assertEquals(30, pippo.getVictoryPoints());
	}
	
	@Test(expected = BuildEmporiumException.class)
	public void buildEmporiumByKingExcBE() throws Exception {
		gameBoard.getCities().get("Arkon").getEmporiums().add(pippo);
		ArrayList<PoliticCard> cards = new ArrayList<>();
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Arkon"), cards);
	}
	

	@Test(expected = WrongCardsException.class)
	public void buildEmporiumByKingExcWC() throws Exception {
		ArrayList<PoliticCard> cards = new ArrayList<>();
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Arkon"), cards);
	}
	
	@Test(expected = YouArePoorException.class)
	public void buildEmporiumByKingExcPoor() throws Exception {
		ArrayList<PoliticCard> cards = new ArrayList<>();
		cards.add(new PoliticCard(true));
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Arkon"), cards);
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void buildEmporiumByKingExcAss() throws Exception {
		ArrayList<PoliticCard> cards = new ArrayList<>();
		cards.add(new PoliticCard(true));
		pippo.addMoney(50);
		gameBoard.getCities().get("Arkon").getEmporiums().add(egli);
		gameBoard.getCities().get("Arkon").getEmporiums().add(esso);
		gameBoard.getCities().get("Arkon").getEmporiums().add(testPlayer);
		pippo.buildEmporiumByKing(gameBoard.getCities().get("Arkon"), cards);
	}

	@Test
	public void electCouncillorByAssistantTest() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		pippo.addAssistant(5);
		pippo.electCouncillorByAssistant(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
		assertEquals(4, pippo.getAssistants().size());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(PoliticColors.ORANGE,gameBoard.getBoards()[1].getBalcony().getCouncil().get(0).getColorCouncillor());
		pippo.setBonusAction(true);
		pippo.electCouncillorByAssistant(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
		assertEquals(3, pippo.getAssistants().size());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(PoliticColors.ORANGE,gameBoard.getBoards()[1].getBalcony().getCouncil().get(0).getColorCouncillor());
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void electCouncillorByAssistantTestExc() throws NotAvailableCouncillorsException, NotAvailableColorCouncillorsException, NotAvailableAssistantsException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		pippo.electCouncillorByAssistant(PoliticColors.ORANGE, gameBoard.getBoards()[1]);
	}
		
	@Test
	public void electKingCouncillorByAssistantTest() throws NotAvailableAssistantsException, NotAvailableCouncillorsException, NotAvailableColorCouncillorsException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		pippo.addAssistant(5);
		pippo.electKingCouncillorByAssistant(PoliticColors.ORANGE);
		assertEquals(4, pippo.getAssistants().size());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(PoliticColors.ORANGE,gameBoard.getKingBoard().getBalcony().getCouncil().get(0).getColorCouncillor());
		pippo.setBonusAction(true);
		pippo.electKingCouncillorByAssistant(PoliticColors.ORANGE);
		assertEquals(3, pippo.getAssistants().size());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(PoliticColors.ORANGE,gameBoard.getKingBoard().getBalcony().getCouncil().get(0).getColorCouncillor());
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void electKingCouncillorByAssistantTestExc() throws NotAvailableAssistantsException, NotAvailableCouncillorsException, NotAvailableColorCouncillorsException {
		ArrayList<Councillor> extendedCouncillors = new ArrayList<Councillor>();
		for(int i=0; i<24; i++){
			Councillor c = new Councillor(PoliticColors.values()[i % Constants.NUM_POLITIC_COLORS]);
			extendedCouncillors.add(c);
		}
		Collections.shuffle(extendedCouncillors);
		gameBoard.setSetConucillors(extendedCouncillors);
		pippo.electKingCouncillorByAssistant(PoliticColors.ORANGE);
	}
	
	@Test
	public void buyAssistantTest() throws NotAvailableAssistantsException, YouArePoorException {
		pippo.addMoney(10);
		pippo.buyAssistant();
		assertEquals(7, pippo.getMoney());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(1, pippo.getAssistants().size());
		pippo.setBonusAction(true);
		pippo.buyAssistant();
		assertEquals(4, pippo.getMoney());
		assertTrue(pippo.getQuickActionDone());
		assertEquals(2, pippo.getAssistants().size());
	}
	
	@Test(expected = YouArePoorException.class)
	public void buyAssistantTestExc() throws NotAvailableAssistantsException, YouArePoorException {
		pippo.buyAssistant();
	}
	
	@Test
	public void changeTilesTest() throws NotAvailableAssistantsException {
		RegionBoard region = gameBoard.getBoards()[1];
		PermitTile pt0 = region.getBusinessPermitTiles()[0];
		PermitTile pt1 = region.getBusinessPermitTiles()[1];
		pippo.addAssistant(10);
		pippo.changeTiles(region);
		PermitTile pt2 = region.getBusinessPermitTiles()[0];
		PermitTile pt3 = region.getBusinessPermitTiles()[1];
		assertEquals(9, pippo.getAssistants().size());
		assertFalse(pt0.equals(pt2));
		assertFalse(pt1.equals(pt3));
		assertTrue(pippo.getQuickActionDone());
		pippo.setBonusAction(true);
		pippo.changeTiles(region);
		assertFalse(pt2.equals(region.getBusinessPermitTiles()[0]));
		assertFalse(pt3.equals(region.getBusinessPermitTiles()[1]));
		assertTrue(pippo.getQuickActionDone());
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void changeTilesTestExc() throws NotAvailableAssistantsException {
		RegionBoard region = gameBoard.getBoards()[1];
		pippo.changeTiles(region);
	}
	
	@Test
	public void getAnotherMainActionTest() throws NotAvailableAssistantsException {
		pippo.addAssistant(10);
		pippo.setMainActionDone(true);
		assertFalse(pippo.getQuickActionDone());
		pippo.getAnotherMainAction();
		assertEquals(7, pippo.getAssistants().size());
		assertTrue(pippo.getQuickActionDone());
		assertFalse(pippo.getMainActionDone());
	}
	
	@Test(expected = NotAvailableAssistantsException.class)
	public void getAnotherMainActionTestExc() throws NotAvailableAssistantsException {
		pippo.getAnotherMainAction();
	}

	@Test
	public void showFDTilesTest() throws Exception {
		PermitTile pt = new PermitTile(gameBoard.getBoards()[1]);
		Bonus[] b = new Bonus[1];
		b[0] = new MoneyBonus(5);
		pt.setBonus(b);
		PermitTile pt1 = new PermitTile(gameBoard.getBoards()[1]);
		Bonus[] b1 = new Bonus[1];
		b1[0] = new MoneyBonus(5);
		pt1.setBonus(b1);
		pippo.getPermitTiles().add(pt);
		pippo.getUsedPermitTiles().add(pt1);
		assertEquals("Permit Tile #0 	Bonus type: MoneyBonus -> value: 5\nPermit Tile #1 	Bonus type: MoneyBonus -> value: 5\n", pippo.showFDTiles());
	}
	
	@Test(expected = NoPermitTilesException.class)
	public void showFDTilesTestExc() throws Exception {
		pippo.showFDTiles();
	}
	
	@Test
	public void showFUTilesTest() {
		List<PermitTile> availablePT = new ArrayList<>();
		for (RegionBoard region: this.gameBoard.getBoards()) {
			for (PermitTile pt: region.getBusinessPermitTiles()) {
				availablePT.add(pt);
			}
		}
		String showtiles = new String();
		int i= 0;
		for (PermitTile pt: availablePT) {
			showtiles = showtiles + "Permit Tile #" + i + " " + pt.getPermitTileDetails();
			i++;
		}
		assertEquals(showtiles, pippo.showFUTiles());
	}

	@Test
	public void showPlayerRewardTokensTest() {
		String showtokens = new String();
		int i= 0;
		for (City c: pippo.getCitiesOwned()) {
			if(!c.isNoble()){
				showtokens = showtokens + "Reward Token #" + i + " " + c.getRewardToken().getRewardTokenDetails();
			}
			i++;
		}
		assertEquals (showtokens, pippo.showPlayerRewardTokens());
		pippo.getCitiesOwned().add(gameBoard.getCities().get("Arkon"));
		String showtokens1 = new String();
		int i1= 0;
		for (City c: pippo.getCitiesOwned()) {
			if(!c.isNoble()){
				showtokens1 = showtokens1 + "Reward Token #" + i1 + " " + c.getRewardToken().getRewardTokenDetails();
			}
			i1++;
		}
		assertEquals (showtokens1, pippo.showPlayerRewardTokens());
	}
	
	@Test(expected = YouArePoorException.class)
	public void removeMoneyTestExc() throws YouArePoorException {
		pippo.removeMoney(10);
	}
	
	@Test
	public void removeMoneyTest() throws YouArePoorException {
		pippo.addMoney(10);
		pippo.removeMoney(5);
		assertEquals(5, pippo.getMoney());
	}
	
	@Test
	public void gettersAndSettersTest() {
		pippo.setName("Pippo");
		assertEquals("Pippo", pippo.getName());
		pippo.setColor(PlayerColors.BLUE);
		assertEquals(PlayerColors.BLUE, pippo.getColor());
		assertTrue(pippo.getMarket() instanceof Market);
		pippo.getCitiesOwned().add(gameBoard.getCities().get("Arkon"));
		assertEquals(1, pippo.getNumEmporiums());
		pippo.incrementNobilityTrackPosition(5);
		assertEquals(5, pippo.getNobilityTrackPosition());
		assertEquals(5, esso.getID());
		pippo.setFuBonus(true);
		assertTrue(pippo.isFuBonus());
		pippo.setFdBonus(true);
		assertTrue(pippo.isFdBonus());
		pippo.setRtBonus(4);
		assertEquals(4, pippo.getRtBonus());
	}
	
	@Test
	public void sellATest() throws YouArePoorException, NotAvailableAssistantsException{
		Market market = pippo.getMarket();
		Assistant ass = new Assistant();
		pippo.getAssistants().add(ass);
		assertEquals(0, market.getAvailableItems().size());
		assertEquals(1, pippo.getAssistants().size());
		pippo.sellAssistant(5);
		assertEquals(0, pippo.getAssistants().size());
		assertEquals(1, market.getAvailableItems().size());
		ItemForSale it = market.getAvailableItems().get(0);
		egli.addMoney(10);
		egli.buyItem(it);
		assertEquals(5, egli.getMoney());
		assertEquals(5, pippo.getMoney());
		assertEquals(0, market.getAvailableItems().size());
		pippo.getAssistants().add(new Assistant());
		pippo.getAssistants().add(new Assistant());
		pippo.getAssistants().add(new Assistant());
		pippo.sellAssistant(2, 5);
		it = market.getAvailableItems().get(0);
		egli.addMoney(10);
		egli.buyItem(it);
		market.returnItemAtOwner();
		assertEquals(10, egli.getMoney());
		assertEquals(10, pippo.getMoney());
		assertEquals(2, pippo.getAssistants().size());
		assertEquals(2, egli.getAssistants().size());
	}
	
	@Test
	public void sellPCTest() throws YouArePoorException, RemoteException{
		Market market = pippo.getMarket();
		PoliticCard pc = new PoliticCard(true);
		pippo.getPoliticCards().add(pc);
		assertEquals(0, market.getAvailableItems().size());
		assertEquals(1, pippo.getPoliticCards().size());
		pippo.sell(pc, 5);
		assertEquals(0, pippo.getPoliticCards().size());
		assertEquals(1, market.getAvailableItems().size());
		ItemForSale it = market.getAvailableItems().get(0);
		egli.addMoney(10);
		egli.buyItem(it);
		assertEquals(5, egli.getMoney());
		assertEquals(5, pippo.getMoney());
		assertEquals(0, market.getAvailableItems().size());
	}
	
	@Test
	public void sellPTTest() throws YouArePoorException, RemoteException{
		City[] c = new City[1];
		c[0] = new City("Cit", CityColors.BLUE, path);
		Market market = pippo.getMarket();
		PermitTile tc = new PermitTile(c, path);
		pippo.getPermitTiles().add(tc);
		assertEquals(0, market.getAvailableItems().size());
		assertEquals(1, pippo.getPermitTiles().size());
		pippo.sell(tc, 5);
		assertEquals(0, pippo.getPermitTiles().size());
		assertEquals(1, market.getAvailableItems().size());
		ItemForSale it = market.getAvailableItems().get(0);
		egli.addMoney(10);
		egli.buyItem(it);
		assertEquals(5, egli.getMoney());
		assertEquals(5, pippo.getMoney());
		assertEquals(0, market.getAvailableItems().size());
	}
}