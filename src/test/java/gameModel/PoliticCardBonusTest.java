package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class PoliticCardBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	PoliticCardBonus pcb;
	GameBoard gameBoard;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		gameBoard = match.getGameBoard();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(gameBoard.getSettingsPath());
		}
		NobilityTrack nt = new NobilityTrack(gameBoard.getSettingsPath());
		nt.setNobilityTrackArray(ntt);
		gameBoard.getKingBoard().setNobilityTrack(nt);
		pippo = new Player("Pippo", match);		
	}

	@Test
	public void testPoliticCardBonus() {
		pcb = new PoliticCardBonus();
		assertTrue(pcb instanceof PoliticCardBonus);
		assertEquals(pcb.getType(), "PoliticCardBonus");
	}

	@Test
	public void testPoliticCardBonusInt() {
		pcb = new PoliticCardBonus(5);
		assertEquals(pcb.getValueBonus(), 5);
		assertEquals(pcb.getType(), "PoliticCardBonus");
	}

	@Test
	public void testGiveBonus() {
		PoliticCard pc0 = gameBoard.getDeck().getDeck()[0];
		PoliticCard pc1 = gameBoard.getDeck().getDeck()[1];
		PoliticCard pc7 = gameBoard.getDeck().getDeck()[1];
		pcb = new PoliticCardBonus(5);
		assertTrue(pippo.getPoliticCards().isEmpty());
		pcb.giveBonus(pippo);
		assertTrue(pippo.getPoliticCards().size() == 5);
		assertTrue(pippo.getPoliticCards().contains(pc0));
		assertTrue(pippo.getPoliticCards().contains(pc1));
		pcb = new PoliticCardBonus(5);
		pcb.giveBonus(pippo);
		assertTrue(pippo.getPoliticCards().size() == 10);
		assertTrue(pippo.getPoliticCards().contains(pc7));
	}
}
