package gameModel;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class PoliticCardTest {
	PlayerList pl;
	Match match;
	Player p1 ;
	
	List<PoliticCard> cards = new ArrayList<PoliticCard>();
	PoliticCard card1;
	PoliticCard card2;
	PoliticCard card3;
	PoliticCard card4;
	PoliticCard card5;
	PoliticCard card6;
	PoliticCard card7;
	PoliticCard card8;
	PoliticCard card9;
	PoliticCard card10;
	
	@Before public void setUp() throws RemoteException{
		pl = new PlayerList();
		card1 = new PoliticCard(true);
		card2 = new PoliticCard(true);
		card3 = new PoliticCard(true);
		card4 = new PoliticCard(true);
		card5 = new PoliticCard(true);
		card6 = new PoliticCard(true);
		card7 = new PoliticCard(true);
		card8 = new PoliticCard(true);
		card9 = new PoliticCard(true);
		card10 = new PoliticCard(PoliticColors.BLACK);
		match = new Match(pl, 0);
		match.initMatch();
		p1 = new Player("p1", match);
		cards.add(card1);
		cards.add(card2);
		cards.add(card3);
		cards.add(card4);
		cards.add(card5);
		cards.add(card6);
		cards.add(card7);
		cards.add(card8);
		cards.add(card9);
		cards.add(card10);
	}
	

	@Test
	public void testBuy() {
		p1.getPoliticCards().add(cards.get(0));
		assertTrue(p1.getPoliticCards().contains(card1));
	}
	
	
	@Test
	public void getJollyCardsNumber() {
    	int jollyCardsNumber = 0;
    	for (PoliticCard card: cards) {
    		if (card.getJolly()) {
    			jollyCardsNumber++;
    		}
    	}
    	assertEquals(jollyCardsNumber, 9);
    }
}
