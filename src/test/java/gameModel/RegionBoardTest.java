package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class RegionBoardTest {
	PlayerList pl;
	Player testPlayer, pippo, egli, esso;
	Match match;
	GameBoard gameBoard;
	RegionBoard region;
	String path;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		path = match.getGameBoard().getSettingsPath();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(path);
		}
		NobilityTrack nt = new NobilityTrack(path);
		nt.setNobilityTrackArray(ntt);
		match.getGameBoard().getKingBoard().setNobilityTrack(nt);
		testPlayer = new Player("testPlayer", match);
		pippo = new Player("Pippo", match);
		egli = new Player("Egli", match);
		esso = new Player("Esso", PlayerColors.BLUE, match, 5);
		gameBoard = match.getGameBoard();
		region = gameBoard.getBoards()[1];
	}

	@Test
	public void testGetTile() {
		PermitTile pt = new PermitTile(region);
		  pt = region.getBusinessPermitTiles()[0];
		  assertEquals(13, region.getPermitTileDeck().size());
		  PermitTile pt1 = new PermitTile(region);
		  pt1 = region.getTile(0);
		  assertEquals(pt, pt1);
		  assertEquals(13, region.getPermitTileDeck().size());
		  region.getTile(0);
		  region.getTile(1);
		  region.getTile(1);
		  region.getTile(0);
		  assertEquals(13, region.getPermitTileDeck().size());
	}

	@Test
	public void testInitPermitTileDeck() {
		region.getPermitTileDeck().clear();
		region.initPermitTileDeck();
		assertEquals(15, region.getPermitTileDeck().size());
	}

	@Test
	public void testSetBusinessPermitTileMetods() {
		region.getPermitTileDeck().clear();
		region.initPermitTileDeck();
		PermitTile pt1 = new PermitTile(region);
		PermitTile pt2 = new PermitTile(region);
		pt1 = region.getPermitTileDeck().get(0);
		pt2 = region.getPermitTileDeck().get(1);
		assertEquals(15, region.getPermitTileDeck().size());
		region.setBusinessPermitTileMetods();
		assertEquals(13, region.getPermitTileDeck().size());
		assertEquals(pt1, region.getBusinessPermitTiles()[0]);
		assertEquals(pt2, region.getBusinessPermitTiles()[1]);
	}

	@Test
	public void testChangeTile() {
		PermitTile pt1 = new PermitTile(region);
		PermitTile pt2 = new PermitTile(region);
		pt1 = region.getBusinessPermitTiles()[0];
		pt2 = region.getBusinessPermitTiles()[1];
		region.changeTile();
		assertFalse(pt1.equals(region.getBusinessPermitTiles()[0]));
		assertFalse(pt2.equals(region.getBusinessPermitTiles()[1]));
	}


}
