package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import common.exception.AssistantRuntimeException;
import server.model.*;

public class RewardTokenTest {
	 private int bonusNumber;
	 RewardToken rt;
	 PlayerList pl;
	 Match match;
	 Player pippo;

	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		NobilityTrackTile[] ntt = new NobilityTrackTile[21];
		for(int i=1; i<ntt.length; i++ ){
			ntt[i] = new NobilityTrackTile(match.getGameBoard().getSettingsPath());
		}
		NobilityTrack nt = new NobilityTrack(match.getGameBoard().getSettingsPath());
		nt.setNobilityTrackArray(ntt);
		match.getGameBoard().getKingBoard().setNobilityTrack(nt);
		rt = new RewardToken(match.getGameBoard().getSettingsPath());
		rt.initRewardToken();
		pippo = new Player("Pippo", match);
	}


	@Test
	public void testGenerateBonusNumber() {
		assertTrue(this.bonusNumber >= 0);
		assertTrue(this.bonusNumber < 4);
	}
	
	
	@Test
	public void testGetRewardTokenDetails() {
		String details = rt.getRewardTokenDetails();
		System.out.println(details.substring(10, 13));
		assertTrue(details.substring(10, 13).equals("ype"));
	}

	
	@Test()
	public void testGiveBonus() {
		try {/*
			System.out.println(pippo.getAssistants().size());			//1		ok
			System.out.println(pippo.getMoney());						//2		ok
			System.out.println(pippo.getPoliticCards().size());			//3		ok
			System.out.println(pippo.getNobilityTrackPosition());		//4		ok
			System.out.println(pippo.getVictoryPoints());				//5		ok
			if (pippo.getMainActionDone())								//6		ok
				System.out.println("azione fatta");
			System.out.println("\n\n dopo: \n\n");*/
			rt.giveBonus(pippo);/*
			System.out.println(pippo.getAssistants().size());			//1		ok
			System.out.println(pippo.getMoney());						//2		ok
			System.out.println(pippo.getPoliticCards().size());			//3		ok
			System.out.println(pippo.getNobilityTrackPosition());		//4		ok
			System.out.println(pippo.getVictoryPoints());				//5		ok
			if (pippo.getMainActionDone())								//6		ok
				System.out.println("azione fatta");
			pippo.setMainActionDone(true);*/
			assertTrue(pippo.getAssistants().size() > 0 || pippo.getMoney() > 0 || pippo.getPoliticCards().size() > 0 || pippo.getNobilityTrackPosition() > 0
					|| pippo.getVictoryPoints() > 0 || pippo.getBonusAction());
		}
		catch (AssistantRuntimeException e) {
			assertEquals(e.getClass(), AssistantRuntimeException.class);
		}
	}
	
}
