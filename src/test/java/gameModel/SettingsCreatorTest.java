package gameModel;

import static org.junit.Assert.*;

import server.model.*;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

public class SettingsCreatorTest {
	SettingsCreator set;
	
	@Before
	public void setUp() throws RemoteException{
		set = new SettingsCreator();
	}

	@Test
	public void testSetMaxBonusNumberRT() {
		set.setMaxBonusNumberRT(5);
		assertEquals(5, set.getMaxBonusNumberRT());
	}

	@Test
	public void testSetMaxValueBonusRT() {
		set.setMaxValueBonusRT(5);
		assertEquals(5, set.getMaxValueBonusRT());
	}

	@Test
	public void testSetMaxBonusNumberPT() {
		set.setMaxBonusNumberPT(5);
		assertEquals(5, set.getMaxBonusNumberPT());
	}

	@Test
	public void testSetMaxValueBonusPT() {
		set.setMaxValueBonusPT(5);
		assertEquals(5, set.getMaxValueBonusPT());
	}

	@Test
	public void testSetMaxCitiesInPT() {
		set.setMaxCitiesInPT(5);
		assertEquals(5, set.getMaxCitiesInPT());
	}

	@Test
	public void testSetMaxBonusNumberNT() {
		set.setMaxBonusNumberNT(5);
		assertEquals(5, set.getMaxBonusNumberNT());
	}

	@Test
	public void testSetMaxValueBonusNT() {
		set.setMaxValueBonusNT(5);
		assertEquals(5, set.getMaxValueBonusNT());
	}

	@Test
	public void testSetNumPlayers() {
		set.setNumPlayers(5);
		assertEquals(5, set.getNumPlayers());
		assertEquals(16, set.getNumCardsForColor());
		assertEquals(15, set.getNumJollyCards());
	}

	@Test
	public void testSetSettingsID() {
		set.setSettingsID(5);
		assertEquals("./src/main/java/server/model/Match5Settings.json", set.getPath());
	}

}
