package gameModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import server.model.*;

public class TokenRewardBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	TokenRewardBonus tb;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testTokenRewardBonus() {
		tb = new TokenRewardBonus();
		assertTrue(tb instanceof TokenRewardBonus);
		assertEquals(tb.getType(), "TokenRewardBonus");
	}

	@Test
	public void testTokenRewardBonusInt() {
		tb = new TokenRewardBonus(5);
		assertEquals(5, tb.getValueBonus());
		assertEquals(tb.getType(), "TokenRewardBonus");
	}

	@Test
	public void testGiveBonus() {
		assertEquals(pippo.getRtBonus(), 0);
		tb = new TokenRewardBonus(5);
		tb.giveBonus(pippo);
		assertEquals(pippo.getRtBonus(), 5);
	}

}
