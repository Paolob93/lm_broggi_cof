package gameModel;

import static org.junit.Assert.*;
import server.model.*;

import org.junit.Before;
import org.junit.Test;

public class VPBonusTest {
	PlayerList pl;
	Match match;
	Player pippo;
	VPBonus vpb;
	
	@Before
	public void setUp() throws Exception {
		pl = new PlayerList();
		match = new Match(pl, 0);
		match.initMatch();
		pippo = new Player("Pippo", match);
	}

	@Test
	public void testVPBonus() {
		vpb = new VPBonus();
		assertTrue(vpb instanceof VPBonus);
		assertEquals(vpb.getType(), "VPBonus");
	}

	@Test
	public void testVPBonusInt() {
		vpb = new VPBonus(5);
		assertEquals(vpb.getValueBonus(), 5);
		assertEquals(vpb.getType(), "VPBonus");
	}

	@Test
	public void testGiveBonus() {
		assertEquals(pippo.getVictoryPoints(), 0);
		vpb = new VPBonus(5);
		vpb.giveBonus(pippo);
		assertEquals(pippo.getVictoryPoints(), 5);
		vpb = new VPBonus(3);
		vpb.giveBonus(pippo);
		assertEquals(pippo.getVictoryPoints(), 8);
	}
}
